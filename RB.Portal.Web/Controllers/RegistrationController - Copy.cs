﻿using RB.Portal.Web.Models;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Xml.Linq;
using static RB.Portal.Web.Models.Registration;
using Excel = Microsoft.Office.Interop.Excel;

namespace RB.Portal.Web.Controllers
{
    public class RegistrationController1 : Controller
    {

        //string rsdsourcefilepath = "~/Source/RSD.xlsx";
        //string nonrsdsourcefilepath = "~/Source/NonRSDWBS.xlsx";
        //string wbssourcefilepath = "~/Source/WBS.xlsx";
        //string nonwbssourcefilepath = "~/Source/NonRSDWBS.xlsx";
        //string sourcefolderpath = "~/Source/";
        //string destfolderpath = "~/UserFile/";
        //string destfolderpathdownload = "/UserFile/";

        //List<string> _JobTypeList = new List<string>();
        //CommonList _com = new CommonList();

        //[HttpGet]
        //public ActionResult Edit()
        //{
        //    return View(new EditModel());
        //}

        //[HttpPost]
        //public async Task<JsonResult> EditExcel(string jobtype, string subtype)
        //{
        //    List<string> _msg = new List<string>();
        //    try
        //    {
        //        var fileContent = Request.Files[0];
        //        if (fileContent != null && fileContent.ContentLength > 0)
        //        {
        //            var fileName = fileContent.FileName;
        //            string path = Path.Combine(Server.MapPath("~/UserFile"), fileName);


        //            if (System.IO.File.Exists(path))
        //            {
        //                System.IO.File.Delete(path);
        //            }

        //            fileContent.SaveAs(path);

        //            if (jobtype == "Government")
        //            {
        //                if (subtype == "rsdcom")
        //                {
        //                    _msg = ReadRSDExcelSheet(path);
        //                }
        //                else if (subtype == "nonrsdcom")
        //                {
        //                    _msg = ReadNonWBSRSDExcelSheet(path);
        //                }
        //            }
        //            else if (jobtype == "Corporate")
        //            {
        //                if (subtype == "wscom")
        //                {
        //                    _msg = ReadWBSExcelSheet(path);
        //                }
        //                else if (subtype == "nonwscom")
        //                {
        //                    _msg = ReadNonWBSRSDExcelSheet(path);
        //                }
        //            }
        //        }

        //        bool haserror = _msg.Count > 0 ? true : false;
        //        string textfilepath = "";
        //        if (HttpContext.Cache["phfilepath"] != null)
        //        {
        //            textfilepath = (string)HttpContext.Cache["phfilepath"];
        //        }
        //        var jsonResult = new
        //        {
        //            Haserror = haserror,
        //            filepath = textfilepath,
        //            msg = _msg
        //        };

        //        return Json(jsonResult, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception ex)
        //    {
        //        _msg = new List<string>();
        //        string errormsg = "File upload fail";
        //        _msg.Add(errormsg);
        //        var jsonResulterror = new
        //        {
        //            Haserror = true,
        //            filepath = "",
        //            msg = _msg
        //        };
        //        return Json(jsonResulterror, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //[HttpGet]
        //public ActionResult Add()
        //{
        //    try
        //    {
        //        XElement bankList = XElement.Load(Server.MapPath("~/Source/BankList.xml"));
        //        List<string> _Bcodelist = new List<string>();
        //        List<string> _BankNameList = new List<string>();
        //        List<string> _BankClearingCodeList = new List<string>();
        //        List<string> _CurrencyList = new List<string>();

        //        List<string> _WBSList = new List<string>();
        //        List<RSD> _RSDList = new List<RSD>();
        //        foreach (XElement name in bankList.Elements("BankNameList").Elements("bankname"))
        //        {
        //            _BankNameList.Add(name.Value);
        //        }
        //        foreach (XElement name in bankList.Elements("BankClearingCodeList").Elements("BankClearingCode"))
        //        {
        //            _BankClearingCodeList.Add(name.Value);
        //        }
        //        //foreach (XElement name in bankList.Elements("CurrencyList").Elements("currency"))
        //        //{
        //        //    _CurrencyList.Add(name.Value);
        //        //}
        //        //foreach (XElement name in bankList.Elements("JobTypeList").Elements("jobtype"))
        //        //{
        //        //    _JobTypeList.Add(name.Value);
        //        //}

        //        //foreach (XElement name in bankList.Elements("WBSList").Elements("wbsitem"))
        //        //{
        //        //    _WBSList.Add(name.Value);
        //        //}

        //        foreach (XElement item in bankList.Elements("RSDList").Elements("rsditem"))
        //        {
        //            RSD _rsd = new RSD();
        //            _rsd.id = item.Attribute("name").Value.ToString();
        //            _rsd.value = item.Value.ToString();
        //            _RSDList.Add(_rsd);
        //        }

        //        return View(new Registration()
        //        {
        //            _BankCodeList = _Bcodelist,
        //            _BankNameList = _BankNameList,
        //            _BankClearingCodeList = _BankClearingCodeList,
        //            _CurrencyListItems = GetCurrencyList(),
        //            _WBSListItems = GetWBSList(),
        //            _RSDListItems = GetRsdList(),
        //            _JobTypeListItems = GetJobTypes()
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Add(Registration _model)
        //{
        //    XElement bankList = XElement.Load(Server.MapPath("~/Source/BankList.xml"));
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            if (System.IO.Directory.Exists(Server.MapPath(sourcefolderpath)))
        //            {
        //                if (_model.JobType == "Government")
        //                {
        //                    if (_model.RSDCompatibity == "rsdcom")
        //                    {
        //                        if (System.IO.File.Exists(Server.MapPath(rsdsourcefilepath)))
        //                        {
        //                            if (System.IO.Directory.Exists(Server.MapPath(destfolderpath)))
        //                            {
        //                                string filename = string.Format("{0}_{1}.xlsx", Guid.NewGuid(), _model.FundingAccount);
        //                                Session["filename"] = filename;
        //                                if (Session["filename"] != null)
        //                                {
        //                                    System.IO.File.Copy(Server.MapPath(rsdsourcefilepath), Server.MapPath(Path.Combine(destfolderpath, Session["filename"].ToString())), true);
        //                                    // UpdateRSDExcelSheet(_model);
        //                                    TempData["SuccessMessage"] = "success";
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (_model.RSDCompatibity == "nonrsdcom")
        //                    {
        //                        if (System.IO.File.Exists(Server.MapPath(nonrsdsourcefilepath)))
        //                        {
        //                            if (System.IO.Directory.Exists(Server.MapPath(destfolderpath)))
        //                            {
        //                                string filename = string.Format("{0}_{1}.xlsx", Guid.NewGuid(), _model.FundingAccount);
        //                                Session["filename"] = filename;
        //                                if (Session["filename"] != null)
        //                                {
        //                                    System.IO.File.Copy(Server.MapPath(nonrsdsourcefilepath), Server.MapPath(Path.Combine(destfolderpath, Session["filename"].ToString())), true);
        //                                    // UpdateNonRSDExcelSheet(_model);
        //                                    TempData["SuccessMessage"] = "success";
        //                                }
        //                            }
        //                        }
        //                    }

        //                }
        //                else if (_model.JobType == "Corporate")
        //                {
        //                    if (_model.WBSCompatibity == "wscom")
        //                    {
        //                        if (System.IO.File.Exists(Server.MapPath(wbssourcefilepath)))
        //                        {
        //                            if (System.IO.Directory.Exists(Server.MapPath(destfolderpath)))
        //                            {
        //                                string filename = string.Format("{0}_{1}.xlsx", Guid.NewGuid(), _model.FundingAccount);
        //                                Session["filename"] = filename;
        //                                if (Session["filename"] != null)
        //                                {
        //                                    System.IO.File.Copy(Server.MapPath(wbssourcefilepath), Server.MapPath(Path.Combine(destfolderpath, Session["filename"].ToString())), true);
        //                                    //UpdateWBSExcelSheet(_model);
        //                                    TempData["SuccessMessage"] = "success";
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (_model.WBSCompatibity == "nonwscom")
        //                    {
        //                        if (System.IO.File.Exists(Server.MapPath(nonwbssourcefilepath)))
        //                        {
        //                            if (System.IO.Directory.Exists(Server.MapPath(destfolderpath)))
        //                            {
        //                                string filename = string.Format("{0}_{1}.xlsx", Guid.NewGuid(), _model.FundingAccount);
        //                                Session["filename"] = filename;
        //                                if (Session["filename"] != null)
        //                                {
        //                                    System.IO.File.Copy(Server.MapPath(nonwbssourcefilepath), Server.MapPath(Path.Combine(destfolderpath, Session["filename"].ToString())), true);
        //                                    //UpdateNonWBSExcelSheet(_model);
        //                                    TempData["SuccessMessage"] = "success";
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    //foreach (XElement name in bankList.Elements("JobTypeList").Elements("jobtype"))
        //    //{
        //    //    _JobTypeList.Add(name.Value);
        //    //}

        //    //foreach (XElement name in bankList.Elements("WBSList").Elements("wbsitem"))
        //    //{
        //    //    _WBSList.Add(name.Value);
        //    //}
        //    _model._WBSListItems = GetWBSList();
        //    _model._JobTypeListItems = GetJobTypes();
        //    _model._RSDListItems = GetRsdList();
        //    _model._CurrencyListItems = GetCurrencyList();
        //    return View(_model);
        //}

        //#region Update Excel Sheet

        //[HttpPost]
        //public async Task<JsonResult> regUpdateNonRSDExcelSheet(Registration _model)
        //{
        //    List<string> _msg = new List<string>();
        //    string filename = "";
        //    string filePathToSave = "";
        //    string filePathToDownload = "";
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_model.CustomerName))
        //        {
        //            _msg.Add("Customer name missing");
        //        }

        //        // isvalidString(_model.CustomerName, _msg, "Customer Name",2, out isva);

        //        _msg = CommonValidation(_model, _msg);

        //        if (_model.CreditDate == null)
        //        {
        //            _msg.Add("Credit date missing");
        //        }
        //        if (_model.CreditDate != null)
        //        {
        //            _msg = CreditDateValidation(_model.CreditDate, _msg, "Credit");
        //        }

        //        bool haserror = _msg.Count > 0 ? true : false;

        //        if (!haserror)
        //        {
        //            if (System.IO.File.Exists(Server.MapPath(nonrsdsourcefilepath)))
        //            {
        //                if (System.IO.Directory.Exists(Server.MapPath(destfolderpath)))
        //                {
        //                    filename = string.Format("{0}_{1}.xlsx", Guid.NewGuid(), _model.FundingAccount);
        //                    System.IO.File.Copy(Server.MapPath(nonrsdsourcefilepath), Server.MapPath(Path.Combine(destfolderpath, filename)), true);

        //                }
        //            }

        //            filePathToSave = Server.MapPath(destfolderpath + filename);
        //            filePathToDownload = destfolderpathdownload + filename;

        //            Excel.Application xlApp = new Excel.Application();
        //            Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePathToSave, 0, false, 5, "", "", false,
        //                Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.Unprotect("rbpayroll");
        //            Excel.Range xlRange = xlWorkSheet.UsedRange;
        //            int rowNumber = 2;

        //            try
        //            {
        //                xlWorkSheet.Cells[rowNumber, 1] = _model.CustomerName;
        //                xlWorkSheet.Cells[rowNumber, 2] = _model.AgremeentCode;
        //                xlWorkSheet.Cells[rowNumber, 3] = _model.FundingAccount;
        //                xlWorkSheet.Cells[rowNumber, 4] = _model.BranchNo;
        //                xlWorkSheet.Cells[rowNumber, 5] = _model.CreditDate;
        //                xlWorkSheet.Cells[rowNumber, 6] = _model.BankCode;
        //                xlWorkSheet.Cells[rowNumber, 7] = _model.Currency;
        //                // xlWorkSheet.Protect("rbpayroll");
        //                xlApp.DisplayAlerts = false;
        //                xlWorkBook.SaveAs(filePathToSave, Excel.XlFileFormat.xlOpenXMLWorkbook,
        //                    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlNoChange,
        //                    Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value,
        //                    Missing.Value, Missing.Value);
        //                xlWorkBook.Close();
        //                xlApp.Quit();

        //                Marshal.ReleaseComObject(xlWorkSheet);
        //                Marshal.ReleaseComObject(xlWorkBook);
        //                Marshal.ReleaseComObject(xlApp);
        //            }
        //            catch (Exception ex)
        //            {
        //                xlWorkBook.Close();
        //                xlApp.Quit();
        //                Marshal.ReleaseComObject(xlWorkSheet);
        //                Marshal.ReleaseComObject(xlWorkBook);
        //                Marshal.ReleaseComObject(xlApp);
        //                throw ex;
        //            }
        //        }

        //        var jsonResult = new
        //        {
        //            Haserror = haserror,
        //            filepath = filePathToDownload,
        //            msg = _msg
        //        };

        //        return Json(jsonResult, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        _msg = new List<string>();
        //        string errormsg = "Error occured!!";
        //        _msg.Add(errormsg);
        //        var jsonResulterror = new
        //        {
        //            Haserror = true,
        //            filepath = "",
        //            msg = _msg
        //        };
        //        return Json(jsonResulterror, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //[HttpPost]
        //public async Task<JsonResult> regUpdateRSDExcelSheet(Registration _regmodel)
        //{
        //    List<string> _errormsg = new List<string>();
        //    string filename = "";
        //    string filePathToSave = "";
        //    string filePathToDownload = "";
        //    try
        //    {
        //        _errormsg = CommonValidation(_regmodel, _errormsg);

        //        if (_regmodel.ValueDate == null)
        //        {
        //            _errormsg.Add("Value date is missing");
        //        }
        //        if (_regmodel.ValueDate != null)
        //        {
        //            _errormsg = CreditDateValidation(_regmodel.ValueDate, _errormsg, "Value");
        //        }


        //        _errormsg = RSDHeaderValidation(_regmodel, _errormsg);

        //        bool haserror = _errormsg.Count > 0 ? true : false;

        //        if (!haserror)
        //        {
        //            if (System.IO.File.Exists(Server.MapPath(rsdsourcefilepath)))
        //            {
        //                if (System.IO.Directory.Exists(Server.MapPath(destfolderpath)))
        //                {
        //                    filename = string.Format("{0}_{1}.xlsx", Guid.NewGuid(), _regmodel.FundingAccount);
        //                    System.IO.File.Copy(Server.MapPath(rsdsourcefilepath), Server.MapPath(Path.Combine(destfolderpath, filename)), true);
        //                }
        //            }
        //            filePathToSave = Server.MapPath(destfolderpath + filename);
        //            filePathToDownload = destfolderpathdownload + filename;

        //            Excel.Application xlApp = new Excel.Application();
        //            Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePathToSave, 0, false, 5, "", "", false,
        //                Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.Unprotect("rbpayroll");
        //            Excel.Range xlRange = xlWorkSheet.UsedRange;
        //            int rowNumber = 2;

        //            try
        //            {
        //                xlWorkSheet.Cells[rowNumber, 1] = _regmodel.CompanyName;
        //                xlWorkSheet.Cells[rowNumber, 2] = _regmodel.AgremeentCode;
        //                xlWorkSheet.Cells[rowNumber, 3] = _regmodel.FundingAccount;
        //                xlWorkSheet.Cells[rowNumber, 4] = _regmodel.BranchNo;
        //                xlWorkSheet.Cells[rowNumber, 5] = _regmodel.ValueDate;
        //                xlWorkSheet.Cells[rowNumber, 6] = _regmodel.DepartmentName;
        //                xlWorkSheet.Cells[rowNumber, 7] = _regmodel.DivisionNumber;
        //                xlWorkSheet.Cells[rowNumber, 8] = _regmodel.SubDivisionNumber;
        //                xlWorkSheet.Cells[rowNumber, 9] = _regmodel.SectorNumber;
        //                xlWorkSheet.Cells[rowNumber, 10] = _regmodel.ClassNumber;
        //                xlWorkSheet.Cells[rowNumber, 11] = _regmodel.PaymentOrderNumber;
        //                xlWorkSheet.Cells[rowNumber, 13] = _regmodel.BankCode;
        //                xlWorkSheet.Cells[rowNumber, 14] = _regmodel.Currency;
        //                xlWorkSheet.Protect("rbpayroll");
        //                xlApp.DisplayAlerts = false;
        //                xlWorkBook.SaveAs(filePathToSave, Excel.XlFileFormat.xlOpenXMLWorkbook,
        //                    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlNoChange,
        //                    Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value,
        //                    Missing.Value, Missing.Value);
        //                xlWorkBook.Close();
        //                xlApp.Quit();

        //                Marshal.ReleaseComObject(xlWorkSheet);
        //                Marshal.ReleaseComObject(xlWorkBook);
        //                Marshal.ReleaseComObject(xlApp);

        //            }
        //            catch (Exception ex)
        //            {
        //                xlWorkBook.Close();
        //                xlApp.Quit();
        //                Marshal.ReleaseComObject(xlWorkSheet);
        //                Marshal.ReleaseComObject(xlWorkBook);
        //                Marshal.ReleaseComObject(xlApp);
        //                throw ex;
        //            }
        //        }

        //        var jsonResult = new
        //        {
        //            Haserror = haserror,
        //            filepath = filePathToDownload,
        //            msg = _errormsg
        //        };

        //        return Json(jsonResult, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception ex)
        //    {
        //        _errormsg = new List<string>();
        //        string errormsg = "Error occured!!";
        //        _errormsg.Add(errormsg);
        //        var jsonResulterror = new
        //        {
        //            Haserror = true,
        //            filepath = "",
        //            msg = _errormsg
        //        };
        //        return Json(jsonResulterror, JsonRequestBehavior.AllowGet);
        //    }

        //    return Json(new object { }, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public async Task<JsonResult> regUpdateWBSExcelSheet(Registration _regmodel)
        //{
        //    List<string> _errormsg = new List<string>();
        //    string filename = "";
        //    string filePathToSave = "";
        //    string filePathToDownload = "";

        //    try
        //    {
        //        _errormsg = WBSValidation(_regmodel, _errormsg);


        //        if (_regmodel.CreditDate == null)
        //        {
        //            _errormsg.Add("Credit date missing");
        //        }
        //        if (_regmodel.CreditDate != null)
        //        {
        //            _errormsg = CreditDateValidation(_regmodel.CreditDate, _errormsg, "Credit");
        //        }

        //        bool haserror = _errormsg.Count > 0 ? true : false;
        //        if (!haserror)
        //        {

        //            if (System.IO.File.Exists(Server.MapPath(wbssourcefilepath)))
        //            {
        //                if (System.IO.Directory.Exists(Server.MapPath(destfolderpath)))
        //                {
        //                    filename = string.Format("{0}_{1}.xlsx", Guid.NewGuid(), _regmodel.FundingAccount);
        //                    System.IO.File.Copy(Server.MapPath(wbssourcefilepath), Server.MapPath(Path.Combine(destfolderpath, filename)), true);
        //                }
        //            }
        //            filePathToSave = Server.MapPath(destfolderpath + filename);
        //            filePathToDownload = destfolderpathdownload + filename;

        //            Excel.Application xlApp = new Excel.Application();

        //            Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePathToSave, 0, false, 5, "", "", false,
        //                Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.Unprotect("rbpayroll");
        //            Excel.Range xlRange = xlWorkSheet.UsedRange;
        //            int rowNumber = 2;

        //            try
        //            {
        //                xlWorkSheet.Cells[rowNumber, 1] = _regmodel.CustomerName;
        //                xlWorkSheet.Cells[rowNumber, 2] = _regmodel.AgremeentCode;
        //                xlWorkSheet.Cells[rowNumber, 3] = _regmodel.FundingAccount;
        //                xlWorkSheet.Cells[rowNumber, 4] = _regmodel.BranchNo;
        //                xlWorkSheet.Cells[rowNumber, 5] = _regmodel.CreditDate;
        //                xlWorkSheet.Cells[rowNumber, 6] = _regmodel.MinsOfLabEstablishedId;
        //                xlWorkSheet.Cells[rowNumber, 7] = _regmodel.CommerceRoomId;
        //                xlWorkSheet.Cells[rowNumber, 8] = _regmodel.BankCode;
        //                xlWorkSheet.Cells[rowNumber, 9] = _regmodel.Currency;
        //                xlWorkSheet.Protect("rbpayroll");
        //                xlApp.DisplayAlerts = false;
        //                xlWorkBook.SaveAs(filePathToSave, Excel.XlFileFormat.xlOpenXMLWorkbook,
        //                    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlNoChange,
        //                    Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value,
        //                    Missing.Value, Missing.Value);
        //                xlWorkBook.Close();
        //                xlApp.Quit();

        //                Marshal.ReleaseComObject(xlWorkSheet);
        //                Marshal.ReleaseComObject(xlWorkBook);
        //                Marshal.ReleaseComObject(xlApp);
        //            }
        //            catch (Exception ex)
        //            {
        //                xlWorkBook.Close();
        //                xlApp.Quit();
        //                Marshal.ReleaseComObject(xlWorkSheet);
        //                Marshal.ReleaseComObject(xlWorkBook);
        //                Marshal.ReleaseComObject(xlApp);
        //                throw ex;
        //            }
        //        }

        //        var jsonResult = new
        //        {
        //            Haserror = haserror,
        //            filepath = filePathToDownload,
        //            msg = _errormsg
        //        };

        //        return Json(jsonResult, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception ex)
        //    {
        //        _errormsg = new List<string>();
        //        string errormsg = "Error occured!!";
        //        _errormsg.Add(errormsg);
        //        var jsonResulterror = new
        //        {
        //            Haserror = true,
        //            filepath = "",
        //            msg = _errormsg
        //        };
        //        return Json(jsonResulterror, JsonRequestBehavior.AllowGet);
        //    }

        //    return Json(new object { }, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public async Task<JsonResult> regUpdateNonWBSExcelSheet(Registration _regmodel)
        //{
        //    List<string> _errormsg = new List<string>();
        //    string filename = "";
        //    string filePathToSave = "";
        //    string filePathToDownload = "";

        //    try
        //    {
        //        if (_regmodel.CustomerName == null)
        //        {
        //            _errormsg.Add("Customer name is missing");
        //        }
        //        _errormsg = CommonValidation(_regmodel, _errormsg);

        //        if (_regmodel.CreditDate == null)
        //        {
        //            _errormsg.Add("Credit date missing");
        //        }
        //        if (_regmodel.CreditDate != null)
        //        {
        //            _errormsg = CreditDateValidation(_regmodel.CreditDate, _errormsg, "Credit");
        //        }


        //        bool haserror = _errormsg.Count > 0 ? true : false;

        //        if (!haserror)
        //        {

        //            if (System.IO.File.Exists(Server.MapPath(nonwbssourcefilepath)))
        //            {
        //                if (System.IO.Directory.Exists(Server.MapPath(destfolderpath)))
        //                {
        //                    filename = string.Format("{0}_{1}.xlsx", Guid.NewGuid(), _regmodel.FundingAccount);
        //                    System.IO.File.Copy(Server.MapPath(nonwbssourcefilepath), Server.MapPath(Path.Combine(destfolderpath, filename)), true);
        //                }
        //            }
        //            filePathToSave = Server.MapPath(destfolderpath + filename);
        //            filePathToDownload = destfolderpathdownload + filename;

        //            Excel.Application xlApp = new Excel.Application();

        //            Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePathToSave, 0, false, 5, "", "", false,
        //                Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
        //            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //            xlWorkSheet.Unprotect("rbpayroll");
        //            Excel.Range xlRange = xlWorkSheet.UsedRange;
        //            int rowNumber = 2;

        //            try
        //            {
        //                xlWorkSheet.Cells[rowNumber, 1] = _regmodel.CustomerName;
        //                xlWorkSheet.Cells[rowNumber, 2] = _regmodel.AgremeentCode;
        //                xlWorkSheet.Cells[rowNumber, 3] = _regmodel.FundingAccount;
        //                xlWorkSheet.Cells[rowNumber, 4] = _regmodel.BranchNo;
        //                xlWorkSheet.Cells[rowNumber, 5] = _regmodel.CreditDate;
        //                xlWorkSheet.Cells[rowNumber, 6] = _regmodel.BankCode;
        //                xlWorkSheet.Cells[rowNumber, 7] = _regmodel.Currency;
        //                xlWorkSheet.Protect("rbpayroll");
        //                // Disable file override confirmaton message  
        //                xlApp.DisplayAlerts = false;
        //                xlWorkBook.SaveAs(filePathToSave, Excel.XlFileFormat.xlOpenXMLWorkbook,
        //                    Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlNoChange,
        //                    Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value,
        //                    Missing.Value, Missing.Value);
        //                xlWorkBook.Close();
        //                xlApp.Quit();

        //                Marshal.ReleaseComObject(xlWorkSheet);
        //                Marshal.ReleaseComObject(xlWorkBook);
        //                Marshal.ReleaseComObject(xlApp);
        //            }
        //            catch (Exception ex)
        //            {
        //                xlWorkBook.Close();
        //                xlApp.Quit();
        //                Marshal.ReleaseComObject(xlWorkSheet);
        //                Marshal.ReleaseComObject(xlWorkBook);
        //                Marshal.ReleaseComObject(xlApp);
        //            }
        //        }
        //        var jsonResult = new
        //        {
        //            Haserror = haserror,
        //            filepath = filePathToDownload,
        //            msg = _errormsg
        //        };

        //        return Json(jsonResult, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception ex)
        //    {
        //        _errormsg = new List<string>();
        //        string errormsg = "Error occured!!";
        //        _errormsg.Add(errormsg);
        //        var jsonResulterror = new
        //        {
        //            Haserror = true,
        //            filepath = "",
        //            msg = _errormsg
        //        };
        //        return Json(jsonResulterror, JsonRequestBehavior.AllowGet);
        //    }

        //    return Json(new object { }, JsonRequestBehavior.AllowGet);
        //}


        //#endregion

        //#region Validations

        //private List<string> WBSValidation(Registration _regmodel, List<string> _errormsg)
        //{
        //    if (_regmodel.CustomerName == null)
        //    {
        //        _errormsg.Add("Customer name is missing");
        //    }

        //    _errormsg = CommonValidation(_regmodel, _errormsg);

        //    if (_regmodel.CreditDate == null)
        //    {
        //        _errormsg.Add("Credit Date is missing");
        //    }

        //    if (_regmodel.CreditDate != null)
        //    {
        //        _errormsg = CreditDateValidation(_regmodel.CreditDate, _errormsg, "Credit");
        //    }

        //    if (_regmodel.MinsOfLabEstablishedId == null)
        //    {
        //        _errormsg.Add("Mins of Lab Established Id is missing");
        //    }

        //    if (_regmodel.CommerceRoomId == null)
        //    {
        //        _errormsg.Add("Commerce Room Id is missing");
        //    }

        //    return _errormsg;
        //}

        //private List<string> CommonValidation(Registration _regmodel, List<string> _errormsg)
        //{
        //    if (_regmodel.AgremeentCode == null)
        //    {
        //        _errormsg.Add("Agremeent Code is missing");
        //    }
        //    if (_regmodel.AgremeentCode != null && !IsValidLength(_regmodel.AgremeentCode, 8))
        //    {
        //        _errormsg.Add("Agremeent Code can't be more than 8 digits");
        //    }

        //    if (_regmodel.AgremeentCode != null && Regex.IsMatch(_regmodel.AgremeentCode, @"\s"))
        //    {
        //        _errormsg.Add("Agreement Code contain spaces");
        //    }

        //    if (_regmodel.FundingAccount == null)
        //    {
        //        _errormsg.Add("Funding account is missing");
        //    }

        //    if (_regmodel.FundingAccount != null && _regmodel.FundingAccount.ToString().Length != 13)
        //    {
        //        _errormsg.Add("Funding Account number should be 13 digits");
        //    }

        //    if (_regmodel.FundingAccount != null && !IsNumeric(_regmodel.FundingAccount.ToString()))
        //    {
        //        _errormsg.Add("Funding Account not numeric");
        //    }

        //    if (_regmodel.BranchNo == null)
        //    {
        //        _errormsg.Add("Branch number is missing");
        //    }

        //    if (_regmodel.BranchNo != null && _regmodel.BranchNo.ToString().Length != 3)
        //    {
        //        _errormsg.Add("Branch number should be 3 digits");
        //    }

        //    if (_regmodel.BranchNo != null)
        //    {
        //        string branchno = _regmodel.BranchNo.Substring(0, 3);

        //        if (!IsNumeric(branchno))
        //        {
        //            _errormsg.Add("First three digits of the Funding Account No , must be Numeric");
        //        }
        //        string fundingacnt = _regmodel.FundingAccount.Substring(0, 3);
        //        if (!(fundingacnt).Equals(branchno))
        //        {
        //            _errormsg.Add("Invalid Branch No , it must be as the first three digits of the Funding Account No");
        //        }
        //    }

        //    if (_regmodel.BankCode == null)
        //    {
        //        _errormsg.Add("Bank Code is missing");
        //    }

        //    if (_regmodel.Currency == null)
        //    {
        //        _errormsg.Add("Currency is missing");
        //    }

        //    if (_regmodel.Currency != null && !IsValidLength(_regmodel.Currency, 3))
        //    {
        //        _errormsg.Add("Currency it must be 3 char");
        //    }

        //    // _errormsg = isvalidString(_regmodel.Currency, _errormsg, "Currency Code");
        //    return _errormsg;
        //}

        //public bool IsValidLength(string data, int length)
        //{
        //    if (data != null && data.Length > length)
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        //public bool IsNumeric(string value)
        //{
        //    bool isNumeric = false;
        //    char[] digits = "0123456789".ToCharArray();
        //    char[] letters = value.ToCharArray();
        //    for (int k = 0; k < letters.Length; k++)
        //    {

        //        if (digits.Contains(letters[k]))
        //        {
        //            isNumeric = true;
        //        }
        //        else
        //        {
        //            isNumeric = false;
        //            break;
        //        }
        //        //for (int i = 0; i < digits.Length; i++)
        //        //{
        //        //    if (letters[k] != digits[i])
        //        //    {
        //        //        isNumeric = false;
        //        //        break;
        //        //    }
        //        //}
        //    }
        //    return isNumeric;
        //}

        //public List<string> isvalidString(string prmStr, List<string> msg, string filedname, int rowno, out bool isvalid)
        //{
        //    isvalid = false;
        //    try
        //    {
        //        bool english_lang = false, arabic_lang = false, invalid_lang = false, space_found = false;
        //        //char ch ='';
        //        int number = 0;
        //        string invalidLetters = "";
        //        if (!string.IsNullOrEmpty(prmStr))
        //        {
        //            for (int i = 0; i < prmStr.Length; i++)
        //            {
        //                char ch = prmStr[i];
        //                if (ch == '-' || ch == '.')
        //                {
        //                }
        //                else if (!IsNumeric(ch.ToString()))
        //                {

        //                    number = (int)ch;
        //                    if (ch == ' ')
        //                    {
        //                        space_found = true;
        //                    }
        //                    else if (number >= 65 && number <= 90)
        //                    {
        //                        english_lang = true;
        //                    }
        //                    else if (number >= 97 && number <= 122)
        //                    {
        //                        english_lang = true;
        //                    }
        //                    else if (number >= 127 && number <= 255)
        //                    {
        //                        arabic_lang = true;
        //                    }
        //                    else
        //                    {
        //                        invalid_lang = true;
        //                        invalidLetters = ch.ToString();
        //                    }
        //                }
        //            }
        //            if (!string.IsNullOrEmpty(invalidLetters))
        //            {
        //                invalidLetters = "[" + invalidLetters + "]";
        //                if (english_lang)
        //                {
        //                    msg.Add(string.Format("Invalid letters in {0} {1}  ", filedname, invalidLetters));
        //                }
        //                else
        //                {
        //                    msg.Add(string.Format("Invalid letters in {0} {1}  ", filedname, invalidLetters));
        //                }

        //                if (english_lang && arabic_lang)
        //                {
        //                    if (english_lang)
        //                    {
        //                        msg.Add(string.Format("Invalid {0} , must be English or Arabic language {1} - row no {2} ", filedname, invalidLetters, rowno));
        //                    }
        //                    else
        //                    {
        //                        msg.Add(string.Format("Invalid {0} , must be English or Arabic language {1} - row no {2}  ", filedname, invalidLetters, rowno));
        //                    }
        //                }

        //            }
        //            if (space_found)
        //            {
        //                msg.Add(string.Format("Invalid {0} , Invalid Agreement Code contain space - row no {1}  ", filedname, rowno));
        //            }
        //            else
        //            {
        //                isvalid = true;
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        msg.Add(string.Format("Invalid data {0} - row no {1}  ", filedname, rowno));
        //    }


        //    return msg;

        //}

        //public List<string> CreditDateValidation(string date, List<string> msglist, string filedname)
        //{
        //    if (string.IsNullOrEmpty(date))
        //    {
        //        msglist.Add(string.Format("Invalid " + filedname + " Date is empty", filedname));
        //    }
        //    else
        //    {
        //        string dd = date.Split('/')[0];
        //        string mm = date.Split('/')[1];
        //        string yyyy = date.Split('/')[2];

        //        if (date.Length != 10 || Convert.ToInt32(mm) > 12)
        //        {
        //            msglist.Add("Invalid " + filedname + " date format" + date + "it must be with DD/MM/YYYY date format");
        //        }
        //        else if (Convert.ToInt32(mm) > 12)
        //        {
        //            msglist.Add("Invalid " + filedname + " date format" + date + "it must be with DD/MM/YYYY date format");
        //        }

        //        else
        //        {
        //            DateTime crdate;
        //            if (!DateTime.TryParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture,
        //                                   DateTimeStyles.None, out crdate))
        //            {
        //                msglist.Add("Invalid " + filedname + " date format" + date + "it must be with dd/MM/YYYY date format");
        //            }
        //            else
        //            {
        //                var _crdate = Convert.ToDateTime(date);
        //                var newdatetime = new DateTime(int.Parse(yyyy), int.Parse(mm), int.Parse(dd));
        //                if (newdatetime < DateTime.Today)
        //                {
        //                    msglist.Add("The " + filedname + " Date : " + _crdate.ToString("dd/MM/yyyy") + " can't be less than current date");
        //                }
        //                else
        //                {
        //                    if (newdatetime > DateTime.Today.AddDays(30))
        //                    {
        //                        msglist.Add("The " + filedname + " Date : " + _crdate.ToString("dd/MM/yyyy") + " can't be more than current date + 30 days");
        //                    }
        //                    else
        //                    {
        //                        var hrs = DateTime.Now.Hour;
        //                        string dayname = newdatetime.DayOfWeek.ToString();
        //                        var datediff = (newdatetime - DateTime.Now).TotalDays;
        //                        datediff = datediff > 0 ? datediff : 0;
        //                        if (dayname == "Friday" || dayname == "Saturday")
        //                        {
        //                            msglist.Add("The " + filedname + " Date : " + _crdate.ToString("dd/MM/yyyy") + "can't be weekend");
        //                        }
        //                        else if (dayname == "Wednesday")
        //                        {

        //                            if (hrs < 12 && Convert.ToInt32(datediff) < 4)
        //                            {
        //                                msglist.Add("Today is Wednesday before noon ,To serve you better , the " + filedname + " Date should be at least two working days ( 2 ) after today");
        //                            }
        //                            else if (hrs > 12 && Convert.ToInt32(datediff) < 5)
        //                            {
        //                                msglist.Add("Today is Wednesday afternoon ,To serve you better , the " + filedname + " Date should be at least three working days ( 3 ) after today");
        //                            }
        //                        }
        //                        else if (dayname == "Thursday")
        //                        {
        //                            if (hrs < 12 && Convert.ToInt32(datediff) < 4)
        //                            {
        //                                msglist.Add("Today is Thursday before noon ,To serve you better , the " + filedname + " Date should be at least two working days ( 2 ) after today");
        //                            }
        //                            else if (hrs > 12 && Convert.ToInt32(datediff) < 5)
        //                            {
        //                                msglist.Add("Today is Thursday afternoon ,To serve you better , the " + filedname + " Date should be at least three working days ( 3 ) after today");
        //                            }
        //                        }
        //                        else
        //                        {
        //                            if (hrs < 12 && Convert.ToInt32(datediff) < 4)
        //                            {
        //                                msglist.Add("To serve you better, the " + filedname + " Date should be at least two(2) days after today");
        //                            }
        //                            else if (hrs > 12 && Convert.ToInt32(datediff) < 5)
        //                            {
        //                                msglist.Add("To serve you better , the " + filedname + " Date should be at least three days ( 3 ) after today");
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return msglist;
        //}

        //public string validateEmpActive(string status)
        //{
        //    string msg = "";
        //    if (status.ToLower().Equals("active"))
        //    {
        //        msg = "Invalid Employee Not Acitve !";
        //    }

        //    return msg;
        //}

        //private List<string> CommonUploadValidation_NonType(List<clsNonWBS> _modelList, List<string> _errormsg)
        //{
        //    int rowno = 4;

        //    foreach (clsNonWBS _model in _modelList)
        //    {
        //        bool isemptyrow = true; ;

        //        if (string.IsNullOrEmpty(_model.AccountNumber))
        //        {
        //            _errormsg.Add(string.Format("Account number is missing-row no {0}", rowno));
        //        }
        //        if (!string.IsNullOrEmpty(_model.AccountNumber) && !IsValidLength(_model.AccountNumber.ToString(), 24))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Account number should be of 24 characters-row no {0}", rowno));
        //        }

        //        if (_model.Address == null)
        //        {
        //            _errormsg.Add(string.Format("Address is missing-row no {0}", rowno));
        //        }
        //        if (_model.Address != null)
        //        {
        //            isemptyrow = false;
        //            if (!IsValidLength(_model.Address, 35))
        //            {
        //                _errormsg.Add(string.Format("Invalid Address can't be more than 35 char-row no {0}", rowno));
        //            }

        //            // _errormsg = isvalidString(_model.Address, _errormsg, "Address", rowno);
        //        }

        //        if (_model.Status == null)
        //        {
        //            _errormsg.Add(string.Format("Status is missing-row no {0}", rowno));
        //        }
        //        if (_model.Status != null && _model.Status != "Active")
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid Employee Not Active !-row no {0}", rowno));
        //        }

        //        if (_model.PaymentDesc != null && !IsValidLength(_model.PaymentDesc, 140))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid Payment Description can't be more than 140 char-row no {0}", rowno));
        //        }

        //        if (_model.BeneficiaryRef == null)
        //        {
        //            _errormsg.Add(string.Format("Beneficiary Ref is missing-row no {0}", rowno));
        //        }
        //        if (_model.BeneficiaryRef != null && !IsValidLength(_model.BeneficiaryRef, 35))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid Beneficiary Name more than 35 char-row no {0}", rowno));
        //        }

        //        if (_model.BankCode == null)
        //        {
        //            _errormsg.Add(string.Format("Bank code is missing-row no {0}", rowno));
        //        }

        //        if (_model.BankCode != null && _model.BankCode.Length != 11)
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("BankCode should be 11 chars-row no {0}", rowno));
        //        }

        //        if (_model.City != null && !IsValidLength(_model.City, 20))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid City Name can't be more than 20 char-row no {0}", rowno));
        //        }

        //        if (_model.ZipCode > 0 && !IsValidLength(_model.ZipCode.ToString(), 9))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid ZipCode can't be more than 9 char-row no {0}", rowno));
        //        }

        //        if (isemptyrow)
        //        {
        //            _errormsg = new List<string>();
        //            _errormsg.Add(string.Format("Empty data sheet, please upload the sheet with data-row no {0}", rowno));
        //        }

        //        rowno = rowno + 1;
        //    }

        //    return _errormsg;
        //}

        //private List<string> WBSUploadValidation(List<clsWBS> _modelList, List<string> _errormsg)
        //{
        //    int rowno = 4;
        //    bool isemptyrow = true;
        //    foreach (var _model in _modelList)
        //    {

        //        if (_model.AccountNumber != null && !IsValidLength(_model.AccountNumber.ToString(), 24))
        //        {
        //            _errormsg.Add(string.Format("Account number should be of 24 characters-row no {0}", rowno));
        //        }

        //        if (!IsValidLength(_model.Address, 35))
        //        {
        //            _errormsg.Add(string.Format("Invalid Address can't be more than 35 char-row no {0}", rowno));
        //        }

        //        // _errormsg = isvalidString(_model.Address, _errormsg, "Address");
        //        //
        //        if (_model.Status == null)
        //        {
        //            _errormsg.Add(string.Format("Status is missing-row no {0}", rowno));
        //        }
        //        if (_model.Status != null && _model.Status != "Active")
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid Employee Not Active !-row no {0}", rowno));
        //        }

        //        if (!IsValidLength(_model.PaymentDesc, 140))
        //        {
        //            _errormsg.Add(string.Format("Invalid Payment Description can't be more than 140 char-row no {0}", rowno));
        //        }

        //        if (!IsValidLength(_model.PaymentRef, 16))
        //        {
        //            _errormsg.Add(string.Format("Invalid Payment Reference can't be more than 16 char-row no {0}", rowno));
        //        }

        //        if (_model.BasicSalary != null && !IsValidLength(_model.BasicSalary.ToString(), 12))
        //        {
        //            _errormsg.Add(string.Format("Invalid Basic Salary can't be more than 12 digits-row no {0}", rowno));
        //        }

        //        if (_model.BasicSalary < 0)
        //        {
        //            _errormsg.Add(string.Format("Invalid Basic Salary-row no {0}", rowno));
        //        }

        //        if (_model.HousingAllowance != null && !IsValidLength(_model.HousingAllowance.ToString(), 12))
        //        {
        //            _errormsg.Add(string.Format("Invalid Housing allowance can't be more than 12 digits-row no {0}", rowno));
        //        }

        //        if (_model.HousingAllowance < 0)
        //        {
        //            _errormsg.Add(string.Format("Invalid Housing allowance-row no {0}", rowno));
        //        }

        //        if (_model.OtherEarnings != null && !IsValidLength(_model.OtherEarnings.ToString(), 12))
        //        {
        //            _errormsg.Add(string.Format("Invalid Other earnings can't be more than 12 digits-row no {0}", rowno));
        //        }

        //        if (_model.OtherEarnings < 0)
        //        {
        //            _errormsg.Add(string.Format("Invalid Other earnings-row no {0}", rowno));
        //        }

        //        if (_model.Deductions != null && !IsValidLength(_model.Deductions.ToString(), 12))
        //        {
        //            _errormsg.Add(string.Format("Invalid Deductions can't be more than 12 digits-row no {0}", rowno));
        //        }

        //        if (_model.Deductions < 0)
        //        {
        //            _errormsg.Add(string.Format("Invalid Deductions-row no {0}", rowno));
        //        }

        //        if (!IsValidLength(_model.BeneficiaryId, 35))
        //        {
        //            _errormsg.Add(string.Format("Invalid Beneficiary Name more than 35 char-row no {0}", rowno));
        //        }

        //        //_errormsg = isvalidString(_model.BeneficiaryName, _errormsg, "Beneficiary Name-row no {0}", rowno));


        //        if (_model.BankCode != null && _model.BankCode.Length != 11)
        //        {
        //            _errormsg.Add(string.Format("BankCode should be 11 chars-row no {0}", rowno));
        //        }
        //    }

        //    return _errormsg;
        //}

        //private List<string> RSDUploadValidation(List<clsRSD> _modelList, Registration _regmodel, List<string> _errormsg)
        //{
        //    _errormsg = RSDHeaderValidation(_regmodel, _errormsg);
        //    bool isemptyrow = true;
        //    int rowno = 4;
        //    foreach (var _model in _modelList)
        //    {
        //        if (_model.AccountNumber <= 0)
        //        {
        //            _errormsg.Add(string.Format("Account number missing-row no {0}", rowno));
        //        }

        //        if (_model.AccountNumber > 0 && !IsValidLength(_model.AccountNumber.ToString(), 24))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Account number should be of 24 characters-row no {0}", rowno));
        //        }


        //        if (_model.Address != null)
        //        {
        //            isemptyrow = false;
        //            //_errormsg = isvalidString(_model.Address, _errormsg, "Address");

        //            if (!IsValidLength(_model.Address, 35))
        //            {
        //                _errormsg.Add(string.Format("Invalid Address can't be more than 35 char-row no {0}", rowno));
        //            }
        //        }


        //        if (_model.City != null && !IsValidLength(_model.City, 20))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid City Name can't be more than 20 char-row no {0}", rowno));
        //        }
        //        if (_model.ZipCode > 0 && !IsValidLength(_model.ZipCode.ToString(), 9))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid ZipCode can't be more than 9 char-row no {0}", rowno));
        //        }
        //        if (_model.Status == null)
        //        {
        //            _errormsg.Add(string.Format("Status is missing-row no {0}", rowno));
        //        }
        //        if (_model.Status != null && _model.Status != "Active")
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid Employee Not Active !-row no {0}", rowno));
        //        }

        //        if (_model.PaymentDesc != null && !IsValidLength(_model.PaymentDesc, 140))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid Payment Description can't be more than 140 char-row no {0}", rowno));
        //        }

        //        if (_model.PaymentRef != null && !IsValidLength(_model.PaymentRef, 16))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid Payment Reference can't be more than 16 char-row no {0}", rowno));
        //        }

        //        if (_model.BasicSalary <= 0)
        //        {
        //            _errormsg.Add(string.Format("Basic Salary missing-row no {0}", rowno));
        //        }

        //        if (_model.BasicSalary > 0)
        //        {
        //            isemptyrow = false;
        //            if (!IsValidLength(_model.BasicSalary.ToString(), 12))
        //            {
        //                _errormsg.Add(string.Format("Invalid Basic Salary can't be more than 12 digits-row no {0}", rowno));
        //            }

        //            if (_model.BasicSalary < 0)
        //            {
        //                _errormsg.Add(string.Format("Invalid Basic Salary-row no {0}", rowno));
        //            }
        //        }

        //        if (_model.HousingAllowance <= 0)
        //        {
        //            _errormsg.Add(string.Format("Housing allowance missing-row no {0}", rowno));
        //        }
        //        if (_model.HousingAllowance > 0)
        //        {
        //            isemptyrow = false;
        //            if (!IsValidLength(_model.HousingAllowance.ToString(), 12))
        //            {
        //                _errormsg.Add(string.Format("Invalid Housing allowance can't be more than 12 digits-row no {0}", rowno));
        //            }

        //            if (_model.HousingAllowance < 0)
        //            {
        //                _errormsg.Add(string.Format("Invalid Housing allowance-row no {0}", rowno));
        //            }
        //        }

        //        if (_model.OtherEarnings < 0)
        //        {
        //            _errormsg.Add(string.Format("Invalid Other earnings-row no {0}", rowno));
        //        }


        //        if (_model.OtherEarnings > 0)
        //        {
        //            isemptyrow = false;
        //            if (!IsValidLength(_model.OtherEarnings.ToString(), 12))
        //            {
        //                _errormsg.Add(string.Format("Invalid Other earnings can't be more than 12 digits-row no {0}", rowno));
        //            }

        //        }

        //        if (_model.Deductions < 0)
        //        {
        //            _errormsg.Add(string.Format("Invalid Deductions-row no {0}", rowno));
        //        }
        //        if (_model.Deductions > 0 && !IsValidLength(_model.Deductions.ToString(), 12))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid Deductions can't be more than 12 digits-row no {0}", rowno));
        //        }


        //        if (_regmodel.DepartmentName == null)
        //        {
        //            _errormsg.Add(string.Format("Department name is missing-row no {0}", rowno));
        //        }

        //        if (_regmodel.DepartmentName != null && !IsValidLength(_regmodel.DepartmentName, 150))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid Department Name more than 150 char-row no {0}", rowno));
        //        }

        //        if (_model.Iqama == null)
        //        {
        //            _errormsg.Add(string.Format("Iqama is missing-row no {0}", rowno));
        //        }

        //        if (_model.Iqama != null && !IsValidLength(_model.Iqama, 18))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid NIN or Iqama No more than 18 char-row no {0}", rowno));
        //        }

        //        if (_model.BeneficiaryName == null)
        //        {
        //            _errormsg.Add(string.Format("Beneficiary name is missing-row no {0}", rowno));
        //        }

        //        if (_model.BeneficiaryName != null)
        //        {
        //            isemptyrow = false;
        //            if (!IsValidLength(_model.BeneficiaryName, 35))
        //            {
        //                _errormsg.Add(string.Format("Invalid Beneficiary Name more than 35 char-row no {0}", rowno));
        //            }

        //            // _errormsg = isvalidString(_model.BeneficiaryName, _errormsg, string.Format("Beneficiary Name-row no {0}", rowno));
        //        }

        //        if (_model.BankCode == null)
        //        {
        //            _errormsg.Add(string.Format("BankCode is missing-row no {0}", rowno));
        //        }
        //        if (_model.BankCode != null && _model.BankCode.Length != 11)
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("BankCode should be 11 chars-row no {0}", rowno));
        //        }

        //        if (_model.BeneficiaryRef == null)
        //        {
        //            _errormsg.Add(string.Format("Beneficiary Ref is missing-row no {0}", rowno));
        //        }

        //        if (_model.BeneficiaryRef != null && !IsValidLength(_model.BeneficiaryRef, 12))
        //        {
        //            isemptyrow = false;
        //            _errormsg.Add(string.Format("Invalid Beneficiary Ref more than 12 char-row no {0}", rowno));
        //        }

        //        if (isemptyrow)
        //        {
        //            _errormsg.Add(string.Format("Empty row -{0}", rowno));
        //        }
        //    }

        //    return _errormsg;
        //}

        //private List<string> RSDHeaderValidation(Registration _regmodel, List<string> _errormsg)
        //{

        //    if (_regmodel.CompanyName == null)
        //    {
        //        _errormsg.Add("Company name is missing");
        //    }

        //    if (_regmodel.CompanyName != null)
        //    {
        //        if (!IsValidLength(_regmodel.CompanyName, 150))
        //        {
        //            _errormsg.Add("Invalid Agency name should be more than 150 chars");
        //        }
        //    }

        //    if (_regmodel.DivisionNumber == null)
        //    {
        //        _errormsg.Add("Division number is missing");
        //    }
        //    if (_regmodel.DivisionNumber != null)
        //    {
        //        if (!IsNumeric(_regmodel.DivisionNumber))
        //        {
        //            _errormsg.Add("Invalid Division Number not numeric");
        //        }
        //        if (!IsValidLength(_regmodel.DivisionNumber, 3))
        //        {
        //            _errormsg.Add("Invalid Division Number more than 3 digits");
        //        }
        //    }

        //    if (_regmodel.SubDivisionNumber == null)
        //    {
        //        _errormsg.Add("Sub division number missing");
        //    }

        //    if (_regmodel.SubDivisionNumber != null && !IsNumeric(_regmodel.SubDivisionNumber))
        //    {
        //        _errormsg.Add("Invalid Sub-Division Number not numeric");
        //    }

        //    if (_regmodel.SectorNumber == null)
        //    {
        //        _errormsg.Add("Sector number missing");
        //    }
        //    if (_regmodel.SectorNumber != null)
        //    {
        //        if (!IsNumeric(_regmodel.SectorNumber))
        //        {
        //            _errormsg.Add("Invalid Sector Number not numeric");
        //        }
        //        if (!IsValidLength(_regmodel.SectorNumber, 2))
        //        {
        //            _errormsg.Add("Invalid Sector Number more than 2 digits");
        //        }
        //    }

        //    if (_regmodel.ClassNumber == null)
        //    {
        //        _errormsg.Add("Class number missing");
        //    }
        //    if (_regmodel.ClassNumber != null)
        //    {
        //        if (!IsNumeric(_regmodel.ClassNumber))
        //        {
        //            _errormsg.Add("Invalid Class Number not numeric");
        //        }
        //        if (!IsValidLength(_regmodel.ClassNumber, 3))
        //        {
        //            _errormsg.Add("Invalid Class Number more than 3 digits");
        //        }
        //    }

        //    if (_regmodel.PaymentOrderNumber == null)
        //    {
        //        _errormsg.Add("Payment order number missing");
        //    }

        //    if (_regmodel.PaymentOrderNumber != null && !IsValidLength(_regmodel.PaymentOrderNumber, 15))
        //    {
        //        _errormsg.Add("Invalid Payment Order No more than 15 digits");
        //    }

        //    if (_regmodel.LabelMsg != null && !IsValidLength(_regmodel.LabelMsg, 35))
        //    {
        //        _errormsg.Add("Invalid Label more than 35 digits");
        //    }

        //    if (_regmodel.DepartmentName == null)
        //    {
        //        _errormsg.Add("Department name is missing");
        //    }

        //    if (_regmodel.DepartmentName != null && !IsValidLength(_regmodel.DepartmentName, 150))
        //    {
        //        _errormsg.Add("Invalid Department Name more than 150 char");
        //    }
        //    return _errormsg;
        //}

        //public List<string> validateHAgreementCode(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        //{
        //    isvalid = false;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(prmStr))
        //        {
        //            _msg.Add(string.Format("Agreement Code is empty - row no {0}", rowno));
        //        }
        //        else if (prmStr.Length > 8)
        //        {
        //            _msg.Add(string.Format("Invalid Agreement Code more than 8 char - row no {0}", rowno));
        //        }
        //        else
        //        {
        //            isvalidString(prmStr, _msg, "Agreement Code", rowno, out isvalid);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        isvalid = false;
        //        _msg.Add(string.Format("Invalid funding account no, please check-row no {0}", rowno));
        //    }

        //    return _msg;
        //}

        //public List<string> validateHFundingAccount(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        //{
        //    isvalid = false;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(prmStr))
        //        {
        //            _msg.Add(string.Format("Funding Account No is empty-row no {0}", rowno));
        //        }

        //        prmStr = Convert.ToString(Decimal.Parse(prmStr, System.Globalization.NumberStyles.Any));

        //        if (!IsNumeric(prmStr))
        //        {
        //            _msg.Add(string.Format("Funding Account not numeric-row no {0}", rowno));
        //        }
        //        else if (prmStr.Length != 13)
        //        {
        //            _msg.Add(string.Format("Account number should be of 13 characters-row no {0}", rowno));
        //        }
        //        else
        //        {
        //            isvalid = true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        isvalid = false;
        //        _msg.Add(string.Format("Invalid funding account no, please check-row no {0}", rowno));
        //    }

        //    return _msg;
        //}

        //public List<string> validateHBranchNo(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        //{
        //    isvalid = false;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(prmStr))
        //        {
        //            _msg.Add(string.Format("Branch No is empty-row no {0}", rowno));
        //        }

        //        prmStr = Convert.ToString(Decimal.Parse(prmStr, System.Globalization.NumberStyles.Any));

        //        if (prmStr.Length != 3)
        //        {
        //            _msg.Add(string.Format("Branch No should be of 3 characters-row no {0}", rowno));
        //        }
        //        else
        //        {
        //            isvalid = true;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        isvalid = false;
        //        _msg.Add(string.Format("Invalid Branch No, please check-row no {0}", rowno));
        //    }

        //    return _msg;
        //}

        //public List<string> validateHBranchNoWithAC(string prmStr, string acNo, List<string> _msg, int rowno, out bool isvalid)
        //{
        //    isvalid = false;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(prmStr))
        //        {
        //            _msg.Add(string.Format("Branch No is empty-row no {0}", rowno));
        //        }
        //        else if (string.IsNullOrEmpty(acNo))
        //        {
        //            _msg.Add(string.Format("Invalid Funding Account No is empty-row no {0}", rowno));
        //        }
        //        else
        //        {
        //            string fundingacnt = acNo.Substring(0, 3);
        //            if (!(prmStr).Equals(fundingacnt))
        //            {
        //                _msg.Add(string.Format("Invalid Branch No , it must be as the first three digits of the Funding Account No-row no {0}", rowno));
        //            }
        //            else
        //            {
        //                isvalid = true;
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        isvalid = false;
        //        _msg.Add(string.Format("Invalid Branch No, please check-row no {0}", rowno));
        //    }

        //    return _msg;
        //}


        //#endregion

        //#region Upload Excel sheet for Non WBS and Non RSD

        //public List<string> ReadNonWBSRSDExcelSheet(string filePath)
        //{
        //    List<string> _result = new List<string>();
        //    try
        //    {
        //        Excel.Application xlApp = new Excel.Application();
        //        Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePath, 0, false, 5, "", "", false,
        //            Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
        //        Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

        //        List<clsNonWBS> nonwbsmodelList = new List<clsNonWBS>();
        //        clsNonWBS _nonwbsmodel;
        //        Excel.Range xlRange = xlWorkSheet.UsedRange;
        //        int rowNumber = 2;
        //        Registration _model = new Registration();

        //        try
        //        {
        //            _model.CustomerName = (string)(xlRange.Cells[rowNumber, 1] as Excel.Range).Value2;
        //            _model.AgremeentCode = (string)(xlRange.Cells[rowNumber, 2] as Excel.Range).Value2;
        //            _model.FundingAccount = (string)(xlRange.Cells[rowNumber, 3] as Excel.Range).Value2;
        //            _model.BranchNo = (string)(xlRange.Cells[rowNumber, 4] as Excel.Range).Value2;
        //            _model.CreditDate = (string)(xlRange.Cells[rowNumber, 5] as Excel.Range).Value2;
        //            _model.BankCode = (string)(xlRange.Cells[rowNumber, 6] as Excel.Range).Value2;
        //            _model.Currency = (string)(xlRange.Cells[rowNumber, 7] as Excel.Range).Value2;

        //            //Iterate the rows in the used range
        //            int srno = 1;
        //            for (int index = 4; index <= xlRange.Rows.Count; index++)
        //            {
        //                _nonwbsmodel = new clsNonWBS();
        //                _nonwbsmodel.SrNo = srno.ToString();

        //                _nonwbsmodel.BeneficiaryRef = Convert.ToString((xlRange.Cells[index, 2] as Excel.Range).Value2);

        //                if ((xlRange.Cells[index, 3] as Excel.Range).Value2 != null)
        //                {
        //                    _nonwbsmodel.EmployeeName = Convert.ToString((xlRange.Cells[index, 3] as Excel.Range).Value2);
        //                }

        //                if ((xlRange.Cells[index, 4] as Excel.Range).Value2 != null)
        //                {
        //                    var val = Convert.ToString(Convert.ToDouble((xlRange.Cells[index, 4] as Excel.Range).Value2));
        //                    _nonwbsmodel.AccountNumber = Convert.ToString(Decimal.Parse(val, System.Globalization.NumberStyles.Any));
        //                }

        //                if ((xlRange.Cells[index, 5] as Excel.Range).Value2 != null)
        //                {
        //                    _nonwbsmodel.BankCode = Convert.ToString((xlRange.Cells[index, 5] as Excel.Range).Value2);
        //                }

        //                if ((xlRange.Cells[index, 6] as Excel.Range).Value2 != null)
        //                {
        //                    _nonwbsmodel.Amount = Convert.ToDouble((xlRange.Cells[index, 6] as Excel.Range).Value2);
        //                }

        //                if ((xlRange.Cells[index, 7] as Excel.Range).Value2 != null)
        //                {
        //                    _nonwbsmodel.Currency = (string)(xlRange.Cells[index, 7] as Excel.Range).Value2;
        //                }

        //                _nonwbsmodel.Status = (string)(xlRange.Cells[index, 8] as Excel.Range).Value2;

        //                if ((xlRange.Cells[index, 9] as Excel.Range).Value2 != null)
        //                {
        //                    _nonwbsmodel.Address = (string)(xlRange.Cells[index, 9] as Excel.Range).Value2;
        //                }

        //                if ((xlRange.Cells[index, 10] as Excel.Range).Value2 != null)
        //                {
        //                    _nonwbsmodel.City = (string)(xlRange.Cells[index, 10] as Excel.Range).Value2;
        //                }
        //                if ((xlRange.Cells[index, 11] as Excel.Range).Value2 != null)
        //                {
        //                    _nonwbsmodel.ZipCode = Convert.ToDouble((xlRange.Cells[index, 11] as Excel.Range).Value2);
        //                }

        //                if ((xlRange.Cells[index, 12] as Excel.Range).Value2 != null)
        //                {
        //                    _nonwbsmodel.PaymentDesc = (string)(xlRange.Cells[index, 12] as Excel.Range).Value2;
        //                }
        //                nonwbsmodelList.Add(_nonwbsmodel);
        //                srno++;
        //            }

        //            xlWorkBook.Close();
        //            xlApp.Quit();

        //            Marshal.ReleaseComObject(xlWorkSheet);
        //            Marshal.ReleaseComObject(xlWorkBook);
        //            Marshal.ReleaseComObject(xlApp);
        //        }
        //        catch (Exception ex)
        //        {
        //            xlWorkBook.Close();
        //            xlApp.DisplayAlerts = false;
        //            xlApp.Quit();

        //            Marshal.ReleaseComObject(xlWorkSheet);
        //            Marshal.ReleaseComObject(xlWorkBook);
        //            Marshal.ReleaseComObject(xlApp);
        //            throw ex;
        //        }

        //        if (nonwbsmodelList.Any())
        //        {
        //            _result = CommonValidation(_model, _result);
        //            _result = nonWBSModelValidation_Upload(nonwbsmodelList, _model, _result);
        //        }
        //        else
        //        {
        //            _result.Add("File is not valid");
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return _result;
        //}

        //public List<string> nonWBSModelValidation_Upload(List<clsNonWBS> _modelList, Registration _regmodel, List<string> _errormsg)
        //{
        //    try
        //    {
        //        _errormsg = CommonValidation(_regmodel, _errormsg);
        //        _errormsg = CreditDateValidation(_regmodel.CreditDate, _errormsg, "Credit");
        //        _errormsg = CommonUploadValidation_NonType(_modelList, _errormsg);

        //        if (_errormsg.Count == 0)
        //        {
        //            double vTotPay = 0, vNegAmt = 0;
        //            int srno = 0;
        //            var dd = _regmodel.CreditDate.Split('/')[0];
        //            var mm = _regmodel.CreditDate.Split('/')[1];
        //            var yy = _regmodel.CreditDate.Split('/')[2];

        //            DateTime startdate = new DateTime(2005, 01, 01);
        //            DateTime vCreditDate = new DateTime(Convert.ToInt32(yy), Convert.ToInt32(mm), Convert.ToInt32(dd));
        //            var iJlnDt = (vCreditDate - startdate).TotalDays;


        //            var vTmpPayRef = _regmodel.AgremeentCode.Substring(0, 6) +
        //                 string.Format("{0:000}", iJlnDt.ToString().Substring(0, 3));

        //            string serialno = string.Format("{0:0000}", 1);
        //            string batchno = string.Format("{0:00}", 1);
        //            string filename = string.Format("{0}.txt", _regmodel.AgremeentCode + "-PR-" + yy + mm + dd + "-" + string.Format("0{0}", batchno));
        //            string filereference = "PAYROLL REF PR-" + string.Format("{0:0000}", _regmodel.BatchNo) + "-" + vCreditDate.ToString("yyyyMMdd") + DateTime.Now.ToString("HHmmss") + serialno;


        //            filereference = filereference.Trim();
        //            var length = filereference.Length;
        //            while (length < 8)
        //            {
        //                filereference = filereference + "X";
        //                length++;
        //            }

        //            string txtfilepath = Path.Combine(@"~\UserFile\" + filename);

        //            HttpContext.Cache.Insert("phfilepath", filename, null, DateTime.UtcNow.AddMinutes(10), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
        //            if (System.IO.File.Exists(Server.MapPath(txtfilepath)))
        //            {
        //                string path = Path.Combine(Server.MapPath(destfolderpath), filename);
        //                System.IO.File.Delete(path);
        //            }

        //            if (!System.IO.File.Exists(Server.MapPath(txtfilepath)))
        //            {
        //                using (var stream = new FileStream(Server.MapPath(txtfilepath), FileMode.OpenOrCreate, FileAccess.Write))
        //                {
        //                    srno = srno + 1;
        //                    string fieldToStr = "";
        //                    StreamWriter writer = new StreamWriter(stream);
        //                    writer.Write("000");

        //                    fieldToStr = _regmodel.AgremeentCode.Trim();
        //                    length = fieldToStr.Length;
        //                    writer.Write(fieldToStr);
        //                    while (length < 8)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    fieldToStr = yy + mm + dd;
        //                    length = fieldToStr.Length;
        //                    writer.Write(fieldToStr);
        //                    while (length < 8)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    length = filereference.Length;
        //                    writer.Write(filereference);
        //                    while (length < 35)
        //                    {
        //                        writer.Write("X");
        //                        length++;
        //                    }

        //                    writer.WriteLine("");

        //                    //----------------------------------------------------------
        //                    double totalNetAmount = 0;
        //                    foreach (var _model in _modelList)
        //                    {
        //                        writer.Write(112);

        //                        fieldToStr = string.Format("{0:000000}", srno);
        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 6)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }


        //                        var sPayRefNo = vTmpPayRef + string.Format("{0:00000}", srno);
        //                        fieldToStr = sPayRefNo.Trim();
        //                        if (fieldToStr.Length > 16)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 16);
        //                        }

        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 16)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.BeneficiaryRef;
        //                        if (fieldToStr.Length > 12)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 12);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 12)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.BankCode.Trim();
        //                        length = fieldToStr.Length;
        //                        if (length > 11)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 11);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 11)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.AccountNumber.ToString();
        //                        length = fieldToStr.Length;
        //                        if (length > 34)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 34);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 34)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = string.Format("{0:000000000000.00}", _model.Amount);
        //                        length = fieldToStr.Length;
        //                        if (length > 15)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 15);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 15)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }


        //                        fieldToStr = _model.Currency;
        //                        length = fieldToStr.Length;
        //                        if (length > 3)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 3);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 3)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.Address != null ? _model.Address : string.Empty;
        //                        length = fieldToStr.Length;
        //                        if (length > 35)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 35);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 35)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.City != null ? _model.City : string.Empty;
        //                        //fieldToStr = _model.City;
        //                        length = fieldToStr.Length;
        //                        if (length > 20)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 20);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 20)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.ZipCode != null ? _model.ZipCode.ToString() : string.Empty;
        //                        // fieldToStr = _model.ZipCode;
        //                        length = fieldToStr.Length;
        //                        if (length > 9)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 9);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 9)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.EmployeeName;
        //                        length = fieldToStr.Length;
        //                        if (length > 35)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 35);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 35)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.PaymentDesc != null ? _model.PaymentDesc : string.Empty;
        //                        length = fieldToStr.Length;
        //                        if (length > 139)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 139);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        length = fieldToStr.Length;
        //                        while (length < 139)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        writer.Write("X");
        //                        writer.WriteLine("");

        //                        totalNetAmount = totalNetAmount + Convert.ToDouble(string.Format("{0:000000000000.00}", _model.Amount));
        //                        srno++;
        //                    }

        //                    writer.Write(string.Format("{0}{1}{2}", 999, string.Format("{0:000000000000000.00}", totalNetAmount), string.Format("{0:000000}", srno)));
        //                    writer.Close();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _errormsg.Add(ex.Message);
        //    }

        //    return _errormsg;
        //}

        //#endregion

        //#region Upload Excel sheet and generate text file for WBS     



        //public List<string> ReadWBSExcelSheet(string filePath)
        //{
        //    List<string> _result = new List<string>();
        //    Excel.Application xlApp = new Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePath, 0, false, 5, "", "", false,
        //        Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
        //    Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        //    try
        //    {
        //        Excel.Range xlRange = xlWorkSheet.UsedRange;
        //        int rowNumber = 2;
        //        Registration _model = new Registration();
        //        List<clsWBS> wbsmodelList = new List<clsWBS>();
        //        clsWBS _wbsmodel;
        //        try
        //        {

        //            //Microsoft.Office.Interop.Excel.Range myrange = xlWorkSheet.get_Range(/*your column letter, ex: "A" or "B"*/ +i.ToString(), System.Reflection.Missing.Value);
        //            // string temp = myrange.Text;
        //            bool isvalid = false;

        //            _model.CustomerName = (string)(xlRange.Cells[rowNumber, 1] as Excel.Range).Text;


        //            dynamic agreementcode = (xlRange.Cells[rowNumber, 2] as Excel.Range).Text;
        //            _result = validateHAgreementCode(agreementcode, _result, rowNumber, out isvalid);
        //            if (isvalid)
        //            {
        //                _model.AgremeentCode = Convert.ToString(agreementcode);
        //            }

        //            dynamic FundingAccount = (xlRange.Cells[rowNumber, 3] as Excel.Range).Text;
        //            _result = validateHFundingAccount(FundingAccount, _result, rowNumber, out isvalid);
        //            if (isvalid)
        //            {
        //                _model.FundingAccount = Convert.ToString(FundingAccount);
        //            }

        //            //dynamic agreementcode = (xlRange.Cells[rowNumber, 2] as Excel.Range).Text;
        //            //_result = validateHAgreementCode(agreementcode, _result, rowNumber, out isvalid);
        //            //if (isvalid)
        //            //{
        //            //    _model.AgremeentCode = Convert.ToString(agreementcode);
        //            //}

        //            dynamic branchNo = (xlRange.Cells[rowNumber, 4] as Excel.Range).Text;
        //            _result =validateHBranchNo(branchNo, _result, rowNumber, out isvalid);
        //            if (isvalid)
        //            {
        //                _result = validateHBranchNoWithAC(branchNo, FundingAccount,_result, rowNumber, out isvalid);
        //                if(isvalid)
        //                {
        //                    _model.BranchNo = Convert.ToString(FundingAccount);
        //                }                       
        //            }



        //            //  _model.AgremeentCode = (string)(xlRange.Cells[rowNumber, 2] as Excel.Range).Text;

        //            //_model.FundingAccount = (string)(xlRange.Cells[rowNumber, 3] as Excel.Range).Text;


        //            //_model.BranchNo = (string)(xlRange.Cells[rowNumber, 4] as Excel.Range).Text;
        //            _model.CreditDate = (string)(xlRange.Cells[rowNumber, 5] as Excel.Range).Text;
        //            _model.MinsOfLabEstablishedId = (string)(xlRange.Cells[rowNumber, 6] as Excel.Range).Text;
        //            _model.CommerceRoomId = (string)(xlRange.Cells[rowNumber, 7] as Excel.Range).Text;
        //            _model.BankCode = (string)(xlRange.Cells[rowNumber, 8] as Excel.Range).Text;
        //            _model.Currency = (string)(xlRange.Cells[rowNumber, 9] as Excel.Range).Text;

        //            //Iterate the rows in the used range
        //            int srno = 1;
        //            for (int index = 4; index <= xlRange.Rows.Count; index++)
        //            {
        //                //   bool isvalid = false;
        //                _wbsmodel = new clsWBS();
        //                _wbsmodel.SrNo = srno.ToString();

        //                _wbsmodel.BeneficiaryId = Convert.ToString((xlRange.Cells[index, 2] as Excel.Range).Text);

        //                if ((xlRange.Cells[index, 3] as Excel.Range).Value2 != null)
        //                {
        //                    _wbsmodel.EmployeeName = Convert.ToString((xlRange.Cells[index, 3] as Excel.Range).Text);
        //                }

        //                if ((xlRange.Cells[index, 4] as Excel.Range).Text != null)
        //                {
        //                    _result = validateHFundingAccount((xlRange.Cells[index, 4] as Excel.Range).Text, _result, index, out isvalid);
        //                    if (isvalid)
        //                    {
        //                        _wbsmodel.AccountNumber = Convert.ToString(Decimal.Parse((xlRange.Cells[index, 4] as Excel.Range).Text, System.Globalization.NumberStyles.Any));
        //                    }
        //                }

        //                if ((xlRange.Cells[index, 5] as Excel.Range).Value2 != null)
        //                {
        //                    _wbsmodel.BankCode = (string)(xlRange.Cells[index, 5] as Excel.Range).Value2;
        //                }

        //                if ((xlRange.Cells[index, 6] as Excel.Range).Value2 != null)
        //                {
        //                    _wbsmodel.NetAmount = Convert.ToDouble((xlRange.Cells[index, 6] as Excel.Range).Value2);
        //                }

        //                if ((xlRange.Cells[index, 7] as Excel.Range).Value2 != null)
        //                {
        //                    _wbsmodel.BasicSalary = Convert.ToDouble((xlRange.Cells[index, 7] as Excel.Range).Value2);
        //                }

        //                if ((xlRange.Cells[index, 8] as Excel.Range).Value2 != null)
        //                {
        //                    _wbsmodel.HousingAllowance = Convert.ToDouble((xlRange.Cells[index, 8] as Excel.Range).Value2);
        //                }

        //                if ((xlRange.Cells[index, 9] as Excel.Range).Value2 != null)
        //                {
        //                    _wbsmodel.OtherEarnings = Convert.ToDouble((xlRange.Cells[index, 9] as Excel.Range).Value2);
        //                }

        //                if ((xlRange.Cells[index, 10] as Excel.Range).Value2 != null)
        //                {
        //                    _wbsmodel.Deductions = Convert.ToDouble((xlRange.Cells[index, 10] as Excel.Range).Value2);
        //                }

        //                if ((xlRange.Cells[index, 11] as Excel.Range).Value2 != null)
        //                {
        //                    _wbsmodel.Address = (string)(xlRange.Cells[index, 11] as Excel.Range).Value2;
        //                }

        //                _wbsmodel.Status = (string)(xlRange.Cells[index, 12] as Excel.Range).Value2;


        //                if ((xlRange.Cells[index, 13] as Excel.Range).Value2 != null)
        //                {
        //                    _wbsmodel.PaymentDesc = (string)(xlRange.Cells[index, 13] as Excel.Range).Value2;
        //                }

        //                if ((xlRange.Cells[index, 14] as Excel.Range).Value2 != null)
        //                {
        //                    _wbsmodel.PaymentRef = (string)(xlRange.Cells[index, 14] as Excel.Range).Value2;
        //                }

        //                if ((xlRange.Cells[index, 15] as Excel.Range).Value2 != null)
        //                {
        //                    _wbsmodel.Currency = (string)(xlRange.Cells[index, 15] as Excel.Range).Value2;
        //                }
        //                wbsmodelList.Add(_wbsmodel);
        //                srno++;
        //            }

        //            xlWorkBook.Close();
        //            xlApp.Quit();

        //            Marshal.ReleaseComObject(xlWorkSheet);
        //            Marshal.ReleaseComObject(xlWorkBook);
        //            Marshal.ReleaseComObject(xlApp);
        //        }
        //        catch (Exception ex)
        //        {
        //            xlWorkBook.Close();
        //            xlApp.Quit();

        //            Marshal.ReleaseComObject(xlWorkSheet);
        //            Marshal.ReleaseComObject(xlWorkBook);
        //            Marshal.ReleaseComObject(xlApp);
        //            throw ex;
        //        }

        //        if (wbsmodelList != null && wbsmodelList.Any() && _result.Count == 0)
        //        {
        //            _result = CommonValidation(_model, _result);
        //            _result = CreditDateValidation(_model.CreditDate, _result, "Credit");
        //            _result = WBSUploadValidation(wbsmodelList, _result);

        //            if (_result.Count == 0)
        //            {
        //                _result = WBS_GenerateTextFile(wbsmodelList, _model, _result);
        //            }
        //        }
        //        else if (_result.Count > 0)
        //        {
        //            // _result.Add("Invalid file");
        //        }
        //        else
        //        {
        //            _result.Add("Invalid file");
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return _result;
        //}

        //public List<string> WBS_GenerateTextFile(List<clsWBS> _modelList, Registration _regmodel, List<string> _errormsg)
        //{
        //    try
        //    {
        //        int srno = 0;
        //        var dd = _regmodel.CreditDate.Split('/')[0];
        //        var mm = _regmodel.CreditDate.Split('/')[1];
        //        var yy = _regmodel.CreditDate.Split('/')[2];

        //        DateTime startdate = new DateTime(2005, 01, 01);
        //        DateTime vCreditDate = new DateTime(Convert.ToInt32(yy), Convert.ToInt32(mm), Convert.ToInt32(dd));
        //        var iJlnDt = (vCreditDate - startdate).TotalDays;
        //        string serialno = string.Format("{0:0000}", 1);
        //        string batchno = string.Format("{0:00}", 1);
        //        string filename = string.Format("{0}.txt", _regmodel.AgremeentCode + "-PR-" + yy + mm + dd + "-" + string.Format("0{0}", batchno));
        //        string filereference = "PAYROLLREF-PR-" + string.Format("{0:0000}", _regmodel.BatchNo) + "-" + DateTime.Now.ToString("yyyyMMdd") + DateTime.Now.ToString("HHmmss") + serialno;
        //        string txtfilepath = Path.Combine(@"~\UserFile\" + filename);
        //        HttpContext.Cache.Insert("phfilepath", filename, null, DateTime.UtcNow.AddMinutes(10), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
        //        if (System.IO.File.Exists(Server.MapPath(txtfilepath)))
        //        {
        //            string path = Path.Combine(Server.MapPath(destfolderpath), filename);
        //            System.IO.File.Delete(path);
        //        }

        //        var vTmpPayRef = _regmodel.AgremeentCode.Substring(_regmodel.AgremeentCode.Length - 6, 6) +
        //             string.Format("{0:00000}", iJlnDt);

        //        if (!System.IO.File.Exists(Server.MapPath(txtfilepath)))
        //        {
        //            using (var stream = new FileStream(Server.MapPath(txtfilepath), FileMode.OpenOrCreate, FileAccess.Write))
        //            {
        //                srno = srno + 1;
        //                string fieldToStr = "";
        //                StreamWriter writer = new StreamWriter(stream);
        //                writer.Write("111");

        //                fieldToStr = _regmodel.AgremeentCode.Trim();
        //                var length = fieldToStr.Length;
        //                writer.Write(fieldToStr);
        //                while (length < 8)
        //                {
        //                    writer.Write(" ");
        //                    length++;
        //                }

        //                fieldToStr = yy + mm + dd;
        //                length = fieldToStr.Length;
        //                writer.Write(fieldToStr);
        //                while (length < 8)
        //                {
        //                    writer.Write(" ");
        //                    length++;
        //                }

        //                length = filereference.Length;
        //                writer.Write(filereference);
        //                while (length < 35)
        //                {
        //                    writer.Write(" ");
        //                    length++;
        //                }

        //                fieldToStr = _regmodel.CommerceRoomId;
        //                length = fieldToStr.Length;
        //                writer.Write(fieldToStr);
        //                while (length < 10)
        //                {
        //                    writer.Write(" ");
        //                    length++;
        //                }


        //                fieldToStr = _regmodel.BankCode;
        //                length = fieldToStr.Length;
        //                writer.Write(fieldToStr);
        //                while (length < 4)
        //                {
        //                    writer.Write(" ");
        //                    length++;
        //                }

        //                fieldToStr = _regmodel.FundingAccount;
        //                length = fieldToStr.Length;
        //                writer.Write(fieldToStr);
        //                while (length < 24)
        //                {
        //                    writer.Write(" ");
        //                    length++;
        //                }

        //                fieldToStr = _regmodel.Currency;
        //                length = fieldToStr.Length;
        //                writer.Write(fieldToStr);
        //                while (length < 3)
        //                {
        //                    writer.Write(" ");
        //                    length++;
        //                }

        //                fieldToStr = _regmodel.MinsOfLabEstablishedId;
        //                int _length = fieldToStr.Length;
        //                if (fieldToStr.Substring(1, 1) == "0")
        //                {
        //                    writer.Write(" ");
        //                    fieldToStr = fieldToStr.Substring(2, _length - 1);
        //                    writer.Write(fieldToStr);
        //                    while (_length < 18)
        //                    {
        //                        writer.Write(" ");
        //                        _length++;
        //                    }
        //                }
        //                else
        //                {
        //                    writer.Write(fieldToStr);
        //                    while (_length < 18)
        //                    {
        //                        writer.Write(" ");
        //                        _length++;
        //                    }
        //                }

        //                writer.WriteLine("");

        //                double totalNetAmount = 0;
        //                foreach (var _model in _modelList)
        //                {
        //                    writer.Write(112);

        //                    fieldToStr = string.Format("{0:000000}", srno);
        //                    writer.Write(fieldToStr);
        //                    length = fieldToStr.Length;
        //                    while (length < 6)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    fieldToStr = _model.PaymentRef;
        //                    length = fieldToStr.Length;
        //                    if (length > 16)
        //                    {
        //                        fieldToStr = fieldToStr.Substring(0, 16);
        //                    }
        //                    writer.Write(fieldToStr);
        //                    length = fieldToStr.Length;
        //                    while (length < 16)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    fieldToStr = _model.BeneficiaryId;
        //                    if (fieldToStr.Length > 10)
        //                    {
        //                        fieldToStr = fieldToStr.Substring(0, 10);
        //                    }
        //                    writer.Write(fieldToStr);
        //                    length = fieldToStr.Length;
        //                    while (length < 10)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    writer.Write("             ");

        //                    fieldToStr = _model.AccountNumber.ToString();
        //                    length = fieldToStr.Length;
        //                    if (length > 34)
        //                    {
        //                        fieldToStr = fieldToStr.Substring(0, 34);
        //                    }
        //                    writer.Write(fieldToStr);
        //                    length = fieldToStr.Length;
        //                    while (length < 34)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    fieldToStr = string.Format("{0:000000000000.00}", _model.NetAmount);
        //                    length = fieldToStr.Length;
        //                    if (length > 15)
        //                    {
        //                        fieldToStr = fieldToStr.Substring(0, 15);
        //                    }
        //                    writer.Write(fieldToStr);
        //                    length = fieldToStr.Length;
        //                    while (length < 15)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }


        //                    fieldToStr = _model.Currency;
        //                    length = fieldToStr.Length;
        //                    if (length > 3)
        //                    {
        //                        fieldToStr = fieldToStr.Substring(0, 3);
        //                    }
        //                    writer.Write(fieldToStr);
        //                    length = fieldToStr.Length;
        //                    while (length < 3)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }
        //                    writer.Write("                                                  ");//50 spaces
        //                    writer.Write("                                                  ");//49 spaces


        //                    fieldToStr = _model.PaymentDesc != null ? _model.PaymentDesc : string.Empty;
        //                    length = fieldToStr.Length;
        //                    if (length > 140)
        //                    {
        //                        fieldToStr = fieldToStr.Substring(0, 140);
        //                    }
        //                    writer.Write(fieldToStr);
        //                    length = fieldToStr.Length;
        //                    while (length < 140)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }


        //                    fieldToStr = _model.EmployeeName;
        //                    length = fieldToStr.Length;
        //                    if (length > 35)
        //                    {
        //                        fieldToStr = fieldToStr.Substring(0, 35);
        //                    }
        //                    writer.Write(fieldToStr);
        //                    length = fieldToStr.Length;
        //                    while (length < 35)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    fieldToStr = _model.Address != null ? _model.Address : string.Empty;
        //                    length = fieldToStr.Length;
        //                    if (length > 105)
        //                    {
        //                        fieldToStr = fieldToStr.Substring(0, 105);
        //                    }
        //                    writer.Write(fieldToStr);
        //                    length = fieldToStr.Length;
        //                    while (length < 105)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    fieldToStr = _model.BankCode.Trim();
        //                    length = fieldToStr.Length;
        //                    if (length > 11)
        //                    {
        //                        fieldToStr = fieldToStr.Substring(0, 11);
        //                    }
        //                    writer.Write(fieldToStr);
        //                    length = fieldToStr.Length;
        //                    while (length < 11)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    writer.Write("                                                  ");//50 spaces
        //                    writer.Write("                                                  ");//50 spaces
        //                    writer.Write("                             ");//29 spaces


        //                    fieldToStr = string.Format("{0:000000000.00}", _model.BasicSalary);
        //                    length = fieldToStr.Length;

        //                    writer.Write(fieldToStr);
        //                    while (length < 12)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }


        //                    fieldToStr = string.Format("{0:000000000.00}", _model.HousingAllowance);
        //                    length = fieldToStr.Length;
        //                    if (length > 12)
        //                    {
        //                        fieldToStr = fieldToStr.Substring(0, 12);
        //                    }

        //                    writer.Write(fieldToStr);
        //                    length = fieldToStr.Length;
        //                    while (length < 12)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    fieldToStr = string.Format("{0:000000000.00}", _model.OtherEarnings);
        //                    length = fieldToStr.Length;

        //                    writer.Write(fieldToStr);
        //                    while (length < 12)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }


        //                    fieldToStr = string.Format("{0:000000000.00}", _model.Deductions);
        //                    length = fieldToStr.Length;

        //                    writer.Write(fieldToStr);
        //                    while (length < 12)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    writer.Write("                                 ");//33 spaces
        //                    writer.WriteLine("");

        //                    totalNetAmount = totalNetAmount + Convert.ToDouble(string.Format("{0:000000000000.00}", _model.NetAmount));
        //                    srno++;
        //                }
        //                writer.WriteLine();
        //                writer.Write(string.Format("{0}{1}{2}", 999, string.Format("{0:000000000000000.00}", totalNetAmount), string.Format("{0:000000}", srno)));
        //                writer.Close();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _errormsg.Add(ex.Message);
        //    }

        //    return _errormsg;
        //}

        //#endregion

        //#region Upload Excel sheet and generate text file for RSD

        //public List<string> ReadRSDExcelSheet(string filePath)
        //{
        //    List<string> _result = new List<string>();
        //    Excel.Application xlApp = new Excel.Application();
        //    Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePath, 0, false, 5, "", "", false,
        //        Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
        //    Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

        //    List<clsRSD> rsdmodelList = new List<clsRSD>();
        //    clsRSD _rsdmodel;
        //    Registration _model = new Registration();
        //    int rowNumber = 2;
        //    Excel.Range xlRange = xlWorkSheet.UsedRange;

        //    try
        //    {

        //        _model.CompanyName = (string)(xlRange.Cells[rowNumber, 1] as Excel.Range).Value2;
        //        _model.AgremeentCode = (string)(xlRange.Cells[rowNumber, 2] as Excel.Range).Value2;
        //        _model.FundingAccount = (string)(xlRange.Cells[rowNumber, 3] as Excel.Range).Value2;
        //        _model.BranchNo = (string)(xlRange.Cells[rowNumber, 4] as Excel.Range).Value2;
        //        _model.ValueDate = (string)(xlRange.Cells[rowNumber, 5] as Excel.Range).Value2;
        //        _model.DepartmentName = (string)(xlRange.Cells[rowNumber, 6] as Excel.Range).Value2;
        //        _model.DivisionNumber = (string)(xlRange.Cells[rowNumber, 7] as Excel.Range).Value2;
        //        _model.SubDivisionNumber = (string)(xlRange.Cells[rowNumber, 8] as Excel.Range).Value2;
        //        _model.SectorNumber = (string)(xlRange.Cells[rowNumber, 9] as Excel.Range).Value2;
        //        _model.ClassNumber = (string)(xlRange.Cells[rowNumber, 10] as Excel.Range).Value2;
        //        _model.PaymentOrderNumber = (string)(xlRange.Cells[rowNumber, 11] as Excel.Range).Value2;
        //        _model.LabelMsg = (string)(xlRange.Cells[rowNumber, 12] as Excel.Range).Value2;
        //        _model.BankCode = (string)(xlRange.Cells[rowNumber, 13] as Excel.Range).Value2;
        //        _model.Currency = (string)(xlRange.Cells[rowNumber, 14] as Excel.Range).Value2;

        //        //Iterate the rows in the used range
        //        int srno = 1;
        //        for (int index = 4; index <= xlRange.Rows.Count; index++)
        //        {
        //            _rsdmodel = new clsRSD();
        //            _rsdmodel.SrNo = srno.ToString();

        //            _rsdmodel.BeneficiaryRef = (string)(xlRange.Cells[index, 2] as Excel.Range).Value2;

        //            if ((xlRange.Cells[index, 3] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.Iqama = Convert.ToString((xlRange.Cells[index, 3] as Excel.Range).Value2);
        //            }

        //            if ((xlRange.Cells[index, 4] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.BeneficiaryName = (string)(xlRange.Cells[index, 4] as Excel.Range).Value2;
        //            }

        //            if ((xlRange.Cells[index, 5] as Excel.Range).Value2 != null)
        //            {
        //                //   var val = Convert.ToString(Convert.ToDouble((xlRange.Cells[index, 5] as Excel.Range).Value2));
        //                var val = Convert.ToString(Convert.ToDouble((xlRange.Cells[index, 5] as Excel.Range).Value2));
        //                _rsdmodel.AccountNumber = Convert.ToString(Decimal.Parse(val, System.Globalization.NumberStyles.Any));
        //                //_rsdmodel.AccountNumber = Convert.ToDouble((xlRange.Cells[index, 5] as Excel.Range).Value2);
        //            }

        //            if ((xlRange.Cells[index, 6] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.BankCode = (string)(xlRange.Cells[index, 6] as Excel.Range).Value2;
        //            }

        //            if ((xlRange.Cells[index, 7] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.NetAmount = Convert.ToDouble((xlRange.Cells[index, 7] as Excel.Range).Value2);
        //            }

        //            if ((xlRange.Cells[index, 8] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.BasicSalary = Convert.ToDouble((xlRange.Cells[index, 8] as Excel.Range).Value2);
        //            }

        //            if ((xlRange.Cells[index, 9] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.OtherEarnings = Convert.ToDouble((xlRange.Cells[index, 9] as Excel.Range).Value2);
        //            }

        //            if ((xlRange.Cells[index, 10] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.Deductions = Convert.ToDouble((xlRange.Cells[index, 10] as Excel.Range).Value2);
        //            }

        //            if ((xlRange.Cells[index, 11] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.AccrualMonth = Convert.ToString((double)(xlRange.Cells[index, 11] as Excel.Range).Value2);
        //            }

        //            if ((xlRange.Cells[index, 12] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.PaymentDesc = (string)(xlRange.Cells[index, 12] as Excel.Range).Value2;
        //            }
        //            if ((xlRange.Cells[index, 13] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.PaymentRef = (string)(xlRange.Cells[index, 13] as Excel.Range).Value2;
        //            }
        //            if ((xlRange.Cells[index, 14] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.Currency = (string)(xlRange.Cells[index, 14] as Excel.Range).Value2;
        //            }
        //            if ((xlRange.Cells[index, 15] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.PaymentRef = (string)(xlRange.Cells[index, 15] as Excel.Range).Value2;
        //            }

        //            _rsdmodel.City = (string)(xlRange.Cells[index, 16] as Excel.Range).Value2;
        //            if ((xlRange.Cells[index, 17] as Excel.Range).Value2 != null)
        //            {
        //                _rsdmodel.ZipCode = (double)(xlRange.Cells[index, 17] as Excel.Range).Value2;
        //            }

        //            _rsdmodel.Apprentice = (string)(xlRange.Cells[index, 18] as Excel.Range).Value2;
        //            _rsdmodel.CareerLadder = (string)(xlRange.Cells[index, 19] as Excel.Range).Value2;
        //            _rsdmodel.Nationality = (string)(xlRange.Cells[index, 20] as Excel.Range).Value2;
        //            _rsdmodel.Status = (string)(xlRange.Cells[index, 21] as Excel.Range).Value2;
        //            _rsdmodel.Remarks = (string)(xlRange.Cells[index, 22] as Excel.Range).Value2;
        //            rsdmodelList.Add(_rsdmodel);
        //            srno++;
        //        }

        //        xlWorkBook.Close();
        //        xlApp.Quit();

        //        Marshal.ReleaseComObject(xlWorkSheet);
        //        Marshal.ReleaseComObject(xlWorkBook);
        //        Marshal.ReleaseComObject(xlApp);

        //        //_result = ModelValidation(_model);
        //        //_result = RSDModelValidation(rsdmodelList, _model, _result);
        //    }
        //    catch (Exception ex)
        //    {
        //        xlWorkBook.Close();
        //        xlApp.DisplayAlerts = false;
        //        xlApp.Quit();

        //        Marshal.ReleaseComObject(xlWorkSheet);
        //        Marshal.ReleaseComObject(xlWorkBook);
        //        Marshal.ReleaseComObject(xlApp);
        //        throw ex;
        //    }

        //    if (rsdmodelList != null && rsdmodelList.Any())
        //    {
        //        _result = CommonValidation(_model, _result);
        //        _result = CreditDateValidation(_model.ValueDate, _result, "Credit");
        //        _result = RSDUploadValidation(rsdmodelList, _model, _result);

        //        if (_result.Count == 0)
        //        {
        //            _result = RSD_GenerateTextFile(rsdmodelList, _model, _result);
        //        }
        //    }
        //    else
        //    {
        //        _result.Add("Invalid file");
        //    }




        //    return _result;
        //}

        //public List<string> RSD_GenerateTextFile(List<clsRSD> _modelList, Registration _regmodel, List<string> _errormsg)
        //{
        //    try
        //    {
        //        _errormsg = RSDUploadValidation(_modelList, _regmodel, _errormsg);
        //        if (_errormsg.Count == 0)
        //        {
        //            var dd = _regmodel.ValueDate.Split('/')[0];
        //            var mm = _regmodel.ValueDate.Split('/')[1];
        //            var yy = _regmodel.ValueDate.Split('/')[2];
        //            string serialno = string.Format("{0:0000}", 1);
        //            string batchno = string.Format("{0:00}", 1);
        //            string filename = string.Format("{0}.txt", _regmodel.AgremeentCode + "-PR-" + yy + mm + dd + "-" + string.Format("0{0}", batchno));
        //            string filereference = "PAYROLLREF-PR-" + string.Format("{0:0000}", _regmodel.BatchNo) + "-" + DateTime.Now.ToString("yyyyMMdd") + DateTime.Now.ToString("HHmmss") + serialno;
        //            string txtfilepath = Path.Combine(@"~\UserFile\" + filename);
        //            HttpContext.Cache.Insert("phfilepath", filename, null, DateTime.UtcNow.AddMinutes(10), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
        //            if (System.IO.File.Exists(Server.MapPath(txtfilepath)))
        //            {
        //                string path = Path.Combine(Server.MapPath(destfolderpath), filename);
        //                System.IO.File.Delete(path);
        //            }
        //            if (!System.IO.File.Exists(Server.MapPath(txtfilepath)))
        //            {
        //                using (var stream = new FileStream(Server.MapPath(txtfilepath), FileMode.OpenOrCreate, FileAccess.Write))
        //                {
        //                    StreamWriter writer = new StreamWriter(stream);
        //                    writer.Write("333");
        //                    writer.Write(_regmodel.AgremeentCode);
        //                    var length = _regmodel.AgremeentCode.Length;

        //                    while (length < 8)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    string newdate = yy + mm + dd;
        //                    writer.Write(newdate);

        //                    length = newdate.Length;
        //                    while (length < 8)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    writer.Write(_regmodel.LabelMsg);
        //                    length = _regmodel.LabelMsg != null ? _regmodel.LabelMsg.Length : 0;
        //                    while (length < 35)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    writer.Write(_regmodel.CompanyName);
        //                    length = _regmodel.CompanyName.Length;
        //                    while (length < 150)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    writer.Write(_regmodel.DepartmentName);
        //                    length = _regmodel.DepartmentName.Length;
        //                    while (length < 150)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    writer.Write(_regmodel.FundingAccount);
        //                    length = _regmodel.FundingAccount.Length;
        //                    while (length < 34)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    writer.Write(_regmodel.PaymentOrderNumber);
        //                    length = _regmodel.PaymentOrderNumber.Length;
        //                    while (length < 15)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    string _SectorNumber = string.Format("{0:00}", _regmodel.SectorNumber);
        //                    writer.Write(_SectorNumber);
        //                    length = _SectorNumber.Length;
        //                    while (length < 2)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    string _ClassNumber = string.Format("{0:000}", _regmodel.ClassNumber);
        //                    writer.Write(_ClassNumber);
        //                    length = _ClassNumber.Length;
        //                    while (length < 3)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    string _BranchNo = string.Format("{0:000}", _regmodel.BranchNo);
        //                    writer.Write(_BranchNo);
        //                    length = _BranchNo.Length;
        //                    while (length < 3)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    string _DivisionNumber = string.Format("{0:000}", _regmodel.DivisionNumber);
        //                    writer.Write(_DivisionNumber);
        //                    length = _DivisionNumber.Length;
        //                    while (length < 3)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    string _SubDivisionNumber = string.Format("{0:000}", _regmodel.SubDivisionNumber);
        //                    writer.Write(_SubDivisionNumber);
        //                    length = _SubDivisionNumber.Length;
        //                    while (length < 3)
        //                    {
        //                        writer.Write(" ");
        //                        length++;
        //                    }

        //                    int srno = 1;
        //                    double totalNetAmount = 0;
        //                    foreach (var _model in _modelList)
        //                    {
        //                        _model.SrNo = srno.ToString();
        //                        writer.WriteLine();

        //                        string rRecordNo = string.Format("{0:000000}", 112);
        //                        writer.Write(rRecordNo);
        //                        length = rRecordNo.Length;
        //                        while (length < 6)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        string fieldToStr = _model.PaymentRef;
        //                        length = fieldToStr.Length;
        //                        if (length > 16)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 16);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 16)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.BeneficiaryRef;
        //                        length = fieldToStr.Length;
        //                        if (length > 12)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 12);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 12)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.BankCode;
        //                        length = fieldToStr.Length;
        //                        if (length > 12)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 11);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 11)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.AccountNumber.ToString();
        //                        length = fieldToStr.Length;
        //                        if (length > 34)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 34);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 34)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = string.Format("{0:000000000.00}", _model.NetAmount);
        //                        length = fieldToStr.Length;
        //                        if (length > 15)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 15);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 15)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = string.Format("{0:000000000.00}", _model.NetAmount);
        //                        length = fieldToStr.Length;
        //                        if (length > 15)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 15);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 15)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }


        //                        fieldToStr = _model.Currency;
        //                        length = fieldToStr.Length;
        //                        if (length > 3)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 3);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 3)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.Address != null ? _model.Address : string.Empty;
        //                        length = fieldToStr.Length;
        //                        if (length > 35)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 35);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 35)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.City;
        //                        length = fieldToStr.Length;
        //                        if (length > 20)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 20);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 20)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.ZipCode != null ? _model.ZipCode.ToString() : "0";
        //                        length = fieldToStr.Length;
        //                        if (length > 9)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 9);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 9)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.BeneficiaryName;
        //                        length = fieldToStr.Length;
        //                        if (length > 35)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 35);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 35)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        length = 1;
        //                        while (length <= 200)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.PaymentDesc != null ? _model.PaymentDesc : string.Empty;
        //                        length = fieldToStr.Length;
        //                        if (length > 140)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 140);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 140)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.Iqama;
        //                        length = fieldToStr.Length;
        //                        if (length > 18)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 18);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 18)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.Nationality;
        //                        length = fieldToStr.Length;
        //                        if (length > 200)
        //                        {
        //                            fieldToStr = fieldToStr.Substring(0, 200);
        //                        }
        //                        writer.Write(fieldToStr);
        //                        while (length < 200)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = string.Format("{0:000000000000.00}", _model.BasicSalary);
        //                        length = fieldToStr.Length;

        //                        writer.Write(fieldToStr);
        //                        while (length < 15)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = string.Format("{0:000000000000.00}", _model.OtherEarnings);
        //                        length = fieldToStr.Length;

        //                        writer.Write(fieldToStr);
        //                        while (length < 15)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }


        //                        fieldToStr = string.Format("{0:000000000000.00}", _model.Deductions);
        //                        length = fieldToStr.Length;

        //                        writer.Write(fieldToStr);
        //                        while (length < 15)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.AccrualMonth != null ? _model.AccrualMonth : string.Empty; ;
        //                        length = fieldToStr.Length;
        //                        writer.Write(fieldToStr);
        //                        while (length < 6)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.CareerLadder != null ? _model.CareerLadder : string.Empty;
        //                        length = fieldToStr.Length;
        //                        writer.Write(fieldToStr);
        //                        while (length < 50)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        fieldToStr = _model.Apprentice != null ? _model.Apprentice : string.Empty;
        //                        length = fieldToStr.Length;
        //                        writer.Write(fieldToStr);
        //                        while (length < 1)
        //                        {
        //                            writer.Write(" ");
        //                            length++;
        //                        }

        //                        writer.WriteLine("                                         ");
        //                        writer.WriteLine("");

        //                        totalNetAmount = totalNetAmount + Convert.ToDouble(string.Format("{0:000000000.00}", _model.NetAmount));

        //                        srno++;
        //                    }
        //                    writer.WriteLine();
        //                    writer.Write(string.Format("{0}{1}{2}", 999, string.Format("{0:000000000000000.00}", totalNetAmount), string.Format("{0:000000}", srno)));
        //                    writer.Close();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _errormsg.Add(ex.Message);
        //    }

        //    return _errormsg;
        //}

        //#endregion        

        //#region Functions

        //public FileResult TextDownload(string filename)
        //{
        //    byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(destfolderpath + filename));
        //    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //public IList<SelectListItem> GetJobTypes()
        //{
        //    var countries = _com.GetJobTypeList()
        //        .Select(x => new SelectListItem { Text = x.value, Value = x.id.ToString() })
        //        .ToList();
        //    countries.Insert(0, new SelectListItem { Text = "Choose a job type", Value = "" });
        //    return countries;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //public IList<SelectListItem> GetRsdList()
        //{
        //    var countries = _com.getRSDList()
        //        .Select(x => new SelectListItem { Text = x.value, Value = x.id.ToString() })
        //        .ToList();
        //    countries.Insert(0, new SelectListItem { Text = "--Select--", Value = "" });
        //    return countries;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //public IList<SelectListItem> GetWBSList()
        //{
        //    var countries = _com.getWBSList()
        //        .Select(x => new SelectListItem { Text = x.value, Value = x.id.ToString() })
        //        .ToList();
        //    countries.Insert(0, new SelectListItem { Text = "--Select--", Value = "" });
        //    return countries;
        //}


        //public IList<SelectListItem> GetCurrencyList()
        //{
        //    var currencies = _com.getCurrencyList()
        //        .Select(x => new SelectListItem { Text = x.value, Value = x.id.ToString() })
        //        .ToList();
        //    currencies.Insert(0, new SelectListItem { Text = "--Select--", Value = "" });
        //    return currencies;
        //}


        ////private string buildApplyOnlineHtml()
        ////{
        ////    StringBuilder applicationDetails = new StringBuilder();
        ////    //applicationDetails.Append("<br/><br/><br/>");
        ////    applicationDetails.Append("<table><tr><td>");
        ////    applicationDetails.Append(string.Format("<h1>{0}</h1>", "Letter of Authorization "));
        ////    applicationDetails.Append("</td></tr></table>");
        ////    applicationDetails.Append("<br/>");
        ////    applicationDetails.Append("<table>");
        ////    SearchRepeatingGroup customerDetails = null;
        ////    applicationDetails.BuildRow(Resources.Text("PDFIDNumber.Text"), searchNumber);
        ////    if (searchResult != null)
        ////    {
        ////        customerDetails = searchResult.GetCustomerDetails();
        ////    }
        ////    if (customerDetails != null)
        ////    {
        ////        applicationDetails.BuildRow(Resources.Text("PDFFirstName.Text"), customerDetails.CustFullName);
        ////    }
        ////    else
        ////    {
        ////        applicationDetails.BuildRow(Resources.Text("PDFTitle.Text"), request.CustomerDetails.Title);
        ////        applicationDetails.BuildRow(Resources.Text("PDFFirstName.Text"), request.CustomerDetails.FirstName);
        ////        applicationDetails.BuildRow(Resources.Text("PDFMiddleName.Text"), request.CustomerDetails.MiddleName);
        ////        applicationDetails.BuildRow(Resources.Text("PDFLastName.Text"), request.CustomerDetails.LastName);
        ////        applicationDetails.BuildRow(Resources.Text("PDFGender.Text"), Indivirtual.RB.PDFExport.PDFManager.GeGenderText(request.CustomerDetails.Gender));
        ////        applicationDetails.BuildRow(Resources.Text("PDFNationality.Text"), Indivirtual.RB.PDFExport.PDFManager.GeNationalityText(request.CustomerDetails.Nationality));
        ////        applicationDetails.BuildRow(Resources.Text("PDFDateOfBirth.Text"), Indivirtual.RB.PDFExport.PDFManager.ConvertDateOfBirth(request.CustomerDetails.DateOfBirth, TridionPage.LanguageCode.ToLower()));
        ////        applicationDetails.BuildRow(Resources.Text("PDFMobile.Text"), request.CustomerDetails.Phone.PhoneNumber);
        ////        applicationDetails.BuildRow(Resources.Text("PDFEmail.Text"), request.CustomerDetails.Email);
        ////    }

        ////    if (!string.IsNullOrEmpty(request.CustomerDetails.EducationLevel))
        ////    {
        ////        applicationDetails.BuildRow(Resources.Text("PDFEducationLevel.Text"), Indivirtual.RB.PDFExport.PDFManager.GetEducationLevelText(request.CustomerDetails.EducationLevel));
        ////    }
        ////    if (!string.IsNullOrEmpty(request.CustomerDetails.JobSector))
        ////    {
        ////        applicationDetails.BuildRow(Resources.Text("PDFAreYouAnEmployee.Text"), Resources.Text("PDFAreYouAnEmployeeYes.Text"));
        ////        applicationDetails.BuildRow(Resources.Text("PDFJobSector.Text"), Indivirtual.RB.PDFExport.PDFManager.GetJobSectorText(request.CustomerDetails.JobSector));
        ////        applicationDetails.BuildRow(Resources.Text("PDFEmployerName.Text"), request.CustomerDetails.EmployerName);
        ////        applicationDetails.BuildRow(Resources.Text("PDFMonthlySalary.Text"), request.CustomerDetails.MonthlySalary);
        ////        applicationDetails.BuildRow(Resources.Text("PDFSalaryTransferredToRB.Text"), request.CustomerDetails.SalaryTransferredToRB);
        ////    }
        ////    else
        ////    {
        ////        applicationDetails.BuildRow(Resources.Text("PDFAreYouAnEmployee.Text"), Resources.Text("PDFAreYouAnEmployeeNo.Text"));
        ////    }
        ////    applicationDetails.BuildRow(Resources.Text("PDFCurrency.Text"), request.ProductDetails.ProductInformation.Currency);

        ////    //AppConstants.ProductTypes.TimeDeposit)
        ////    if (!string.IsNullOrEmpty(request.ProductDetails.ProductInformation.Term))
        ////    {
        ////        applicationDetails.BuildRow(Resources.Text("PDFTerm.Text"), request.ProductDetails.ProductInformation.Term);
        ////        applicationDetails.BuildRow(Resources.Text("PDFAmount.Text"), request.ProductDetails.ProductInformation.Amount);
        ////    }
        ////    //Credit Card
        ////    if (!string.IsNullOrEmpty(request.ProductDetails.ProductInformation.CardType))
        ////    {
        ////        applicationDetails.BuildRow(Resources.Text("PDFCardType.Text"), request.ProductDetails.ProductInformation.CardType);
        ////    }
        ////    //AppConstants.ProductTypes.PersonalLoans
        ////    if (!string.IsNullOrEmpty(request.ProductDetails.ProductInformation.MonthlyObligations))
        ////    {
        ////        applicationDetails.BuildRow(Resources.Text("PDFCurrenyMonthlyOblications.Text"), request.ProductDetails.ProductInformation.MonthlyObligations);
        ////    }

        ////    //AppConstants.ProductTypes.CarLoans
        ////    if (!string.IsNullOrEmpty(request.ProductDetails.ProductInformation.CarType))
        ////    {
        ////        applicationDetails.BuildRow(Resources.Text("PDFArea.Text"), Indivirtual.RB.PDFExport.PDFManager.GetAreasText(request.ProductDetails.ProductInformation.Region));
        ////        applicationDetails.BuildRow(Resources.Text("PDFCarType.Text"), request.ProductDetails.ProductInformation.CarType);
        ////        applicationDetails.BuildRow(Resources.Text("PDFCarModel.Text"), request.ProductDetails.ProductInformation.CarModel);
        ////    }

        ////    //AppConstants.ProductTypes.HomeLoans
        ////    if (!string.IsNullOrEmpty(request.ProductDetails.ProductInformation.PropertyType))
        ////    {
        ////        applicationDetails.BuildRow(Resources.Text("PDFWhereDoYouLive.Text"), request.ProductDetails.ProductInformation.LivingLocation);
        ////        applicationDetails.BuildRow(Resources.Text("PDFPropertyType.Text"), Indivirtual.RB.PDFExport.PDFManager.GetPropertyTypeText(request.ProductDetails.ProductInformation.PropertyType));
        ////        applicationDetails.BuildRow(Resources.Text("PDFPropertyValue.Text"), request.ProductDetails.ProductInformation.PropertyValue);
        ////    }
        ////    if (!string.IsNullOrEmpty(request.ProductDetails.ProductInformation.Comments))
        ////    {
        ////        applicationDetails.BuildRow(Resources.Text("PDFCommentsMessage.Text"), request.ProductDetails.ProductInformation.Comments);
        ////    }

        ////    applicationDetails.BuildRow(Resources.Text("PDFServiceRequestNumber.Text"), serviceRequestNumber);

        ////    applicationDetails.Append("</table>");
        ////    return applicationDetails.ToString();
        ////}

        //#endregion
    }
}