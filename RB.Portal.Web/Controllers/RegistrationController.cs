﻿using RB.Portal.Web.Models;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Xml.Linq;
using static RB.Portal.Web.Models.Registration;
using log4net.Config;
using Excel = Microsoft.Office.Interop.Excel;
using System.Configuration;
using RB.Portal.Web.App_LocalResources;
using RB.Portal.Web.Helper;
using iTextSharp.text;
using System.Data;
using iTextSharp.text.pdf;
using RB.Portal.Web.Attributes;
using System.Threading;

namespace RB.Portal.Web.Controllers
{
    public class RegistrationController : BaseController
    {
        string rsdsourcefilepath = "~/Source/RSD.xlsx";
        string wbssourcefilepath = "~/Source/WBS.xlsx";
        string nonwbssourcefilepath = "~/Source/NonRSDWBS.xlsx";
        // string sourcefolderpath = "~/Source/";
        string destfolderpath = "~/UserFile/";
        string destfolderpathdownload = "/UserFile/";
        CommonList _com = new CommonList();

        [HttpGet]
        public ActionResult Edit()
        {
            return View(new EditModel());
        }

        public string PDFGenerator(clsAL _clsal)
        {
            string html = buildApplyOnlineHtml(_clsal);
            var culture = System.Threading.Thread.CurrentThread.CurrentUICulture;
            var language = culture != null && culture.Name.Equals("ar") ? "ar" : "en";
            var AlName = string.Format("{0}_{1}.pdf", _clsal.AgremeentCode, _clsal.AccountNumber);
            string alpath = Path.Combine(Server.MapPath("~/UserFile"), AlName);
            Indivirtual.RB.PDFExport.PDFManager.GeneratePDF(html, language, AlName, alpath);
            return AlName;
        }

        [HttpPost]
        public async Task<JsonResult> EditExcel(string jobtype, string subtype, string lang)
        {

            if (!string.IsNullOrEmpty(lang))
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang);
            }

            List<string> _msg = new List<string>();
            try
            {
                var fileContent = Request.Files[0];

                if (fileContent != null && fileContent.ContentLength > 0)
                {
                    var fileName = fileContent.FileName;
                    var fileextension = Path.GetExtension(fileName);
                    if (fileextension.ToLower() == ".xls" || fileextension.ToLower() == ".xlsx")
                    {
                        string path = Path.Combine(Server.MapPath("~/UserFile"), fileName);

                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Delete(path);
                        }

                        fileContent.SaveAs(path);

                        if (jobtype == "Government")
                        {
                            if (subtype == "rsdcom")
                            {
                                _msg = ReadRSDExcelSheet(path);
                            }
                            else if (subtype == "nonrsdcom")
                            {
                                _msg = ReadNonWBSRSDExcelSheet(path);
                            }
                        }
                        else if (jobtype == "Corporate")
                        {
                            if (subtype == "wscom")
                            {
                                _msg = ReadWBSExcelSheet(path);
                            }
                            else if (subtype == "nonwscom")
                            {
                                _msg = ReadNonWBSRSDExcelSheet(path);
                            }
                        }
                    }
                    else
                    {
                        _msg.Add(GlobalRes.Error_InvalidFileExtension);
                    }
                }

                bool haserror = _msg.Count > 0 ? true : false;
                string textfilepath = "";
                string alpath = "";
                if (HttpContext.Cache["phfilepath"] != null)
                {
                    textfilepath = (string)HttpContext.Cache["phfilepath"];
                }

                if (HttpContext.Cache["ALName"] != null)
                {
                    alpath = string.Format("{0}{1}", destfolderpathdownload, (string)HttpContext.Cache["ALName"]);
                }
                var jsonResult = new
                {
                    Haserror = haserror,
                    filepath = textfilepath,
                    alpath = alpath,
                    msg = _msg
                };

                return Json(jsonResult, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                _msg = new List<string>();
                string errormsg = ex.Message.ToString();// "File upload fail";
                _msg.Add(errormsg);
                var jsonResulterror = new
                {
                    Haserror = true,
                    filepath = "",
                    alpath = "",
                    msg = _msg
                };

                Log.Error(ex.Message.ToString());
                return Json(jsonResulterror, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Add()
        {
            try
            {
                XElement bankList = XElement.Load(Server.MapPath("~/Source/BankList.xml"));
                List<string> _Bcodelist = new List<string>();
                List<string> _BankNameList = new List<string>();
                List<string> _BankClearingCodeList = new List<string>();
                List<string> _CurrencyList = new List<string>();

                List<string> _WBSList = new List<string>();
                List<RSD> _RSDList = new List<RSD>();
                foreach (XElement name in bankList.Elements("BankNameList").Elements("bankname"))
                {
                    _BankNameList.Add(name.Value);
                }
                foreach (XElement name in bankList.Elements("BankClearingCodeList").Elements("BankClearingCode"))
                {
                    _BankClearingCodeList.Add(name.Value);
                }

                foreach (XElement item in bankList.Elements("RSDList").Elements("rsditem"))
                {
                    RSD _rsd = new RSD();
                    _rsd.id = item.Attribute("name").Value.ToString();
                    _rsd.value = item.Value.ToString();
                    _RSDList.Add(_rsd);
                }

                return View(new Registration()
                {
                    _BankCodeList = _Bcodelist,
                    _BankNameList = _BankNameList,
                    _BankClearingCodeList = _BankClearingCodeList,
                    _CurrencyListItems = GetCurrencyList(),
                    _WBSListItems = GetWBSList(),
                    _RSDListItems = GetRsdList(),
                    _JobTypeListItems = GetJobTypes()
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Update Excel Sheet      

        [HttpPost]
        public async Task<JsonResult> regUpdateRSDExcelSheet(Registration _regmodel)
        {
            if (_regmodel != null)
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(_regmodel.Language);
            }
            deleteAllfiles();
            List<string> _result = new List<string>();
            string filename = "";
            string filePathToSave = "";
            string filePathToDownload = "";
            bool isvalid = false;
            int rowNumber = 0;
            try
            {
                dynamic agreementcode = _regmodel.AgremeentCode;
                _result = validateHAgreementCode(agreementcode, _result, rowNumber, out isvalid);

                dynamic FundingAccount = _regmodel.FundingAccount;
                _result = validateHFundingAccount(FundingAccount, _result, rowNumber, out isvalid);

                dynamic branchNo = _regmodel.BranchNo;
                _result = validateHBranchNo(branchNo, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _result = validateHBranchNoWithAC(branchNo, FundingAccount, _result, rowNumber, out isvalid);
                }

                dynamic BankCode = _regmodel.BankCode;
                _result = validateHBankCode(BankCode, _result, rowNumber, out isvalid);

                dynamic Currency = _regmodel.Currency;
                _result = validateHCurrency(Currency, _result, rowNumber, out isvalid);

                dynamic ValueDate = _regmodel.ValueDate;
                _result = CreditDateValidation(ValueDate, _result, GlobalRes.ValueDate, rowNumber, out isvalid);

                dynamic DepartmentName = _regmodel.DepartmentName;
                _result = validateDepartmentName_RSD(DepartmentName, _result, rowNumber, out isvalid);

                dynamic DivisionNumber = _regmodel.DivisionNumber;
                _result = validateHDivisionNumber_RSD(DivisionNumber, _result, rowNumber, out isvalid);

                dynamic SubDivisionNumber = _regmodel.SubDivisionNumber;
                _result = validateHSubDivisionNumber_RSD(SubDivisionNumber, _result, rowNumber, out isvalid);

                dynamic SectorNumber = _regmodel.SectorNumber;
                _result = validateHSector_RSD(SectorNumber, _result, rowNumber, out isvalid);

                dynamic ClassNumber = _regmodel.ClassNumber;
                _result = validateHClassNumber_RSD(ClassNumber, _result, rowNumber, out isvalid);


                dynamic PaymentOrderNumber = _regmodel.PaymentOrderNumber;
                _result = validateHPaymentOrderNo_RSD(PaymentOrderNumber, _result, rowNumber, out isvalid);

                dynamic LabelMsg = _regmodel.LabelMsg;
                _result = validateHLabel_RSD(LabelMsg, _result, rowNumber, out isvalid);

                //dynamic _labelMsg = _regmodel.LabelMsg;
                //_result = validateHLabel_RSD(_labelMsg, _result, rowNumber, out isvalid);

                dynamic _Currency = _regmodel.Currency;
                _result = validateHCurrency(_Currency, _result, rowNumber, out isvalid);

                bool haserror = _result.Count > 0 ? true : false;

                if (!haserror)
                {
                    if (System.IO.File.Exists(Server.MapPath(rsdsourcefilepath)))
                    {
                        if (System.IO.Directory.Exists(Server.MapPath(destfolderpath)))
                        {
                            filename = string.Format("{0}_{1}.xlsx", Guid.NewGuid(), _regmodel.FundingAccount);
                            System.IO.File.Copy(Server.MapPath(rsdsourcefilepath), Server.MapPath(Path.Combine(destfolderpath, filename)), true);
                        }
                    }
                    filePathToSave = Server.MapPath(destfolderpath + filename);
                    filePathToDownload = destfolderpathdownload + filename;

                    Excel.Application xlApp = new Excel.Application();
                    Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePathToSave, 0, false, 5, "", "", false,
                        Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                    Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    xlWorkSheet.Unprotect("rbpayroll");
                    Excel.Range xlRange = xlWorkSheet.UsedRange;
                    rowNumber = 2;

                    try
                    {
                        xlWorkSheet.Cells[rowNumber, 1] = _regmodel.CompanyName;
                        xlWorkSheet.Cells[rowNumber, 2] = _regmodel.AgremeentCode;
                        xlWorkSheet.Cells[rowNumber, 3] = _regmodel.FundingAccount;
                        xlWorkSheet.Cells[rowNumber, 4] = _regmodel.BranchNo;
                        xlWorkSheet.Cells[rowNumber, 5] = _regmodel.ValueDate;
                        xlWorkSheet.Cells[rowNumber, 6] = _regmodel.DepartmentName;
                        xlWorkSheet.Cells[rowNumber, 7] = _regmodel.DivisionNumber;
                        xlWorkSheet.Cells[rowNumber, 8] = _regmodel.SubDivisionNumber;
                        xlWorkSheet.Cells[rowNumber, 9] = _regmodel.SectorNumber;
                        xlWorkSheet.Cells[rowNumber, 10] = _regmodel.ClassNumber;
                        xlWorkSheet.Cells[rowNumber, 11] = _regmodel.PaymentOrderNumber;
                        xlWorkSheet.Cells[rowNumber, 13] = _regmodel.BankCode;
                        xlWorkSheet.Cells[rowNumber, 14] = _regmodel.Currency;
                        xlWorkSheet.Protect("rbpayroll");
                        xlApp.DisplayAlerts = false;
                        xlWorkBook.SaveAs(filePathToSave, Excel.XlFileFormat.xlOpenXMLWorkbook,
                            Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlNoChange,
                            Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value,
                            Missing.Value, Missing.Value);
                        xlWorkBook.Close();
                        xlApp.Quit();

                        Marshal.ReleaseComObject(xlWorkSheet);
                        Marshal.ReleaseComObject(xlWorkBook);
                        Marshal.ReleaseComObject(xlApp);

                    }
                    catch (Exception ex)
                    {
                        xlWorkBook.Close();
                        xlApp.Quit();
                        Marshal.ReleaseComObject(xlWorkSheet);
                        Marshal.ReleaseComObject(xlWorkBook);
                        Marshal.ReleaseComObject(xlApp);
                        throw ex;
                    }
                }

                var jsonResult = new
                {
                    Haserror = haserror,
                    filepath = filePathToDownload,
                    msg = _result
                };

                return Json(jsonResult, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                _result = new List<string>();
                string errormsg = GlobalRes.Error_ErrorOccured;
                _result.Add(errormsg);
                var jsonResulterror = new
                {
                    Haserror = true,
                    filepath = "",
                    msg = _result
                };
                return Json(jsonResulterror, JsonRequestBehavior.AllowGet);
            }

            return Json(new object { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> regUpdateWBSExcelSheet(Registration _regmodel, string lang)
        {
            if (_regmodel != null)
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(_regmodel.Language);
            }

            deleteAllfiles();
            List<string> _result = new List<string>();
            string filename = "";
            string filePathToSave = "";
            string filePathToDownload = "";
            bool isvalid = false;
            int rowNumber = 0;
            try
            {
                dynamic CustomerName = _regmodel.CustomerName;
                _result = validateHAgreementCode(CustomerName, _result, rowNumber, out isvalid);

                dynamic agreementcode = _regmodel.AgremeentCode;
                _result = validateHAgreementCode(agreementcode, _result, rowNumber, out isvalid);


                dynamic FundingAccount = _regmodel.FundingAccount;
                _result = validateHFundingAccount(FundingAccount, _result, rowNumber, out isvalid);


                dynamic branchNo = _regmodel.BranchNo;
                _result = validateHBranchNo(branchNo, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _result = validateHBranchNoWithAC(branchNo, FundingAccount, _result, rowNumber, out isvalid);
                }

                dynamic CreditDate = _regmodel.CreditDate;
                _result = CreditDateValidation(CreditDate, _result, GlobalRes.CreditDate, rowNumber, out isvalid);


                dynamic MinsOfLabEstablishedId = _regmodel.MinsOfLabEstablishedId;
                _result = validateMofLabEstablishID(MinsOfLabEstablishedId, _result, rowNumber, out isvalid);

                dynamic CommerceRoomId = _regmodel.CommerceRoomId;
                _result = validateHEstablishIdCommRoom(CommerceRoomId, _result, rowNumber, out isvalid);


                dynamic BankCode = _regmodel.BankCode;
                _result = validateHBankCode(BankCode, _result, rowNumber, out isvalid);

                dynamic Currency = _regmodel.Currency;
                _result = validateHCurrency(Currency, _result, rowNumber, out isvalid);

                bool haserror = _result.Count > 0 ? true : false;
                if (!haserror)
                {

                    if (System.IO.File.Exists(Server.MapPath(wbssourcefilepath)))
                    {
                        if (System.IO.Directory.Exists(Server.MapPath(destfolderpath)))
                        {
                            filename = string.Format("{0}_{1}.xlsx", Guid.NewGuid(), _regmodel.FundingAccount);
                            System.IO.File.Copy(Server.MapPath(wbssourcefilepath), Server.MapPath(Path.Combine(destfolderpath, filename)), true);
                        }
                    }
                    filePathToSave = Server.MapPath(destfolderpath + filename);
                    filePathToDownload = destfolderpathdownload + filename;

                    Excel.Application xlApp = new Excel.Application();

                    Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePathToSave, 0, false, 5, "", "", false,
                        Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                    Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    xlWorkSheet.Unprotect("rbpayroll");
                    Excel.Range xlRange = xlWorkSheet.UsedRange;
                    rowNumber = 2;

                    try
                    {
                        xlWorkSheet.Cells[rowNumber, 1] = _regmodel.CustomerName;
                        xlWorkSheet.Cells[rowNumber, 2] = _regmodel.AgremeentCode;
                        xlWorkSheet.Cells[rowNumber, 3] = _regmodel.FundingAccount;
                        xlWorkSheet.Cells[rowNumber, 4] = _regmodel.BranchNo;
                        xlWorkSheet.Cells[rowNumber, 5] = _regmodel.CreditDate;
                        xlWorkSheet.Cells[rowNumber, 6] = _regmodel.MinsOfLabEstablishedId;
                        xlWorkSheet.Cells[rowNumber, 7] = _regmodel.CommerceRoomId;
                        xlWorkSheet.Cells[rowNumber, 8] = _regmodel.BankCode;
                        xlWorkSheet.Cells[rowNumber, 9] = _regmodel.Currency;
                        xlWorkSheet.Protect("rbpayroll");
                        xlApp.DisplayAlerts = false;
                        xlWorkBook.SaveAs(filePathToSave, Excel.XlFileFormat.xlOpenXMLWorkbook,
                            Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlNoChange,
                            Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value,
                            Missing.Value, Missing.Value);
                        xlWorkBook.Close();
                        xlApp.Quit();

                        Marshal.ReleaseComObject(xlWorkSheet);
                        Marshal.ReleaseComObject(xlWorkBook);
                        Marshal.ReleaseComObject(xlApp);
                    }
                    catch (Exception ex)
                    {
                        xlWorkBook.Close();
                        xlApp.Quit();
                        Marshal.ReleaseComObject(xlWorkSheet);
                        Marshal.ReleaseComObject(xlWorkBook);
                        Marshal.ReleaseComObject(xlApp);
                        throw ex;
                    }
                }

                var jsonResult = new
                {
                    Haserror = haserror,
                    filepath = filePathToDownload,
                    msg = _result
                };

                return Json(jsonResult, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                _result = new List<string>();
                string errormsg = GlobalRes.Error_ErrorOccured;
                _result.Add(errormsg);
                var jsonResulterror = new
                {
                    Haserror = true,
                    filepath = "",
                    msg = _result
                };
                return Json(jsonResulterror, JsonRequestBehavior.AllowGet);
            }

            return Json(new object { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> regUpdateNonRSDWBSExcelSheet(Registration _regmodel)
        {
            if (_regmodel != null)
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(_regmodel.Language);
            }
            deleteAllfiles();
            List<string> _result = new List<string>();
            string filename = "";
            string filePathToSave = "";
            string filePathToDownload = "";
            bool isvalid = false;
            int rowNumber = 0;

            try
            {
                //if (_regmodel.CustomerName == null)
                //{
                //    _errormsg.Add("Customer name is missing");
                //}
                //_errormsg = CommonValidation(_regmodel, _errormsg);

                //if (_regmodel.CreditDate == null)
                //{
                //    _errormsg.Add("Credit date missing");
                //}
                //if (_regmodel.CreditDate != null)
                //{
                //    // _errormsg = CreditDateValidation(_regmodel.CreditDate, _errormsg, "Credit");
                //}

                dynamic agreementcode = _regmodel.AgremeentCode;
                _result = validateHAgreementCode(agreementcode, _result, rowNumber, out isvalid);

                dynamic FundingAccount = _regmodel.FundingAccount;
                _result = validateHFundingAccount(FundingAccount, _result, rowNumber, out isvalid);

                dynamic CustomerName = _regmodel.CustomerName;
                _result = validateHCustomerName(CustomerName, _result, rowNumber, out isvalid);

                dynamic branchNo = _regmodel.BranchNo;
                _result = validateHBranchNo(branchNo, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _result = validateHBranchNoWithAC(branchNo, FundingAccount, _result, rowNumber, out isvalid);
                }

                dynamic CreditDate = _regmodel.CreditDate;
                _result = CreditDateValidation(CreditDate, _result, "Credit", rowNumber, out isvalid);


                dynamic BankCode = _regmodel.BankCode;
                _result = validateHBankCode(BankCode, _result, rowNumber, out isvalid);

                dynamic Currency = _regmodel.Currency;
                _result = validateHCurrency(Currency, _result, rowNumber, out isvalid);

                bool haserror = _result.Count > 0 ? true : false;

                if (!haserror)
                {

                    if (System.IO.File.Exists(Server.MapPath(nonwbssourcefilepath)))
                    {
                        if (System.IO.Directory.Exists(Server.MapPath(destfolderpath)))
                        {
                            filename = string.Format("{0}_{1}.xlsx", Guid.NewGuid(), _regmodel.FundingAccount);
                            System.IO.File.Copy(Server.MapPath(nonwbssourcefilepath), Server.MapPath(Path.Combine(destfolderpath, filename)), true);
                        }
                    }
                    filePathToSave = Server.MapPath(destfolderpath + filename);
                    filePathToDownload = destfolderpathdownload + filename;

                    Excel.Application xlApp = new Excel.Application();

                    Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePathToSave, 0, false, 5, "", "", false,
                        Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                    Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                    xlWorkSheet.Unprotect("rbpayroll");
                    Excel.Range xlRange = xlWorkSheet.UsedRange;
                    rowNumber = 2;

                    try
                    {
                        xlWorkSheet.Cells[rowNumber, 1] = _regmodel.CustomerName;
                        xlWorkSheet.Cells[rowNumber, 2] = _regmodel.AgremeentCode;
                        xlWorkSheet.Cells[rowNumber, 3] = _regmodel.FundingAccount;
                        xlWorkSheet.Cells[rowNumber, 4] = _regmodel.BranchNo;
                        xlWorkSheet.Cells[rowNumber, 5] = _regmodel.CreditDate;
                        xlWorkSheet.Cells[rowNumber, 6] = _regmodel.BankCode;
                        xlWorkSheet.Cells[rowNumber, 7] = _regmodel.Currency;
                        xlWorkSheet.Protect("rbpayroll");
                        // Disable file override confirmaton message  
                        xlApp.DisplayAlerts = false;
                        xlWorkBook.SaveAs(filePathToSave, Excel.XlFileFormat.xlOpenXMLWorkbook,
                            Missing.Value, Missing.Value, Missing.Value, Missing.Value, Excel.XlSaveAsAccessMode.xlNoChange,
                            Excel.XlSaveConflictResolution.xlLocalSessionChanges, Missing.Value, Missing.Value,
                            Missing.Value, Missing.Value);
                        xlWorkBook.Close();
                        xlApp.Quit();

                        Marshal.ReleaseComObject(xlWorkSheet);
                        Marshal.ReleaseComObject(xlWorkBook);
                        Marshal.ReleaseComObject(xlApp);
                    }
                    catch (Exception ex)
                    {
                        xlWorkBook.Close();
                        xlApp.Quit();
                        Marshal.ReleaseComObject(xlWorkSheet);
                        Marshal.ReleaseComObject(xlWorkBook);
                        Marshal.ReleaseComObject(xlApp);
                    }
                }
                var jsonResult = new
                {
                    Haserror = haserror,
                    filepath = filePathToDownload,
                    msg = _result
                };

                return Json(jsonResult, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                _result = new List<string>();
                string errormsg = GlobalRes.Error_ErrorOccured;
                _result.Add(errormsg);
                var jsonResulterror = new
                {
                    Haserror = true,
                    filepath = "",
                    msg = _result
                };
                return Json(jsonResulterror, JsonRequestBehavior.AllowGet);
            }

            return Json(new object { }, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Validations  

        public bool IsValidLength(string data, int length)
        {
            if (data != null && data.Length > length)
            {
                return false;
            }

            return true;
        }

        public bool IsNumeric(string value)
        {
            bool isNumeric = false;
            char[] digits = "0123456789".ToCharArray();
            char[] letters = value.ToCharArray();
            for (int k = 0; k < letters.Length; k++)
            {

                if (digits.Contains(letters[k]))
                {
                    isNumeric = true;
                }
                else
                {
                    isNumeric = false;
                    break;
                }
            }
            return isNumeric;
        }

        public List<string> isvalidString(string prmStr, List<string> msg, string filedname, int rowno, out bool isvalid)
        {
            isvalid = false;
            try
            {
                bool english_lang = false, arabic_lang = false, invalid_lang = false, space_found = false;
                int number = 0;
                string invalidLetters = "";
                if (!string.IsNullOrEmpty(prmStr))
                {
                    for (int i = 0; i < prmStr.Length; i++)
                    {
                        char ch = prmStr[i];
                        if (ch == '-' || ch == '.')
                        {
                        }
                        else if (!IsNumeric(ch.ToString()))
                        {

                            number = (int)ch;
                            if (ch == ' ')
                            {
                                space_found = true;
                            }
                            else if (number >= 65 && number <= 90)
                            {
                                english_lang = true;
                            }
                            else if (number >= 97 && number <= 122)
                            {
                                english_lang = true;
                            }

                            else if (number >= 127 && number <= 255)
                            {
                                arabic_lang = true;
                            }
                            else
                            {
                                Regex regex = new Regex("[\u0600-\u06ff]|[\u0750-\u077f]|[\ufb50-\ufc3f]|[\ufe70-\ufefc]");
                                bool isarabichar = regex.IsMatch(ch.ToString());
                                bool isArabic = Regex.IsMatch(ch.ToString(), @"[\p{IsArabic}\s]+");
                                if (isArabic)
                                {
                                    arabic_lang = true;
                                }
                                else if (isarabichar)
                                {
                                    arabic_lang = true;
                                }
                                else
                                {
                                    invalid_lang = true;
                                    invalidLetters = ch.ToString();
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(invalidLetters))
                    {
                        invalidLetters = "[" + invalidLetters + "]";
                        if (english_lang)
                        {
                            msg.Add(string.Format(GlobalRes.Error_InvalidLetter, filedname, invalidLetters));
                        }
                        else
                        {
                            msg.Add(string.Format(GlobalRes.Error_InvalidLetter, filedname, invalidLetters));
                        }

                        if (english_lang && arabic_lang)
                        {
                            if (english_lang)
                            {
                                msg.Add(string.Format(GlobalRes.Error_EngArLang, filedname, invalidLetters, rowno));
                            }
                            else
                            {
                                msg.Add(string.Format(GlobalRes.Error_EngArLang, filedname, invalidLetters, rowno));
                            }
                        }

                    }
                    if (space_found)
                    {
                        msg.Add(string.Format(GlobalRes.Error_InvalidAgreementCode, filedname, rowno));
                    }
                    else
                    {
                        isvalid = true;
                    }
                }
            }
            catch
            {
                msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, filedname, rowno));
            }


            return msg;

        }

        public List<string> CreditDateValidation(string date, List<string> msglist, string filedname, int rowNumber, out bool isvalid)
        {

            string livedateformat = ConfigurationManager.AppSettings["Livedateformat"];

            Log.Debug("CreditDate Validation method Start");
            isvalid = false;
            if (string.IsNullOrEmpty(date))
            {
                msglist.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, filedname, filedname));
            }
            else
            {
                string dd = date.Split('/')[0];
                string mm = date.Split('/')[1];
                string yyyy = date.Split('/')[2];

                Log.Debug(string.Format("date-{0}", date));
                Log.Debug(string.Format("dd-{0}", dd));
                Log.Debug(string.Format("mm-{0}", mm));
                Log.Debug(string.Format("yyyy-{0}", yyyy));

                if (date.Length != 10 || Convert.ToInt32(mm) > 12)
                {
                    msglist.Add(string.Format(GlobalRes.Error_DateFormatError, filedname, date));
                }
                else if (Convert.ToInt32(mm) > 12)
                {
                    msglist.Add(string.Format(GlobalRes.Error_DateFormatError, filedname, date));
                }

                else
                {
                    Log.Debug("crdate-1");
                    DateTime crdate;
                    if (!DateTime.TryParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture,
                                           DateTimeStyles.None, out crdate))
                    {
                        Log.Debug("crdate-2");
                        msglist.Add(string.Format(GlobalRes.Error_DateFormatError, filedname, date));
                    }
                    else
                    {
                        var _newdatetime = new DateTime(int.Parse(yyyy), int.Parse(mm), int.Parse(dd));

                        Log.Debug("crdate-5- " + _newdatetime);
                        var newdatetime = Convert.ToDateTime(_newdatetime);
                        Log.Debug("crdate-5.1-: " + newdatetime);
                        Log.Debug("crdate-3");
                        var _crdate = Convert.ToDateTime(newdatetime);
                        Log.Debug("crdate-4");

                        if (newdatetime < DateTime.Today)
                        {
                            Log.Debug("crdate-6");
                            msglist.Add(string.Format(GlobalRes.Error_DateMinFormatError, filedname, _crdate.ToString("dd/MM/yyyy")));
                        }
                        else
                        {
                            Log.Debug("crdate-7");
                            if (newdatetime > DateTime.Today.AddDays(30))
                            {
                                msglist.Add(string.Format(GlobalRes.Error_DateMaxFormatError, filedname, _crdate.ToString("dd/MM/yyyy")));
                            }
                            else
                            {
                                string valdatedayname = _newdatetime.DayOfWeek.ToString();
                                string dayname = DateTime.Now.DayOfWeek.ToString();
                                var hrs = DateTime.Now.Hour;
                                var datediff = (newdatetime - DateTime.Now).TotalDays;
                                datediff = datediff > 0 ? datediff : 0;

                                Log.Debug("hrs : " + hrs);
                                Log.Debug("datediff : " + datediff);
                                Log.Debug("dayname : " + dayname);
                                Log.Debug("datediff : " + datediff);

                                if (valdatedayname == "Friday" || valdatedayname == "Saturday")
                                {
                                    msglist.Add(string.Format(GlobalRes.Error_DateWeekendError, filedname, _crdate.ToString("dd/MM/yyyy")));
                                }
                                else if (dayname == "Wednesday")
                                {

                                    if (hrs < 12 && Convert.ToInt32(datediff) < 4)
                                    {
                                        msglist.Add(string.Format(GlobalRes.Error_DateWednessdayError, filedname));
                                    }
                                    else if (hrs > 12 && Convert.ToInt32(datediff) < 5)
                                    {
                                        msglist.Add(string.Format(GlobalRes.Error_DateWednessdayAfterNoonError, filedname));
                                    }
                                    else
                                    {
                                        isvalid = true;
                                    }
                                }
                                else if (dayname == "Thursday")
                                {
                                    if (hrs < 12 && Convert.ToInt32(datediff) < 4)
                                    {
                                        msglist.Add(string.Format(GlobalRes.Error_DateThursdaydayError, filedname));
                                    }
                                    else if (hrs > 12 && Convert.ToInt32(datediff) < 5)
                                    {
                                        msglist.Add(string.Format(GlobalRes.Error_DateThursdayAfterNoonError, filedname));
                                    }
                                    else
                                    {
                                        isvalid = true;
                                    }
                                }
                                else
                                {
                                    if (hrs < 12 && Convert.ToInt32(datediff) < 2)
                                    {
                                        msglist.Add(string.Format(GlobalRes.Error_DateTodayError, filedname));
                                    }
                                    else if (hrs > 12 && Convert.ToInt32(datediff) < 3)
                                    {
                                        msglist.Add(string.Format(GlobalRes.Error_DateThreeDayError, filedname));
                                    }
                                    else
                                    {
                                        isvalid = true;
                                    }
                                }

                            }
                        }
                    }
                }
            }
            return msglist;
        }

        public string validateEmpActive(string status)
        {
            string msg = "";
            if (status.ToLower().Equals("active"))
            {
                msg = GlobalRes.Error_EmpNotActive;
            }

            return msg;
        }

        public List<string> validateHAgreementCode(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.AgremeentCode, rowcal));
                }
                else if (prmStr.Length > 8)
                {
                    _msg.Add(string.Format(GlobalRes.Error_AgreementCodeMaxChar, rowcal));
                }
                else
                {
                    isvalidString(prmStr, _msg, GlobalRes.AgremeentCode, rowno, out isvalid);
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.AgremeentCode, rowno));
            }

            return _msg;
        }

        public List<string> validateHFundingAccount(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.FundingAccount, rowcal));
                }

                prmStr = Convert.ToString(Decimal.Parse(prmStr, System.Globalization.NumberStyles.Any));

                if (!IsNumeric(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericNotNumericFiled, GlobalRes.FundingAccount, rowcal));
                }
                else if (prmStr.Length != 13)
                {
                    _msg.Add(string.Format(GlobalRes.Error_FundingAccountNoMaxChar, rowcal));
                }
                else
                {
                    isvalid = true;
                }

            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.FundingAccount, rowcal));
            }

            return _msg;
        }

        public List<string> validateHAccountNo(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.AccountNo, rowcal));
                }

                prmStr = Convert.ToString(Decimal.Parse(prmStr, System.Globalization.NumberStyles.Any));

                if (!IsNumeric(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericNotNumericFiled, GlobalRes.AccountNo, rowcal));
                    //_msg.Add(string.Format("Account is not numeric{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 34))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxDigits, GlobalRes.AccountNo, 34, rowcal));
                    //_msg.Add(string.Format("Account should not be more than 34 digits{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }

            }
            catch (Exception ex)
            {
                isvalid = false;
                //_msg.Add(string.Format("Invalid funding account no, please check{0}", rowcal));
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.AccountNo, rowcal));
            }

            return _msg;
        }

        public List<string> validateHBranchNo(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.BranchNo, rowcal));
                    //_msg.Add(string.Format("Branch No is empty{0}", rowcal));
                }

                prmStr = Convert.ToString(Decimal.Parse(prmStr, System.Globalization.NumberStyles.Any));

                if (prmStr.Length != 3)
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxDigits, GlobalRes.BranchNo, 3, rowcal));
                    // _msg.Add(string.Format("Branch No should be of 3 characters{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }

            }
            catch (Exception ex)
            {
                isvalid = false;
                //_msg.Add(string.Format("Invalid Branch No, please check{0}", rowcal));
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.BranchNo, rowcal));
            }

            return _msg;
        }

        public List<string> validateHBranchNoWithAC(string prmStr, string acNo, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.BranchNo, rowcal));
                    // _msg.Add(string.Format("Branch No is empty{0}", rowcal));
                }
                else if (string.IsNullOrEmpty(acNo))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.AccountNo, rowcal));
                    //_msg.Add(string.Format("Invalid Funding Account No is empty{0}", rowcal));
                }
                else
                {
                    string fundingacnt = acNo.Substring(0, 3);
                    if (!(prmStr).Equals(fundingacnt))
                    {
                        _msg.Add(string.Format(GlobalRes.Error_InvalidBranchNo, rowcal));
                    }
                    else
                    {
                        isvalid = true;
                    }
                }

            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.BranchNo, rowcal));
                // _msg.Add(string.Format("Invalid Branch No, please check{0}", rowcal));
            }

            return _msg;
        }

        public List<string> validateMofLabEstablishID(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    var xx = GlobalRes.Error_GenericEmptyFiled;
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.EstablishmentId, rowcal));
                    //_msg.Add(string.Format("Minsistry Of Labor Establishment ID is empty{0}", rowcal));
                }
                else if (prmStr.Length > 18)
                {
                    var yyy = GlobalRes.Error_GenericMaxChar;
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.EstablishmentId, 18, rowcal));
                    //_msg.Add(string.Format("Minsistry Of Labor Establishment ID more than 18 char{0}", rowcal));
                }
                else
                {
                    var first_digits = "";
                    var lastAfterDash = "";

                    if (prmStr.Substring(0, 2) == "-")
                    {
                        first_digits = prmStr.Split('-')[0];
                        lastAfterDash = prmStr.Split('-')[1];
                        if (!IsNumeric(first_digits) || !IsNumeric(lastAfterDash))
                        {
                            _msg.Add(string.Format(GlobalRes.Error_MoLEstablishmentIdFormat, rowcal));
                        }
                        if (first_digits == "0")
                        {
                            _msg.Add(string.Format(GlobalRes.Error_MoLEstablishmentIdFormat, rowcal));
                            // _msg.Add(string.Format("Invalid MoL Establishment Id , it must be Numbers only with this format XX-XXXXXX ... (OR X-XXXXXX ...) the first two digits represent the Region Code number and then MoL Est Id{0}", rowcal));

                        }
                        else
                        {
                            isvalid = true;
                        }
                    }
                    else if (prmStr.Substring(2, 1) == "-")
                    {
                        first_digits = prmStr.Split('-')[0];
                        lastAfterDash = prmStr.Split('-')[1];
                        if (!IsNumeric(first_digits) || !IsNumeric(lastAfterDash))
                        {
                            _msg.Add(string.Format(GlobalRes.Error_MoLEstablishmentIdFormat, rowcal));
                            // _msg.Add(string.Format("Invalid MoL Establishment Id , it must be Numbers only with this format XX-XXXXXX ... (OR X-XXXXXX ...) the first two digits represent the Region Code number and then MoL Est Id{0}", rowcal));
                        }
                        if (first_digits.Substring(0, 1) == "0")
                        {
                            _msg.Add(string.Format(GlobalRes.Error_MoLEstablishmentIdZeroFormat, rowcal));

                        }
                        if (first_digits.Substring(1, 1) == " ")
                        {
                            _msg.Add(string.Format(GlobalRes.Error_MoLEstablishmentIdSecDigitFormat, rowcal));
                        }
                        else
                        {
                            isvalid = true;
                        }
                    }
                    else
                    {
                        _msg.Add(string.Format(GlobalRes.Error_MoLEstablishmentIdFormat, rowcal));
                    }
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                //_msg.Add(string.Format("Invalid MoL Establishment Id , it must be Numbers only with this format XX-XXXXXX ... (OR X-XXXXXX ...) the first two digits represent the Region Code number and then MoL Est Id{0}", rowcal));
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.EstablishmentId, rowcal));
            }

            return _msg;
        }

        public List<string> validateHEstablishIdCommRoom(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.EstablishmentCommerceRoomID, rowcal));
                    //_msg.Add(string.Format("Invalid EstablishID in Commerce Room is empty{0}", rowcal));
                }
                else if (prmStr.Length != 10)
                {
                    //_msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.Currency, 10, rowcal));
                    _msg.Add(string.Format(GlobalRes.Error_EstablishmentCommerceRoomIDLength, rowcal));
                }
                else if (!IsNumeric(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericNotNumericFiled, GlobalRes.EstablishmentCommerceRoomID, rowcal));
                    // _msg.Add(string.Format("Invalid Establishment Commerce Room ID not a numeric{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.EstablishmentCommerceRoomID, rowcal));
            }

            return _msg;
        }

        public List<string> validateHBankCode(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.BankCode, rowcal));
                    // _msg.Add(string.Format("Invalid Bank Code is empty{0}", rowcal));
                }
                else if (!prmStr.Contains("RIBL"))
                {
                    _msg.Add(string.Format(GlobalRes.Error_InvalidBankCode, rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.BankCode, rowcal));
            }

            return _msg;
        }

        public List<string> validateHBankCode_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.BankCode, rowcal));
                    //_msg.Add(string.Format("Invalid Bank Code is empty{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.BankCode, rowcal));
            }

            return _msg;
        }

        public List<string> validateHCurrency(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.Currency, rowcal));
                    //_msg.Add(string.Format("Invalid Currency Name is empty{0}", rowcal));
                }
                else if (prmStr.Length != 3)
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericLengthChars, GlobalRes.Currency, 3, rowcal));
                    //_msg.Add(string.Format("Invalid Currency it must be 3 char {0}", rowcal));
                }
                else
                {
                    isvalidString(prmStr, _msg, GlobalRes.Currency, rowno, out isvalid);
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.Currency, rowcal));
            }

            return _msg;
        }

        public List<string> validateBenefRef(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.BeneficiaryRefNo, rowcal));
                    //_msg.Add(string.Format("Invalid Beneficiary Ref is empty{0}", rowcal));
                }
                else if (prmStr.Length != 10)
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericLengthDigits, GlobalRes.BeneficiaryRefNo, 10, rowcal));
                    // _msg.Add(string.Format("Invalid Beneficiary Ref it must be 10 char{0}", rowcal));
                }
                else if (!IsNumeric(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericNotNumericFiled, GlobalRes.BeneficiaryRefNo, rowcal));
                    //_msg.Add(string.Format("Invalid Beneficiary Ref not as numeric{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.BeneficiaryRefNo, rowcal));
            }

            return _msg;
        }

        public List<string> validateBenefRefNoneWPS(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.BeneficiaryRefNo, rowcal));
                    // _msg.Add(string.Format("Invalid Beneficiary Ref is empty{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 12))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.BeneficiaryRefNo, 12, rowcal));
                    //_msg.Add(string.Format("Invalid Beneficiary Ref more than 12 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.BeneficiaryRefNo, rowcal));
            }

            return _msg;
        }

        public List<string> validateBenefName(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.BeneficiaryName, rowcal));
                    // _msg.Add(string.Format("Invalid Beneficiary Name is empty{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 35))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.BeneficiaryName, 35, rowcal));
                    //_msg.Add(string.Format("Invalid Beneficiary Name more than 35 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.BeneficiaryName, rowcal));
            }

            return _msg;
        }

        public List<string> validateBankCode(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.BankCode, rowcal));
                    // _msg.Add(string.Format("Invalid Bank Code is empty{0}", rowcal));
                }
                //else if (prmStr.Length != 11)
                //{
                //    _msg.Add(string.Format("Invalid Bank Code more than 11 char{0}", rowcal));
                //}
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.BankCode, rowcal));
            }

            return _msg;
        }

        public List<string> validateNetAmount(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.NetAmount, rowcal));
                    // _msg.Add(string.Format("Invalid NetAmount is Empty{0}", rowcal));
                }
                else if (!IsNumeric(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericNotNumericFiled, GlobalRes.NetAmount, rowcal));
                    //_msg.Add(string.Format("Invalid NetAmount, Not Numeric{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 15))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxDigits, GlobalRes.NetAmount, 15, rowcal));
                    //_msg.Add(string.Format("Invalid NetAmount more than 15 digits{0}", rowcal));
                }
                else if (Convert.ToInt32(prmStr) <= 0)
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericGreaterThanZero, GlobalRes.NetAmount, rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.NetAmount, rowcal));
            }

            return _msg;
        }

        public List<string> validateAmount(string prmStr, List<string> _msg, string fieldname, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, fieldname, rowcal));
                    // _msg.Add(string.Format("Invalid {1} is Empty{0}", rowcal, fieldname));
                }
                else if (!IsNumeric(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericNotNumericFiled, fieldname, rowcal));
                    // _msg.Add(string.Format("Invalid {1}, Not Numeric{0}", rowcal, fieldname));
                }
                else if (!IsValidLength(prmStr, 15))
                {
                    //Error_GenericMaxDigits
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxDigits, fieldname, 15, rowcal));
                    // _msg.Add(string.Format("Invalid {1} more than 15 digits{0}", rowcal, fieldname));
                }
                else if (Convert.ToInt32(prmStr) < 0)
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericGreaterThanZero, fieldname, rowcal));
                    //_msg.Add(string.Format("Invalid {1} should be greater than 0 {0}", rowcal, fieldname));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, fieldname, rowcal));
            }

            return _msg;
        }

        public List<string> validateAddress(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {

                if (!string.IsNullOrEmpty(prmStr) && !IsValidLength(prmStr, 35))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.Address, 35, rowcal));
                    //_msg.Add(string.Format("Invalid Address can't be more than 35 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.Address, rowcal));
            }

            return _msg;
        }

        public List<string> validateCity(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {

                if (!string.IsNullOrEmpty(prmStr) && !IsValidLength(prmStr, 20))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.City, 20, rowcal));
                    // _msg.Add(string.Format("Invalid City Name can't be more than 20 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.City, rowcal));
            }

            return _msg;
        }

        public List<string> validateZipCode(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (!string.IsNullOrEmpty(prmStr) && !IsValidLength(prmStr, 9))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.ZipCode, 9, rowcal));
                    // _msg.Add(string.Format("Invalid ZipCode can't be more than 9 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.ZipCode, rowcal));
            }

            return _msg;
        }

        public List<string> validateEmpActive(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (!prmStr.ToUpper().Equals("ACTIVE"))
                {
                    _msg.Add(string.Format(GlobalRes.Error_InactiveEmployee, rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.Status, rowcal));
            }

            return _msg;
        }

        public List<string> validatePaymentDescr(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (!IsValidLength(prmStr, 140))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.PaymentDescription, 140, rowcal));
                    // _msg.Add(string.Format("Invalid Payment Description can't be more than 140 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.PaymentDescription, rowcal));
            }

            return _msg;
        }

        public List<string> validatePaymentRef(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (!IsValidLength(prmStr, 16))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.PaymentReference, 16, rowcal));
                    //_msg.Add(string.Format("Invalid Payment Reference can't be more than 16 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.PaymentReference, rowcal));
            }

            return _msg;
        }

        public List<string> validateCommonAmount(string prmStr, List<string> _msg, int rowno, string filedname, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, filedname, rowcal));
                    //_msg.Add(string.Format("Invalid {0} is Empty - row no {1}", filedname, rowno));
                }
                else if (!IsNumeric(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericNotNumericFiled, filedname, rowcal));
                    //_msg.Add(string.Format("Invalid {0}, Not Numeric -  row no {1}", filedname, rowno));
                }
                else if (!IsValidLength(prmStr, 12))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, filedname, 15, rowcal));
                    //_msg.Add(string.Format("Invalid {0} more than 15 digits -  row no {1}", filedname, rowno));
                }
                else if (Convert.ToInt32(prmStr) < 0)
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericLessThanZero, filedname, rowno));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, filedname, rowcal));
            }

            return _msg;
        }

        public List<string> validateHCustomerName(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.CustomeName, rowcal));
                    // _msg.Add(string.Format("Invalid Customer Name is Empty{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 35))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.CustomeName, 35, rowcal));
                    //_msg.Add(string.Format("Invalid Customer Name more than 35 char{0}", rowcal));
                }
                else
                {
                    isvalidString(prmStr, _msg, GlobalRes.CustomeName, rowno, out isvalid);
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.CustomeName, rowcal));
            }

            return _msg;
        }

        public List<string> validateDepartmentName_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (!IsValidLength(prmStr, 150))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.DepartmentName, 150, rowcal));
                    // _msg.Add(string.Format("Invalid Department Name more than 150 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.DepartmentName, rowcal));
            }

            return _msg;
        }

        public List<string> validateBenefRef_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.BeneficiaryRefNo, rowcal));
                    //_msg.Add(string.Format("Invalid Beneficiary Ref is empty{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 12))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.BeneficiaryRefNo, 12, rowcal));
                    // _msg.Add(string.Format("Invalid Beneficiary Ref more than 12 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.BeneficiaryRefNo, rowcal));
            }

            return _msg;
        }

        public List<string> validateHDivisionNumber_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.DivisionNumber, rowcal));
                    //_msg.Add(string.Format("Invalid Division Number is empty{0}", rowcal));
                }
                else if (!IsNumeric(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericNotNumericFiled, GlobalRes.DivisionNumber, rowcal));
                    //_msg.Add(string.Format("Invalid Division Number not numeric{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 3))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.DivisionNumber, 3, rowcal));
                    //_msg.Add(string.Format("Invalid Division Number can't be more than 3 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.DivisionNumber, rowcal));
            }

            return _msg;
        }

        public List<string> validateHSubDivisionNumber_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {

                if (!string.IsNullOrEmpty(prmStr) && !IsNumeric(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericNotNumericFiled, GlobalRes.SubDivisionNumber, rowcal));
                    // _msg.Add(string.Format("Invalid Sub-Division Number not numeric{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 3))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.SubDivisionNumber, 3, rowcal));
                    // _msg.Add(string.Format("Invalid Sub-Division Number can't be more than 3 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.SubDivisionNumber, rowcal));
            }

            return _msg;
        }

        public List<string> validateHSector_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.SectorNumber, rowcal));
                    // _msg.Add(string.Format("Invalid Sector Number is empty{0}", rowcal));
                }
                else if (!IsNumeric(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericNotNumericFiled, GlobalRes.SectorNumber, rowcal));
                    // _msg.Add(string.Format("Invalid Sector Number not numeric{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 2))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.SectorNumber, 2, rowcal));
                    // _msg.Add(string.Format("Invalid Sector Number can't be more than 2 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.SectorNumber, rowcal));
            }

            return _msg;
        }

        public List<string> validateHClassNumber_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.ClassNumber, rowcal));
                    //_msg.Add(string.Format("Invalid Class Number is empty{0}", rowcal));
                }
                else if (!IsNumeric(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericNotNumericFiled, GlobalRes.ClassNumber, rowcal));
                    // _msg.Add(string.Format("Invalid Class Number not numeric{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 3))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.ClassNumber, 3, rowcal));
                    // _msg.Add(string.Format("Invalid Class Number can't be more than 3 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.ClassNumber, rowcal));
            }

            return _msg;
        }

        public List<string> validateHPaymentOrderNo_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {

                if (!string.IsNullOrEmpty(prmStr) && !IsValidLength(prmStr, 15))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxDigits, GlobalRes.PaymentOrderNumber, 15, rowcal));
                    //_msg.Add(string.Format("Invalid Payment Order No more than 15 digits{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.PaymentOrderNumber, rowcal));
            }

            return _msg;
        }

        public List<string> validateIqma_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.Iqama, rowcal));
                    // _msg.Add(string.Format("Invalid Iqama is empty{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 18))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.Iqama, 18, rowcal));
                    // _msg.Add(string.Format("Invalid Iqama can't be more than 18 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.Iqama, rowcal));
            }

            return _msg;
        }

        public List<string> validateHLabel_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {

                if (!string.IsNullOrEmpty(prmStr) && !IsValidLength(prmStr, 35))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxDigits, GlobalRes.LabelMsg, 35, rowcal));
                    //_msg.Add(string.Format("Invalid Label more than 35 digits{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.LabelMsg, rowcal));
            }

            return _msg;
        }

        public List<string> validateAccrualMonth_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {

            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.AccrualMonth, rowcal));
                    // _msg.Add(string.Format("Invalid Accrual month is empty{0}", rowcal));
                }
                else
                {
                    var holdUserDate = prmStr;
                    string mm = prmStr.Substring(4, 2);
                    string yyyy = prmStr.Substring(0, 4);
                    if (holdUserDate.Length != 6)
                    {
                        _msg.Add(string.Format(GlobalRes.Error_GenericDateformatYYYYMM, GlobalRes.AccrualMonth, rowcal));

                    }
                    else if (!IsNumeric(mm) || !IsNumeric(yyyy))
                    {
                        _msg.Add(string.Format(GlobalRes.Error_GenericDateformatYYYYMM, GlobalRes.AccrualMonth, rowcal));
                    }
                    else if (int.Parse(mm) > 12)
                    {
                        _msg.Add(string.Format(GlobalRes.Error_GenericDateformatYYYYMM, GlobalRes.AccrualMonth, rowcal));
                    }
                    else
                    {
                        isvalid = true;
                    }
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.AccrualMonth, rowcal));
            }

            return _msg;
        }

        public List<string> validateApprentice_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (prmStr.Length > 1)
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.Apprentice, 1, rowcal));
                    //_msg.Add(string.Format("Invalid Apprentice can't be more than 1 char{0}", rowcal));

                }
                else if (prmStr.ToUpper() != "Y" && prmStr.ToUpper() != "N")
                {
                    _msg.Add(string.Format(GlobalRes.Error_InvalidApprenticeValue, rowcal));
                }

                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.Apprentice, rowcal));
                // _msg.Add(string.Format("Invalid Apprentice{0}", rowcal));
            }

            return _msg;
        }

        public List<string> validateNationality_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.Nationality, rowcal));
                    //_msg.Add(string.Format("Invalid Nationality Name is empty{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 200))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.Nationality, 200, rowcal));
                    // _msg.Add(string.Format("Invalid Nationality Name can't be more than 200 char{0}", rowcal));
                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.Nationality, rowcal));
            }

            return _msg;
        }

        public List<string> validateCareerLadder_RSD(string prmStr, List<string> _msg, int rowno, out bool isvalid)
        {
            isvalid = false;
            string rowcal = rowno == 0 ? "" : " - row no " + rowno;
            try
            {
                if (string.IsNullOrEmpty(prmStr))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericEmptyFiled, GlobalRes.CareerLadder, rowcal));
                    //_msg.Add(string.Format("Invalid Career Ladder is empty{0}", rowcal));
                }
                else if (!IsValidLength(prmStr, 50))
                {
                    _msg.Add(string.Format(GlobalRes.Error_GenericMaxChar, GlobalRes.CareerLadder, 50, rowcal));

                }
                else
                {
                    isvalid = true;
                }
            }
            catch (Exception ex)
            {
                isvalid = false;
                _msg.Add(string.Format(GlobalRes.Error_GenericInvalidFiled, GlobalRes.CareerLadder, rowcal));
            }

            return _msg;
        }

        #endregion

        #region Upload Excel sheet for Non WBS and Non RSD

        public List<string> ReadNonWBSRSDExcelSheet(string filePath)
        {
            List<string> _result = new List<string>();
            try
            {
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePath, 0, false, 5, "", "", false,
                    Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                List<clsNonWBS> nonwbsmodelList = new List<clsNonWBS>();
                clsNonWBS _nonwbsmodel;
                Excel.Range xlRange = xlWorkSheet.UsedRange;
                int rowNumber = 2;
                Registration _model = new Registration();

                try
                {
                    //Logger.Error("ErrorHandler failed", error);
                    bool isvalid = false;
                    dynamic CustomerName = (xlRange.Cells[rowNumber, 1] as Excel.Range).Text;
                    _result = validateHAgreementCode(CustomerName, _result, rowNumber, out isvalid);

                    if (isvalid)
                    {
                        _model.CustomerName = Convert.ToString(CustomerName);
                    }

                    dynamic agreementcode = (xlRange.Cells[rowNumber, 2] as Excel.Range).Text;
                    _result = validateHAgreementCode(agreementcode, _result, rowNumber, out isvalid);

                    if (isvalid)
                    {
                        _model.AgremeentCode = Convert.ToString(agreementcode);
                    }

                    dynamic FundingAccount = (xlRange.Cells[rowNumber, 3] as Excel.Range).Text;
                    _result = validateHFundingAccount(FundingAccount, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _model.FundingAccount = Convert.ToString(FundingAccount);
                    }

                    dynamic branchNo = (xlRange.Cells[rowNumber, 4] as Excel.Range).Text;
                    _result = validateHBranchNo(branchNo, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _result = validateHBranchNoWithAC(branchNo, FundingAccount, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _model.BranchNo = Convert.ToString(FundingAccount);
                        }
                    }

                    dynamic CreditDate = (xlRange.Cells[rowNumber, 5] as Excel.Range).Text;
                    _result = CreditDateValidation(CreditDate, _result, GlobalRes.CreditDate, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _model.CreditDate = Convert.ToString(CreditDate);
                    }

                    dynamic BankCode = (xlRange.Cells[rowNumber, 6] as Excel.Range).Text;
                    _result = validateHBankCode(BankCode, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _model.BankCode = Convert.ToString(BankCode);
                    }

                    dynamic Currency = (xlRange.Cells[rowNumber, 7] as Excel.Range).Text;
                    _result = validateHCurrency(Currency, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _model.Currency = Convert.ToString(Currency);
                    }

                    dynamic BatchNo = (xlRange.Cells[rowNumber, 8] as Excel.Range).Text;
                    _model.BatchNo = Convert.ToString(BatchNo);

                    //Iterate the rows in the used range
                    int srno = 1;
                    for (int index = 4; index <= xlRange.Rows.Count; index++)
                    {
                        _nonwbsmodel = new clsNonWBS();
                        _nonwbsmodel.SrNo = srno.ToString();

                        dynamic BeneficiaryId = (xlRange.Cells[index, 2] as Excel.Range).Text;
                        _result = validateBenefRef(BeneficiaryId, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _nonwbsmodel.BeneficiaryRef = Convert.ToString(BeneficiaryId);
                        }

                        dynamic EmployeeName = (xlRange.Cells[index, 3] as Excel.Range).Text;
                        _result = validateBenefName(EmployeeName, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _nonwbsmodel.EmployeeName = Convert.ToString(EmployeeName);
                        }

                        dynamic AccountNumber = (xlRange.Cells[index, 4] as Excel.Range).Text;
                        _result = validateHAccountNo(AccountNumber, _result, index, out isvalid);
                        if (isvalid)
                        {
                            _nonwbsmodel.AccountNumber = AccountNumber;
                        }

                        dynamic _bankCode = (xlRange.Cells[index, 5] as Excel.Range).Text;
                        _result = validateBenefName(_bankCode, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _nonwbsmodel.BankCode = Convert.ToString(_bankCode);
                        }

                        dynamic BasicSalary = (xlRange.Cells[index, 6] as Excel.Range).Text;
                        _result = validateCommonAmount(BasicSalary, _result, rowNumber, GlobalRes.BasicSalary, out isvalid);
                        if (isvalid)
                        {
                            _nonwbsmodel.Amount = Convert.ToString(BasicSalary);
                        }

                        dynamic _currency = (xlRange.Cells[index, 7] as Excel.Range).Text;
                        _result = validateHCurrency(_currency, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _nonwbsmodel.Currency = Convert.ToString(_currency);
                        }
                        dynamic Status = (xlRange.Cells[index, 8] as Excel.Range).Text;
                        _result = validateEmpActive(Status, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _nonwbsmodel.Status = Convert.ToString(Status);
                        }

                        dynamic Address = (xlRange.Cells[index, 9] as Excel.Range).Text;
                        _result = validateAddress(Address, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _nonwbsmodel.Address = Convert.ToString(Address);
                        }

                        dynamic City = (xlRange.Cells[index, 10] as Excel.Range).Text;
                        _result = validateCity(City, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _nonwbsmodel.City = Convert.ToString(City);
                        }

                        dynamic ZipCode = (xlRange.Cells[index, 11] as Excel.Range).Text;
                        _result = validateZipCode(ZipCode, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _nonwbsmodel.ZipCode = Convert.ToString(ZipCode);
                        }

                        dynamic PaymentDesc = (xlRange.Cells[index, 12] as Excel.Range).Text;
                        _result = validatePaymentDescr(PaymentDesc, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _nonwbsmodel.PaymentDesc = Convert.ToString(PaymentDesc);
                        }
                        nonwbsmodelList.Add(_nonwbsmodel);
                        srno++;
                    }

                    xlWorkBook.Close();
                    xlApp.Quit();

                    Marshal.ReleaseComObject(xlWorkSheet);
                    Marshal.ReleaseComObject(xlWorkBook);
                    Marshal.ReleaseComObject(xlApp);
                }
                catch (Exception ex)
                {
                    xlWorkBook.Close();
                    xlApp.DisplayAlerts = false;
                    xlApp.Quit();

                    Marshal.ReleaseComObject(xlWorkSheet);
                    Marshal.ReleaseComObject(xlWorkBook);
                    Marshal.ReleaseComObject(xlApp);
                    throw ex;
                }

                if (nonwbsmodelList != null && nonwbsmodelList.Any() && _result.Count == 0)
                {
                    if (_result.Count == 0)
                    {
                        _result = nonWBSModelValidation_Upload(nonwbsmodelList, _model, _result);
                    }
                }
                else if (_result.Count > 0)
                {
                    // _result.Add("Invalid file");
                }
                else
                {
                    _result.Add(GlobalRes.Error_InvalidFile);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _result;
        }

        public List<string> nonWBSModelValidation_Upload(List<clsNonWBS> _modelList, Registration _regmodel, List<string> _errormsg)
        {
            try
            {
                //  _errormsg = CommonValidation(_regmodel, _errormsg);
                // _errormsg = CreditDateValidation(_regmodel.CreditDate, _errormsg, "Credit");
                //   _errormsg = CommonUploadValidation_NonType(_modelList, _errormsg);

                //if (_errormsg.Count == 0)
                //{
                double vTotPay = 0, vNegAmt = 0;
                int srno = 0;
                var dd = _regmodel.CreditDate.Split('/')[0];
                var mm = _regmodel.CreditDate.Split('/')[1];
                var yy = _regmodel.CreditDate.Split('/')[2];

                DateTime startdate = new DateTime(2005, 01, 01);
                DateTime vCreditDate = new DateTime(Convert.ToInt32(yy), Convert.ToInt32(mm), Convert.ToInt32(dd));
                var iJlnDt = (vCreditDate - startdate).TotalDays;


                var vTmpPayRef = _regmodel.AgremeentCode.Substring(0, 6) +
                     string.Format("{0:000}", iJlnDt.ToString().Substring(0, 3));

                string serialno = string.Format("{0:0000}", 1);
                string batchno = string.IsNullOrEmpty(_regmodel.BatchNo) ? string.Format("{0:00}", 1) : string.Format("{0:00}", _regmodel.BatchNo);
                string filename = string.Format("{0}.txt", _regmodel.AgremeentCode + "-PR-" + yy + mm + dd + "-" + string.Format("0{0}", batchno));
                string filereference = "PAYROLL REF PR-" + string.Format("{0:0000}", _regmodel.BatchNo) + "-" + vCreditDate.ToString("yyyyMMdd") + DateTime.Now.ToString("HHmmss") + serialno;


                filereference = filereference.Trim();
                var length = filereference.Length;
                while (length < 8)
                {
                    filereference = filereference + "X";
                    length++;
                }

                string txtfilepath = Path.Combine(@"~\UserFile\" + filename);

                HttpContext.Cache.Insert("phfilepath", filename, null, DateTime.UtcNow.AddMinutes(10), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                if (System.IO.File.Exists(Server.MapPath(txtfilepath)))
                {
                    string path = Path.Combine(Server.MapPath(destfolderpath), filename);
                    System.IO.File.Delete(path);
                }

                if (!System.IO.File.Exists(Server.MapPath(txtfilepath)))
                {
                    using (var stream = new FileStream(Server.MapPath(txtfilepath), FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        //srno = srno + 1;
                        string fieldToStr = "";
                        StreamWriter writer = new StreamWriter(stream);
                        writer.Write("000");

                        fieldToStr = _regmodel.AgremeentCode.Trim();
                        length = fieldToStr.Length;
                        writer.Write(fieldToStr);
                        while (length < 8)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        fieldToStr = yy + mm + dd;
                        length = fieldToStr.Length;
                        writer.Write(fieldToStr);
                        while (length < 8)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        length = filereference.Length;
                        writer.Write(filereference);
                        while (length < 35)
                        {
                            writer.Write("X");
                            length++;
                        }

                        writer.WriteLine("");

                        //----------------------------------------------------------
                        double totalNetAmount = 0;
                        foreach (var _model in _modelList)
                        {
                            srno = srno + 1;
                            writer.Write(112);

                            fieldToStr = string.Format("{0:000000}", srno);
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 6)
                            {
                                writer.Write(" ");
                                length++;
                            }


                            var sPayRefNo = vTmpPayRef + string.Format("{0:00000}", srno);
                            fieldToStr = sPayRefNo.Trim();
                            if (fieldToStr.Length > 16)
                            {
                                fieldToStr = fieldToStr.Substring(0, 16);
                            }

                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 16)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.BeneficiaryRef;
                            if (fieldToStr.Length > 12)
                            {
                                fieldToStr = fieldToStr.Substring(0, 12);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 12)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.BankCode.Trim();
                            length = fieldToStr.Length;
                            if (length > 11)
                            {
                                fieldToStr = fieldToStr.Substring(0, 11);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 11)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.AccountNumber.ToString();
                            length = fieldToStr.Length;
                            if (length > 34)
                            {
                                fieldToStr = fieldToStr.Substring(0, 34);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 34)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = string.Format("{0:000000000000.00}", _model.Amount);
                            length = fieldToStr.Length;
                            if (length > 15)
                            {
                                fieldToStr = fieldToStr.Substring(0, 15);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 15)
                            {
                                writer.Write(" ");
                                length++;
                            }


                            fieldToStr = _model.Currency;
                            length = fieldToStr.Length;
                            if (length > 3)
                            {
                                fieldToStr = fieldToStr.Substring(0, 3);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 3)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.Address != null ? _model.Address : string.Empty;
                            length = fieldToStr.Length;
                            if (length > 35)
                            {
                                fieldToStr = fieldToStr.Substring(0, 35);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 35)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.City != null ? _model.City : string.Empty;
                            //fieldToStr = _model.City;
                            length = fieldToStr.Length;
                            if (length > 20)
                            {
                                fieldToStr = fieldToStr.Substring(0, 20);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 20)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.ZipCode != null ? _model.ZipCode.ToString() : string.Empty;
                            // fieldToStr = _model.ZipCode;
                            length = fieldToStr.Length;
                            if (length > 9)
                            {
                                fieldToStr = fieldToStr.Substring(0, 9);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 9)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.EmployeeName;
                            length = fieldToStr.Length;
                            if (length > 35)
                            {
                                fieldToStr = fieldToStr.Substring(0, 35);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 35)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.PaymentDesc != null ? _model.PaymentDesc : string.Empty;
                            length = fieldToStr.Length;
                            if (length > 139)
                            {
                                fieldToStr = fieldToStr.Substring(0, 139);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 139)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            writer.Write("X");
                            writer.WriteLine("");

                            totalNetAmount = totalNetAmount + Convert.ToDouble(string.Format("{0:000000000000.00}", _model.Amount));
                            //srno++;
                        }

                        writer.Write(string.Format("{0}{1}{2}", 999, string.Format("{0:000000000000000.00}", totalNetAmount), string.Format("{0:000000}", srno)));
                        writer.Close();



                        clsAL _clsal = new clsAL()
                        {
                            AgremeentCode = _regmodel.AgremeentCode,
                            AccountNumber = _regmodel.FundingAccount,
                            TotalRecords = srno.ToString(),
                            TotalAmount = totalNetAmount.ToString(),
                            ValueCreditDate = _regmodel.CreditDate
                        };
                        var ALName = PDFGenerator(_clsal);
                        HttpContext.Cache.Insert("ALName", ALName, null, DateTime.UtcNow.AddMinutes(10), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);

                    }
                }
                //}
            }
            catch (Exception ex)
            {
                _errormsg.Add(ex.Message);
            }

            return _errormsg;
        }

        #endregion

        #region Upload Excel sheet and generate text file for WBS     


        public List<string> ReadWBSExcelSheet(string filePath)
        {
            List<string> _result = new List<string>();
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePath, 0, false, 5, "", "", false,
                Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            try
            {
                Excel.Range xlRange = xlWorkSheet.UsedRange;
                int rowNumber = 2;
                Registration _model = new Registration();
                List<clsWBS> wbsmodelList = new List<clsWBS>();
                clsWBS _wbsmodel;
                try
                {
                    bool isvalid = false;

                    dynamic CustomerName = (xlRange.Cells[rowNumber, 1] as Excel.Range).Text;
                    _result = validateHAgreementCode(CustomerName, _result, rowNumber, out isvalid);

                    if (isvalid)
                    {
                        _model.CustomerName = Convert.ToString(CustomerName);
                    }

                    dynamic agreementcode = (xlRange.Cells[rowNumber, 2] as Excel.Range).Text;
                    _result = validateHAgreementCode(agreementcode, _result, rowNumber, out isvalid);

                    if (isvalid)
                    {
                        _model.AgremeentCode = Convert.ToString(agreementcode);
                    }

                    dynamic FundingAccount = (xlRange.Cells[rowNumber, 3] as Excel.Range).Text;
                    _result = validateHFundingAccount(FundingAccount, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _model.FundingAccount = Convert.ToString(FundingAccount);
                    }

                    dynamic branchNo = (xlRange.Cells[rowNumber, 4] as Excel.Range).Text;
                    _result = validateHBranchNo(branchNo, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _result = validateHBranchNoWithAC(branchNo, FundingAccount, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _model.BranchNo = Convert.ToString(FundingAccount);
                        }
                    }

                    dynamic CreditDate = (xlRange.Cells[rowNumber, 5] as Excel.Range).Text;
                    _result = CreditDateValidation(CreditDate, _result, GlobalRes.CreditDate, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _model.CreditDate = Convert.ToString(CreditDate);
                    }

                    dynamic MinsOfLabEstablishedId = (xlRange.Cells[rowNumber, 6] as Excel.Range).Text;
                    _result = validateMofLabEstablishID(MinsOfLabEstablishedId, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _model.MinsOfLabEstablishedId = Convert.ToString(MinsOfLabEstablishedId);
                    }

                    dynamic CommerceRoomId = (xlRange.Cells[rowNumber, 7] as Excel.Range).Text;
                    _result = validateHEstablishIdCommRoom(CommerceRoomId, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _model.CommerceRoomId = Convert.ToString(CommerceRoomId);
                    }

                    dynamic BankCode = (xlRange.Cells[rowNumber, 8] as Excel.Range).Text;
                    _result = validateHBankCode(BankCode, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _model.BankCode = Convert.ToString(BankCode);
                    }

                    dynamic Currency = (xlRange.Cells[rowNumber, 9] as Excel.Range).Text;
                    _result = validateHCurrency(Currency, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _model.Currency = Convert.ToString(Currency);
                    }

                    dynamic BatchNo = (xlRange.Cells[rowNumber, 10] as Excel.Range).Text;
                    _model.BatchNo = Convert.ToString(BatchNo);

                    //Iterate the rows in the used range
                    int srno = 1;
                    for (int index = 4; index <= xlRange.Rows.Count; index++)
                    {
                        rowNumber = index;
                        //   bool isvalid = false;
                        _wbsmodel = new clsWBS();
                        _wbsmodel.SrNo = srno.ToString();


                        //_wbsmodel.BeneficiaryId = Convert.ToString((xlRange.Cells[index, 2] as Excel.Range).Text);

                        dynamic BeneficiaryId = (xlRange.Cells[index, 2] as Excel.Range).Text;
                        _result = validateBenefRef(BeneficiaryId, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.BeneficiaryId = Convert.ToString(BeneficiaryId);
                        }

                        dynamic EmployeeName = (xlRange.Cells[index, 3] as Excel.Range).Text;
                        _result = validateBenefName(EmployeeName, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.EmployeeName = Convert.ToString(EmployeeName);
                        }


                        dynamic AccountNumber = (xlRange.Cells[index, 4] as Excel.Range).Text;
                        _result = validateHAccountNo(AccountNumber, _result, index, out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.AccountNumber = AccountNumber;
                        }

                        //if ((xlRange.Cells[index, 5] as Excel.Range).Value2 != null)
                        //{
                        //    _wbsmodel.BankCode = (string)(xlRange.Cells[index, 5] as Excel.Range).Value2;
                        //}



                        dynamic _bankCode = (xlRange.Cells[index, 5] as Excel.Range).Text;
                        _result = validateBenefName(_bankCode, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.BankCode = Convert.ToString(_bankCode);
                        }



                        //if ((xlRange.Cells[index, 6] as Excel.Range).Value2 != null)
                        //{
                        //    _wbsmodel.NetAmount = Convert.ToDouble((xlRange.Cells[index, 6] as Excel.Range).Value2);
                        //}

                        dynamic NetAmount = (xlRange.Cells[index, 6] as Excel.Range).Text;
                        _result = validateNetAmount(NetAmount, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.NetAmount = Convert.ToString(NetAmount);
                        }


                        //if ((xlRange.Cells[index, 7] as Excel.Range).Value2 != null)
                        //{
                        //    _wbsmodel.BasicSalary = Convert.ToDouble((xlRange.Cells[index, 7] as Excel.Range).Value2);
                        //}

                        dynamic BasicSalary = (xlRange.Cells[index, 7] as Excel.Range).Text;
                        _result = validateCommonAmount(BasicSalary, _result, rowNumber, "Basic Salary", out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.BasicSalary = Convert.ToString(BasicSalary);
                        }


                        //if ((xlRange.Cells[index, 8] as Excel.Range).Value2 != null)
                        //{
                        //    _wbsmodel.HousingAllowance = Convert.ToDouble((xlRange.Cells[index, 8] as Excel.Range).Value2);
                        //}

                        dynamic HousingAllowance = (xlRange.Cells[index, 8] as Excel.Range).Text;
                        _result = validateCommonAmount(HousingAllowance, _result, rowNumber, "Housing Allowance", out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.HousingAllowance = Convert.ToString(HousingAllowance);
                        }



                        //if ((xlRange.Cells[index, 9] as Excel.Range).Value2 != null)
                        //{
                        //    _wbsmodel.OtherEarnings = Convert.ToDouble((xlRange.Cells[index, 9] as Excel.Range).Value2);
                        //}

                        dynamic OtherEarnings = (xlRange.Cells[index, 9] as Excel.Range).Text;
                        _result = validateCommonAmount(OtherEarnings, _result, rowNumber, "Other Earnings", out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.OtherEarnings = Convert.ToString(OtherEarnings);
                        }



                        //if ((xlRange.Cells[index, 10] as Excel.Range).Value2 != null)
                        //{
                        //    _wbsmodel.Deductions = Convert.ToDouble((xlRange.Cells[index, 10] as Excel.Range).Value2);
                        //}

                        dynamic Deductions = (xlRange.Cells[index, 10] as Excel.Range).Text;
                        _result = validateCommonAmount(Deductions, _result, rowNumber, GlobalRes.Deductions, out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.Deductions = Convert.ToString(Deductions);
                        }





                        //if ((xlRange.Cells[index, 11] as Excel.Range).Value2 != null)
                        //{
                        //    _wbsmodel.Address = (string)(xlRange.Cells[index, 11] as Excel.Range).Value2;
                        //}

                        dynamic Address = (xlRange.Cells[index, 11] as Excel.Range).Text;
                        _result = validateAddress(Address, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.Address = Convert.ToString(Address);
                        }


                        // _wbsmodel.Status = (string)(xlRange.Cells[index, 12] as Excel.Range).Value2;

                        dynamic Status = (xlRange.Cells[index, 12] as Excel.Range).Text;
                        _result = validateEmpActive(Status, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.Status = Convert.ToString(Status);
                        }

                        //if ((xlRange.Cells[index, 13] as Excel.Range).Value2 != null)
                        //{
                        //    _wbsmodel.PaymentDesc = (string)(xlRange.Cells[index, 13] as Excel.Range).Value2;
                        //}

                        dynamic PaymentDesc = (xlRange.Cells[index, 13] as Excel.Range).Text;
                        _result = validatePaymentDescr(PaymentDesc, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.PaymentDesc = Convert.ToString(PaymentDesc);
                        }


                        //if ((xlRange.Cells[index, 14] as Excel.Range).Value2 != null)
                        //{
                        //    _wbsmodel.PaymentRef = (string)(xlRange.Cells[index, 14] as Excel.Range).Value2;
                        //}

                        dynamic PaymentRef = (xlRange.Cells[index, 14] as Excel.Range).Text;
                        _result = validatePaymentRef(PaymentRef, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.PaymentRef = Convert.ToString(PaymentRef);
                        }

                        //if ((xlRange.Cells[index, 15] as Excel.Range).Value2 != null)
                        //{
                        //    _wbsmodel.Currency = (string)(xlRange.Cells[index, 15] as Excel.Range).Value2;
                        //}

                        dynamic _currency = (xlRange.Cells[index, 15] as Excel.Range).Text;
                        _result = validateHCurrency(_currency, _result, rowNumber, out isvalid);
                        if (isvalid)
                        {
                            _wbsmodel.Currency = Convert.ToString(_currency);
                        }
                        wbsmodelList.Add(_wbsmodel);
                        srno++;
                    }

                    xlWorkBook.Close();
                    xlApp.Quit();

                    Marshal.ReleaseComObject(xlWorkSheet);
                    Marshal.ReleaseComObject(xlWorkBook);
                    Marshal.ReleaseComObject(xlApp);
                }
                catch (Exception ex)
                {
                    xlWorkBook.Close();
                    xlApp.Quit();

                    Marshal.ReleaseComObject(xlWorkSheet);
                    Marshal.ReleaseComObject(xlWorkBook);
                    Marshal.ReleaseComObject(xlApp);
                    throw ex;
                }

                if (wbsmodelList != null && wbsmodelList.Any() && _result.Count == 0)
                {
                    // _result = CommonValidation(_model, _result);
                    //_result = CreditDateValidation(_model.CreditDate, _result, "Credit");
                    // _result = WBSUploadValidation(wbsmodelList, _result);

                    if (_result.Count == 0)
                    {
                        _result = WBS_GenerateTextFile(wbsmodelList, _model, _result);
                    }
                }
                else if (_result.Count > 0)
                {
                    // _result.Add("Invalid file");
                }
                else
                {
                    _result.Add(GlobalRes.Error_InvalidFile);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _result;
        }

        public List<string> WBS_GenerateTextFile(List<clsWBS> _modelList, Registration _regmodel, List<string> _errormsg)
        {
            try
            {
                int srno = 0;
                var dd = _regmodel.CreditDate.Split('/')[0];
                var mm = _regmodel.CreditDate.Split('/')[1];
                var yy = _regmodel.CreditDate.Split('/')[2];

                DateTime startdate = new DateTime(2005, 01, 01);
                DateTime vCreditDate = new DateTime(Convert.ToInt32(yy), Convert.ToInt32(mm), Convert.ToInt32(dd));
                var iJlnDt = (vCreditDate - startdate).TotalDays;
                string serialno = string.Format("{0:0000}", 1);
                string batchno = string.IsNullOrEmpty(_regmodel.BatchNo) ? string.Format("{0:00}", 1) : string.Format("{0:00}", _regmodel.BatchNo);
                //string batchno = string.Format("{0:00}", 1);
                string filename = string.Format("{0}.txt", _regmodel.AgremeentCode + "-PR-" + yy + mm + dd + "-" + string.Format("0{0}", batchno));
                string filereference = "PAYROLLREF-PR-" + string.Format("{0:0000}", _regmodel.BatchNo) + "-" + DateTime.Now.ToString("yyyyMMdd") + DateTime.Now.ToString("HHmmss") + serialno;
                string txtfilepath = Path.Combine(@"~\UserFile\" + filename);
                HttpContext.Cache.Insert("phfilepath", filename, null, DateTime.UtcNow.AddMinutes(10), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                if (System.IO.File.Exists(Server.MapPath(txtfilepath)))
                {
                    string path = Path.Combine(Server.MapPath(destfolderpath), filename);
                    System.IO.File.Delete(path);
                }

                var vTmpPayRef = _regmodel.AgremeentCode.Substring(_regmodel.AgremeentCode.Length - 6, 6) +
                     string.Format("{0:00000}", iJlnDt);

                if (!System.IO.File.Exists(Server.MapPath(txtfilepath)))
                {
                    using (var stream = new FileStream(Server.MapPath(txtfilepath), FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        //srno = srno + 1;
                        string fieldToStr = "";
                        StreamWriter writer = new StreamWriter(stream);
                        writer.Write("111");

                        fieldToStr = _regmodel.AgremeentCode.Trim();
                        var length = fieldToStr.Length;
                        writer.Write(fieldToStr);
                        while (length < 8)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        fieldToStr = yy + mm + dd;
                        length = fieldToStr.Length;
                        writer.Write(fieldToStr);
                        while (length < 8)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        length = filereference.Length;
                        writer.Write(filereference);
                        while (length < 35)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        fieldToStr = _regmodel.CommerceRoomId;
                        length = fieldToStr.Length;
                        writer.Write(fieldToStr);
                        while (length < 10)
                        {
                            writer.Write(" ");
                            length++;
                        }


                        fieldToStr = _regmodel.BankCode;
                        length = fieldToStr.Length;
                        writer.Write(fieldToStr);
                        while (length < 4)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        fieldToStr = _regmodel.FundingAccount;
                        length = fieldToStr.Length;
                        writer.Write(fieldToStr);
                        while (length < 24)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        fieldToStr = _regmodel.Currency;
                        length = fieldToStr.Length;
                        writer.Write(fieldToStr);
                        while (length < 3)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        fieldToStr = _regmodel.MinsOfLabEstablishedId;
                        int _length = fieldToStr.Length;
                        if (fieldToStr.Substring(1, 1) == "0")
                        {
                            writer.Write(" ");
                            fieldToStr = fieldToStr.Substring(2, _length - 1);
                            writer.Write(fieldToStr);
                            while (_length < 18)
                            {
                                writer.Write(" ");
                                _length++;
                            }
                        }
                        else
                        {
                            writer.Write(fieldToStr);
                            while (_length < 18)
                            {
                                writer.Write(" ");
                                _length++;
                            }
                        }

                        writer.WriteLine("");

                        double totalNetAmount = 0;
                        foreach (var _model in _modelList)
                        {
                            srno = srno + 1;
                            writer.Write(112);

                            fieldToStr = string.Format("{0:000000}", srno);
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 6)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.PaymentRef;
                            length = fieldToStr.Length;
                            if (length > 16)
                            {
                                fieldToStr = fieldToStr.Substring(0, 16);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 16)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.BeneficiaryId;
                            if (fieldToStr.Length > 10)
                            {
                                fieldToStr = fieldToStr.Substring(0, 10);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 10)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            writer.Write("             ");

                            fieldToStr = _model.AccountNumber.ToString();
                            length = fieldToStr.Length;
                            if (length > 34)
                            {
                                fieldToStr = fieldToStr.Substring(0, 34);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 34)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = string.Format("{0:000000000000.00}", _model.NetAmount);
                            length = fieldToStr.Length;
                            if (length > 15)
                            {
                                fieldToStr = fieldToStr.Substring(0, 15);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 15)
                            {
                                writer.Write(" ");
                                length++;
                            }


                            fieldToStr = _model.Currency;
                            length = fieldToStr.Length;
                            if (length > 3)
                            {
                                fieldToStr = fieldToStr.Substring(0, 3);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 3)
                            {
                                writer.Write(" ");
                                length++;
                            }
                            writer.Write("                                                  ");//50 spaces
                            writer.Write("                                                  ");//49 spaces


                            fieldToStr = _model.PaymentDesc != null ? _model.PaymentDesc : string.Empty;
                            length = fieldToStr.Length;
                            if (length > 140)
                            {
                                fieldToStr = fieldToStr.Substring(0, 140);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 140)
                            {
                                writer.Write(" ");
                                length++;
                            }


                            fieldToStr = _model.EmployeeName;
                            length = fieldToStr.Length;
                            if (length > 35)
                            {
                                fieldToStr = fieldToStr.Substring(0, 35);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 35)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.Address != null ? _model.Address : string.Empty;
                            length = fieldToStr.Length;
                            if (length > 105)
                            {
                                fieldToStr = fieldToStr.Substring(0, 105);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 105)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.BankCode.Trim();
                            length = fieldToStr.Length;
                            if (length > 11)
                            {
                                fieldToStr = fieldToStr.Substring(0, 11);
                            }
                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 11)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            writer.Write("                                                  ");//50 spaces
                            writer.Write("                                                  ");//50 spaces
                            writer.Write("                             ");//29 spaces


                            fieldToStr = string.Format("{0:000000000.00}", _model.BasicSalary);
                            length = fieldToStr.Length;

                            writer.Write(fieldToStr);
                            while (length < 12)
                            {
                                writer.Write(" ");
                                length++;
                            }


                            fieldToStr = string.Format("{0:000000000.00}", _model.HousingAllowance);
                            length = fieldToStr.Length;
                            if (length > 12)
                            {
                                fieldToStr = fieldToStr.Substring(0, 12);
                            }

                            writer.Write(fieldToStr);
                            length = fieldToStr.Length;
                            while (length < 12)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = string.Format("{0:000000000.00}", _model.OtherEarnings);
                            length = fieldToStr.Length;

                            writer.Write(fieldToStr);
                            while (length < 12)
                            {
                                writer.Write(" ");
                                length++;
                            }


                            fieldToStr = string.Format("{0:000000000.00}", _model.Deductions);
                            length = fieldToStr.Length;

                            writer.Write(fieldToStr);
                            while (length < 12)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            writer.Write("                                 ");//33 spaces
                            writer.WriteLine("");

                            totalNetAmount = totalNetAmount + Convert.ToDouble(string.Format("{0:000000000000.00}", _model.NetAmount));
                            //srno++;
                        }
                        writer.WriteLine();
                        writer.Write(string.Format("{0}{1}{2}", 999, string.Format("{0:000000000000000.00}", totalNetAmount), string.Format("{0:000000}", srno)));
                        writer.Close();

                        clsAL _clsal = new clsAL()
                        {
                            AgremeentCode = _regmodel.AgremeentCode,
                            AccountNumber = _regmodel.FundingAccount,
                            TotalRecords = srno.ToString(),
                            TotalAmount = totalNetAmount.ToString(),
                            ValueCreditDate = _regmodel.CreditDate
                        };

                        var ALName = PDFGenerator(_clsal);
                        HttpContext.Cache.Insert("ALName", ALName, null, DateTime.UtcNow.AddMinutes(10), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                    }
                }
            }
            catch (Exception ex)
            {
                _errormsg.Add(ex.Message);
            }

            return _errormsg;
        }

        #endregion

        #region Upload Excel sheet and generate text file for RSD

        public List<string> ReadRSDExcelSheet(string filePath)
        {
            List<string> _result = new List<string>();
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkBook = xlApp.Workbooks.Open(filePath, 0, false, 5, "", "", false,
                Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            List<clsRSD> rsdmodelList = new List<clsRSD>();
            clsRSD _rsdmodel;
            Registration _model = new Registration();
            int rowNumber = 2;
            Excel.Range xlRange = xlWorkSheet.UsedRange;

            try
            {
                bool isvalid = false;
                // _model.CompanyName = (string)(xlRange.Cells[rowNumber, 1] as Excel.Range).Value2;

                dynamic CompanyName = (xlRange.Cells[rowNumber, 1] as Excel.Range).Text;
                _result = validateHAgreementCode(CompanyName, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.CompanyName = Convert.ToString(CompanyName);
                }

                // _model.AgremeentCode = (string)(xlRange.Cells[rowNumber, 2] as Excel.Range).Value2;

                dynamic agreementcode = (xlRange.Cells[rowNumber, 2] as Excel.Range).Text;
                _result = validateHAgreementCode(agreementcode, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.AgremeentCode = Convert.ToString(agreementcode);
                }

                //_model.FundingAccount = (string)(xlRange.Cells[rowNumber, 3] as Excel.Range).Value2;

                dynamic FundingAccount = (xlRange.Cells[rowNumber, 3] as Excel.Range).Text;
                _result = validateHFundingAccount(FundingAccount, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.FundingAccount = Convert.ToString(FundingAccount);
                }

                // _model.BranchNo = (string)(xlRange.Cells[rowNumber, 4] as Excel.Range).Value2;

                dynamic branchNo = (xlRange.Cells[rowNumber, 4] as Excel.Range).Text;
                _result = validateHBranchNo(branchNo, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _result = validateHBranchNoWithAC(branchNo, FundingAccount, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _model.BranchNo = Convert.ToString(branchNo);
                    }
                }

                //_model.ValueDate = (string)(xlRange.Cells[rowNumber, 5] as Excel.Range).Value2;

                dynamic ValueDate = (xlRange.Cells[rowNumber, 5] as Excel.Range).Text;
                _result = CreditDateValidation(ValueDate, _result, "Value", rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.ValueDate = Convert.ToString(ValueDate);
                }

                //  _model.DepartmentName = (string)(xlRange.Cells[rowNumber, 6] as Excel.Range).Value2;

                dynamic DepartmentName = (xlRange.Cells[rowNumber, 6] as Excel.Range).Text;
                _result = validateDepartmentName_RSD(DepartmentName, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.DepartmentName = Convert.ToString(DepartmentName);
                }

                //_model.DivisionNumber = (string)(xlRange.Cells[rowNumber, 7] as Excel.Range).Value2;

                dynamic DivisionNumber = (xlRange.Cells[rowNumber, 7] as Excel.Range).Text;
                _result = validateHDivisionNumber_RSD(DivisionNumber, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.DivisionNumber = Convert.ToString(DivisionNumber);
                }

                //  _model.SubDivisionNumber = (string)(xlRange.Cells[rowNumber, 8] as Excel.Range).Value2;

                dynamic SubDivisionNumber = (xlRange.Cells[rowNumber, 8] as Excel.Range).Text;
                _result = validateHSubDivisionNumber_RSD(SubDivisionNumber, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.SubDivisionNumber = Convert.ToString(SubDivisionNumber);
                }

                //_model.SectorNumber = (string)(xlRange.Cells[rowNumber, 9] as Excel.Range).Value2;


                dynamic SectorNumber = (xlRange.Cells[rowNumber, 9] as Excel.Range).Text;
                _result = validateHSector_RSD(SectorNumber, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.SectorNumber = Convert.ToString(SectorNumber);
                }

                // _model.ClassNumber = (string)(xlRange.Cells[rowNumber, 10] as Excel.Range).Value2;
                dynamic ClassNumber = (xlRange.Cells[rowNumber, 10] as Excel.Range).Text;
                _result = validateHClassNumber_RSD(ClassNumber, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.ClassNumber = Convert.ToString(ClassNumber);
                }

                // _model.PaymentOrderNumber = (string)(xlRange.Cells[rowNumber, 11] as Excel.Range).Value2;

                dynamic PaymentOrderNumber = (xlRange.Cells[rowNumber, 11] as Excel.Range).Text;
                _result = validateHPaymentOrderNo_RSD(PaymentOrderNumber, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.PaymentOrderNumber = Convert.ToString(PaymentOrderNumber);
                }

                //  _model.LabelMsg = (string)(xlRange.Cells[rowNumber, 12] as Excel.Range).Value2;

                dynamic LabelMsg = (xlRange.Cells[rowNumber, 12] as Excel.Range).Text;
                _result = validateHLabel_RSD(LabelMsg, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.LabelMsg = Convert.ToString(LabelMsg);
                }

                //_model.BankCode = (string)(xlRange.Cells[rowNumber, 13] as Excel.Range).Value2;


                dynamic BankCode = (xlRange.Cells[rowNumber, 13] as Excel.Range).Text;
                _result = validateBankCode(BankCode, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.BankCode = Convert.ToString(BankCode);
                }

                // _model.Currency = (string)(xlRange.Cells[rowNumber, 14] as Excel.Range).Value2;


                dynamic Currency = (xlRange.Cells[rowNumber, 14] as Excel.Range).Text;
                _result = validateHCurrency(Currency, _result, rowNumber, out isvalid);
                if (isvalid)
                {
                    _model.Currency = Convert.ToString(Currency);
                }

                dynamic BatchNo = (xlRange.Cells[rowNumber, 15] as Excel.Range).Text;
                _model.BatchNo = Convert.ToString(BatchNo);

                //Iterate the rows in the used range
                int srno = 1;
                for (int index = 4; index <= xlRange.Rows.Count; index++)
                {
                    _rsdmodel = new clsRSD();
                    _rsdmodel.SrNo = srno.ToString();

                    //_rsdmodel.BeneficiaryRef = (string)(xlRange.Cells[index, 2] as Excel.Range).Value2;

                    dynamic BeneficiaryId = (xlRange.Cells[index, 2] as Excel.Range).Text;
                    _result = validateBenefRef(BeneficiaryId, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.BeneficiaryRef = Convert.ToString(BeneficiaryId);
                    }


                    //if ((xlRange.Cells[index, 3] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.Iqama = Convert.ToString((xlRange.Cells[index, 3] as Excel.Range).Value2);
                    //}

                    dynamic Iqama = (xlRange.Cells[index, 3] as Excel.Range).Text;
                    _result = validateIqma_RSD(Iqama, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.Iqama = Convert.ToString(Iqama);
                    }

                    //if ((xlRange.Cells[index, 4] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.BeneficiaryName = (string)(xlRange.Cells[index, 4] as Excel.Range).Value2;
                    //}

                    dynamic BeneficiaryName = (xlRange.Cells[index, 4] as Excel.Range).Text;
                    _result = validateBenefName(BeneficiaryName, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.BeneficiaryName = Convert.ToString(BeneficiaryName);
                    }

                    dynamic AccountNumber = (xlRange.Cells[index, 5] as Excel.Range).Text;
                    _result = validateHAccountNo(AccountNumber, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.AccountNumber = Convert.ToString(AccountNumber);
                    }


                    //if ((xlRange.Cells[index, 5] as Excel.Range).Value2 != null)
                    //{
                    //    //   var val = Convert.ToString(Convert.ToDouble((xlRange.Cells[index, 5] as Excel.Range).Value2));
                    //    var val = Convert.ToString(Convert.ToDouble((xlRange.Cells[index, 5] as Excel.Range).Value2));
                    //    _rsdmodel.AccountNumber = Convert.ToString(Decimal.Parse(val, System.Globalization.NumberStyles.Any));
                    //    //_rsdmodel.AccountNumber = Convert.ToDouble((xlRange.Cells[index, 5] as Excel.Range).Value2);
                    //}

                    //if ((xlRange.Cells[index, 6] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.BankCode = (string)(xlRange.Cells[index, 6] as Excel.Range).Value2;
                    //}

                    dynamic _bankCode = (xlRange.Cells[index, 6] as Excel.Range).Text;
                    _result = validateHBankCode_RSD(_bankCode, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.BankCode = Convert.ToString(_bankCode);
                    }

                    dynamic NetAmount = (xlRange.Cells[index, 7] as Excel.Range).Text;
                    _result = validateNetAmount(NetAmount, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.NetAmount = Convert.ToDouble(NetAmount);
                    }

                    //if ((xlRange.Cells[index, 7] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.NetAmount = Convert.ToDouble((xlRange.Cells[index, 7] as Excel.Range).Value2);
                    //}

                    dynamic BasicSalary = (xlRange.Cells[index, 8] as Excel.Range).Text;
                    _result = validateAmount(BasicSalary, _result, GlobalRes.BasicSalary, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.BasicSalary = Convert.ToDouble(BasicSalary);
                    }

                    //if ((xlRange.Cells[index, 8] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.BasicSalary = Convert.ToDouble((xlRange.Cells[index, 8] as Excel.Range).Value2);
                    //}

                    //if ((xlRange.Cells[index, 9] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.OtherEarnings = Convert.ToDouble((xlRange.Cells[index, 9] as Excel.Range).Value2);
                    //}
                    dynamic OtherEarnings = (xlRange.Cells[index, 9] as Excel.Range).Text;
                    _result = validateAmount(OtherEarnings, _result, GlobalRes.OtherEarnings, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.OtherEarnings = Convert.ToDouble(OtherEarnings);
                    }


                    //if ((xlRange.Cells[index, 10] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.Deductions = Convert.ToDouble((xlRange.Cells[index, 10] as Excel.Range).Value2);
                    //}

                    dynamic Deductions = (xlRange.Cells[index, 10] as Excel.Range).Text;
                    _result = validateAmount(Deductions, _result, GlobalRes.Deductions, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.Deductions = Convert.ToDouble(Deductions);
                    }

                    //if ((xlRange.Cells[index, 11] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.AccrualMonth = Convert.ToString((double)(xlRange.Cells[index, 11] as Excel.Range).Value2);
                    //}

                    dynamic AccrualMonth = (xlRange.Cells[index, 11] as Excel.Range).Text;
                    _result = validateAccrualMonth_RSD(AccrualMonth, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.AccrualMonth = Convert.ToString(AccrualMonth);
                    }

                    //if ((xlRange.Cells[index, 12] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.PaymentDesc = (string)(xlRange.Cells[index, 12] as Excel.Range).Value2;
                    //}

                    dynamic PaymentDesc = (xlRange.Cells[index, 12] as Excel.Range).Text;
                    _result = validatePaymentDescr(PaymentDesc, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.PaymentDesc = Convert.ToString(PaymentDesc);
                    }


                    dynamic PaymentRef = (xlRange.Cells[index, 13] as Excel.Range).Text;
                    _result = validatePaymentRef(PaymentRef, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.PaymentRef = Convert.ToString(PaymentRef);
                    }

                    dynamic _Currency = (xlRange.Cells[index, 14] as Excel.Range).Text;
                    _result = validateHCurrency(_Currency, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.Currency = Convert.ToString(_Currency);
                    }

                    dynamic _address = (xlRange.Cells[index, 15] as Excel.Range).Text;
                    _result = validateAddress(_address, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.Address = Convert.ToString(_address);
                    }


                    dynamic City = (xlRange.Cells[index, 16] as Excel.Range).Text;
                    _result = validateCity(City, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.City = Convert.ToString(City);
                    }

                    dynamic ZipCode = (xlRange.Cells[index, 17] as Excel.Range).Text;
                    _result = validateZipCode(ZipCode, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.ZipCode = Convert.ToString(ZipCode);
                    }




                    //if ((xlRange.Cells[index, 13] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.PaymentRef = (string)(xlRange.Cells[index, 13] as Excel.Range).Value2;
                    //}
                    //if ((xlRange.Cells[index, 14] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.Currency = (string)(xlRange.Cells[index, 14] as Excel.Range).Value2;
                    //}
                    //if ((xlRange.Cells[index, 15] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.PaymentRef = (string)(xlRange.Cells[index, 15] as Excel.Range).Value2;
                    //}

                    //  _rsdmodel.City = (string)(xlRange.Cells[index, 16] as Excel.Range).Value2;
                    //if ((xlRange.Cells[index, 17] as Excel.Range).Value2 != null)
                    //{
                    //    _rsdmodel.ZipCode = (double)(xlRange.Cells[index, 17] as Excel.Range).Value2;
                    //}

                    dynamic Apprentice = (xlRange.Cells[index, 18] as Excel.Range).Text;
                    _result = validateApprentice_RSD(Apprentice, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.Apprentice = Convert.ToString(Apprentice);
                    }

                    //_rsdmodel.Apprentice = (string)(xlRange.Cells[index, 18] as Excel.Range).Value2;

                    dynamic CareerLadder = (xlRange.Cells[index, 19] as Excel.Range).Text;
                    _result = validateCareerLadder_RSD(CareerLadder, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.CareerLadder = Convert.ToString(CareerLadder);
                    }

                    // _rsdmodel.CareerLadder = (string)(xlRange.Cells[index, 19] as Excel.Range).Value2;
                    //_rsdmodel.Nationality = (string)(xlRange.Cells[index, 20] as Excel.Range).Value2;
                    dynamic Nationality = (xlRange.Cells[index, 20] as Excel.Range).Text;
                    _result = validateNationality_RSD(Nationality, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.Nationality = Convert.ToString(Nationality);
                    }

                    // _rsdmodel.Status = (string)(xlRange.Cells[index, 21] as Excel.Range).Value2;


                    dynamic Status = (xlRange.Cells[index, 21] as Excel.Range).Text;
                    _result = validateEmpActive(Status, _result, rowNumber, out isvalid);
                    if (isvalid)
                    {
                        _rsdmodel.Status = Convert.ToString(Status);
                    }

                    _rsdmodel.Remarks = (string)(xlRange.Cells[index, 22] as Excel.Range).Value2;

                    rsdmodelList.Add(_rsdmodel);
                    srno++;
                }

                xlWorkBook.Close();
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);

                //_result = ModelValidation(_model);
                //_result = RSDModelValidation(rsdmodelList, _model, _result);
            }
            catch (Exception ex)
            {
                xlWorkBook.Close();
                xlApp.DisplayAlerts = false;
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);
                throw ex;
            }

            if (rsdmodelList != null && rsdmodelList.Any() && _result.Count == 0)
            {
                //_result = CommonValidation(_model, _result);
                //_result = CreditDateValidation(_model.ValueDate, _result, "Credit");
                //_result = RSDUploadValidation(rsdmodelList, _model, _result);

                if (_result.Count == 0)
                {
                    _result = RSD_GenerateTextFile(rsdmodelList, _model, _result);
                }
            }
            //else if(_result.Count>0)
            //{
            //    _result.Add("Invalid file");
            //}


            return _result;
        }

        public List<string> RSD_GenerateTextFile(List<clsRSD> _modelList, Registration _regmodel, List<string> _errormsg)
        {
            try
            {
                //  _errormsg = RSDUploadValidation(_modelList, _regmodel, _errormsg);
                //if (_errormsg.Count == 0)
                //{
                var dd = _regmodel.ValueDate.Split('/')[0];
                var mm = _regmodel.ValueDate.Split('/')[1];
                var yy = _regmodel.ValueDate.Split('/')[2];
                string serialno = string.Format("{0:0000}", 1);
                // string batchno = string.Format("{0:00}", 1);
                string batchno = string.IsNullOrEmpty(_regmodel.BatchNo) ? string.Format("{0:00}", 1) : string.Format("{0:00}", _regmodel.BatchNo);
                string filename = string.Format("{0}.txt", _regmodel.AgremeentCode + "-PR-" + yy + mm + dd + "-" + string.Format("0{0}", batchno));
                string filereference = "PAYROLLREF-PR-" + string.Format("{0:0000}", _regmodel.BatchNo) + "-" + DateTime.Now.ToString("yyyyMMdd") + DateTime.Now.ToString("HHmmss") + serialno;
                string txtfilepath = Path.Combine(@"~\UserFile\" + filename);
                HttpContext.Cache.Insert("phfilepath", filename, null, DateTime.UtcNow.AddMinutes(10), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                if (System.IO.File.Exists(Server.MapPath(txtfilepath)))
                {
                    string path = Path.Combine(Server.MapPath(destfolderpath), filename);
                    System.IO.File.Delete(path);
                }
                if (!System.IO.File.Exists(Server.MapPath(txtfilepath)))
                {
                    using (var stream = new FileStream(Server.MapPath(txtfilepath), FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        StreamWriter writer = new StreamWriter(stream);
                        writer.Write("333");
                        writer.Write(_regmodel.AgremeentCode);
                        var length = _regmodel.AgremeentCode.Length;

                        while (length < 8)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        string newdate = yy + mm + dd;
                        writer.Write(newdate);

                        length = newdate.Length;
                        while (length < 8)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        writer.Write(_regmodel.LabelMsg);
                        length = _regmodel.LabelMsg != null ? _regmodel.LabelMsg.Length : 0;
                        while (length < 35)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        writer.Write(_regmodel.CompanyName);
                        length = _regmodel.CompanyName.Length;
                        while (length < 150)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        writer.Write(_regmodel.DepartmentName);
                        length = _regmodel.DepartmentName.Length;
                        while (length < 150)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        writer.Write(_regmodel.FundingAccount);
                        length = _regmodel.FundingAccount.Length;
                        while (length < 34)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        writer.Write(_regmodel.PaymentOrderNumber);
                        length = _regmodel.PaymentOrderNumber.Length;
                        while (length < 15)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        string _SectorNumber = string.Format("{0:00}", _regmodel.SectorNumber);
                        writer.Write(_SectorNumber);
                        length = _SectorNumber.Length;
                        while (length < 2)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        string _ClassNumber = string.Format("{0:000}", _regmodel.ClassNumber);
                        writer.Write(_ClassNumber);
                        length = _ClassNumber.Length;
                        while (length < 3)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        string _BranchNo = string.Format("{0:000}", _regmodel.BranchNo);
                        writer.Write(_BranchNo);
                        length = _BranchNo.Length;
                        while (length < 3)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        string _DivisionNumber = string.Format("{0:000}", _regmodel.DivisionNumber);
                        writer.Write(_DivisionNumber);
                        length = _DivisionNumber.Length;
                        while (length < 3)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        string _SubDivisionNumber = string.Format("{0:000}", _regmodel.SubDivisionNumber);
                        writer.Write(_SubDivisionNumber);
                        length = _SubDivisionNumber.Length;
                        while (length < 3)
                        {
                            writer.Write(" ");
                            length++;
                        }

                        int srno = 0;
                        double totalNetAmount = 0;
                        foreach (var _model in _modelList)
                        {
                            srno = srno+1;
                            _model.SrNo = srno.ToString();
                            writer.WriteLine();

                            string rRecordNo = string.Format("{0:000000}", 112);
                            writer.Write(rRecordNo);
                            length = rRecordNo.Length;
                            while (length < 6)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            string fieldToStr = _model.PaymentRef;
                            length = fieldToStr.Length;
                            if (length > 16)
                            {
                                fieldToStr = fieldToStr.Substring(0, 16);
                            }
                            writer.Write(fieldToStr);
                            while (length < 16)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.BeneficiaryRef;
                            length = fieldToStr.Length;
                            if (length > 12)
                            {
                                fieldToStr = fieldToStr.Substring(0, 12);
                            }
                            writer.Write(fieldToStr);
                            while (length < 12)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.BankCode;
                            length = fieldToStr.Length;
                            if (length > 12)
                            {
                                fieldToStr = fieldToStr.Substring(0, 11);
                            }
                            writer.Write(fieldToStr);
                            while (length < 11)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.AccountNumber.ToString();
                            length = fieldToStr.Length;
                            if (length > 34)
                            {
                                fieldToStr = fieldToStr.Substring(0, 34);
                            }
                            writer.Write(fieldToStr);
                            while (length < 34)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = string.Format("{0:000000000.00}", _model.NetAmount);
                            length = fieldToStr.Length;
                            if (length > 15)
                            {
                                fieldToStr = fieldToStr.Substring(0, 15);
                            }
                            writer.Write(fieldToStr);
                            while (length < 15)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = string.Format("{0:000000000.00}", _model.NetAmount);
                            length = fieldToStr.Length;
                            if (length > 15)
                            {
                                fieldToStr = fieldToStr.Substring(0, 15);
                            }
                            writer.Write(fieldToStr);
                            while (length < 15)
                            {
                                writer.Write(" ");
                                length++;
                            }


                            fieldToStr = _model.Currency;
                            length = fieldToStr.Length;
                            if (length > 3)
                            {
                                fieldToStr = fieldToStr.Substring(0, 3);
                            }
                            writer.Write(fieldToStr);
                            while (length < 3)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.Address != null ? _model.Address : string.Empty;
                            length = fieldToStr.Length;
                            if (length > 35)
                            {
                                fieldToStr = fieldToStr.Substring(0, 35);
                            }
                            writer.Write(fieldToStr);
                            while (length < 35)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.City;
                            length = fieldToStr.Length;
                            if (length > 20)
                            {
                                fieldToStr = fieldToStr.Substring(0, 20);
                            }
                            writer.Write(fieldToStr);
                            while (length < 20)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.ZipCode != null ? _model.ZipCode.ToString() : "0";
                            length = fieldToStr.Length;
                            if (length > 9)
                            {
                                fieldToStr = fieldToStr.Substring(0, 9);
                            }
                            writer.Write(fieldToStr);
                            while (length < 9)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.BeneficiaryName;
                            length = fieldToStr.Length;
                            if (length > 35)
                            {
                                fieldToStr = fieldToStr.Substring(0, 35);
                            }
                            writer.Write(fieldToStr);
                            while (length < 35)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            length = 1;
                            while (length <= 200)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.PaymentDesc != null ? _model.PaymentDesc : string.Empty;
                            length = fieldToStr.Length;
                            if (length > 140)
                            {
                                fieldToStr = fieldToStr.Substring(0, 140);
                            }
                            writer.Write(fieldToStr);
                            while (length < 140)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.Iqama;
                            length = fieldToStr.Length;
                            if (length > 18)
                            {
                                fieldToStr = fieldToStr.Substring(0, 18);
                            }
                            writer.Write(fieldToStr);
                            while (length < 18)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.Nationality;
                            length = fieldToStr.Length;
                            if (length > 200)
                            {
                                fieldToStr = fieldToStr.Substring(0, 200);
                            }
                            writer.Write(fieldToStr);
                            while (length < 200)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = string.Format("{0:000000000000.00}", _model.BasicSalary);
                            length = fieldToStr.Length;

                            writer.Write(fieldToStr);
                            while (length < 15)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = string.Format("{0:000000000000.00}", _model.OtherEarnings);
                            length = fieldToStr.Length;

                            writer.Write(fieldToStr);
                            while (length < 15)
                            {
                                writer.Write(" ");
                                length++;
                            }


                            fieldToStr = string.Format("{0:000000000000.00}", _model.Deductions);
                            length = fieldToStr.Length;

                            writer.Write(fieldToStr);
                            while (length < 15)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.AccrualMonth != null ? _model.AccrualMonth : string.Empty; ;
                            length = fieldToStr.Length;
                            writer.Write(fieldToStr);
                            while (length < 6)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.CareerLadder != null ? _model.CareerLadder : string.Empty;
                            length = fieldToStr.Length;
                            writer.Write(fieldToStr);
                            while (length < 50)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            fieldToStr = _model.Apprentice != null ? _model.Apprentice : string.Empty;
                            length = fieldToStr.Length;
                            writer.Write(fieldToStr);
                            while (length < 1)
                            {
                                writer.Write(" ");
                                length++;
                            }

                            writer.WriteLine("                                         ");
                            writer.WriteLine("");

                            totalNetAmount = totalNetAmount + Convert.ToDouble(string.Format("{0:000000000.00}", _model.NetAmount));

                            //srno++;
                        }
                        writer.WriteLine();
                        writer.Write(string.Format("{0}{1}{2}", 999, string.Format("{0:000000000000000.00}", totalNetAmount), string.Format("{0:000000}", srno)));
                        writer.Close();

                        clsAL _clsal = new clsAL()
                        {
                            AgremeentCode = _regmodel.AgremeentCode,
                            AccountNumber = _regmodel.FundingAccount,
                            TotalRecords = srno.ToString(),
                            TotalAmount = totalNetAmount.ToString(),
                            ValueCreditDate = _regmodel.ValueDate
                        };

                        var ALName = PDFGenerator(_clsal);
                        HttpContext.Cache.Insert("ALName", ALName, null, DateTime.UtcNow.AddMinutes(10), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);


                    }
                }
                //}
            }
            catch (Exception ex)
            {
                _errormsg.Add(ex.Message);
            }

            return _errormsg;
        }

        #endregion        

        #region Functions

        public FileResult TextDownload(string filename)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath(destfolderpath + filename));
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<SelectListItem> GetJobTypes()
        {
            var countries = _com.GetJobTypeList()
                .Select(x => new SelectListItem { Text = x.value, Value = x.id.ToString() })
                .ToList();
            countries.Insert(0, new SelectListItem { Text = GlobalRes.ChooseJobType, Value = "" });
            return countries;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<SelectListItem> GetRsdList()
        {
            var countries = _com.getRSDList()
                .Select(x => new SelectListItem { Text = x.value, Value = x.id.ToString() })
                .ToList();
            countries.Insert(0, new SelectListItem { Text = GlobalRes.Select, Value = "" });
            return countries;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IList<SelectListItem> GetWBSList()
        {
            var countries = _com.getWBSList()
                .Select(x => new SelectListItem { Text = x.value, Value = x.id.ToString() })
                .ToList();
            countries.Insert(0, new SelectListItem { Text = GlobalRes.Select, Value = "" });
            return countries;
        }


        public IList<SelectListItem> GetCurrencyList()
        {
            var currencies = _com.getCurrencyList()
                .Select(x => new SelectListItem { Text = x.value, Value = x.id.ToString() })
                .ToList();
            currencies.Insert(0, new SelectListItem { Text = GlobalRes.Select, Value = "" });
            return currencies;
        }


        public void deleteAllfiles()
        {
            try
            {
                string[] files = System.IO.Directory.GetFiles(Server.MapPath(destfolderpath));
                foreach (string fi in files)
                {
                    var creationTime = System.IO.File.GetCreationTime(fi);
                    if (creationTime < (DateTime.Now - new TimeSpan(1, 0, 0, 0)))
                    {
                        System.IO.File.Delete(fi);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }


        //public static void BuildRow(this StringBuilder source, string title, string value)
        //{
        //    source.Append("<tr>");
        //    source.Append("<td>");
        //    source.Append(title);
        //    source.Append("</td>");
        //    source.Append("<td>");
        //    source.Append(value);
        //    source.Append("</td>");
        //    source.Append("</tr>");

        //}
        private string buildApplyOnlineHtml(clsAL _clsal)
        {
            StringBuilder applicationDetails = new StringBuilder();
            //applicationDetails.Append("<br/><br/><br/>");
            applicationDetails.Append("<table>");

            applicationDetails.Append("<tr><td>");
            applicationDetails.Append(string.Format("<h1>{0}</h1>", GlobalRes.ALHeader));
            applicationDetails.Append("</td></tr>");
            applicationDetails.Append("<br/>");
            applicationDetails.Append("<tr><td>");
            applicationDetails.Append(string.Format("{0}: {1}  {2}: {3}", GlobalRes.ALDate, DateTime.Now.ToShortDateString(), GlobalRes.ALTime, DateTime.Now.ToShortTimeString()));
            applicationDetails.Append("</td></tr>");
            applicationDetails.Append("<br/>");
            applicationDetails.Append("<tr><td>");
            applicationDetails.Append(string.Format("{0}: {1}", GlobalRes.ALFrom, _clsal.AccountNumber));
            applicationDetails.Append("</td></tr>");
            applicationDetails.Append("<br/>");
            applicationDetails.Append("<tr><td>");
            applicationDetails.Append(string.Format("{0} :{1}", GlobalRes.ALTo, GlobalRes.RBAddress));
            applicationDetails.Append("</td></tr>");
            applicationDetails.Append("<br/>");
            applicationDetails.Append("<tr><td>");
            applicationDetails.Append(string.Format("{0}: {1}", GlobalRes.ALSubjectHeader, GlobalRes.ALSubject));
            applicationDetails.Append("</td></tr>");

            applicationDetails.Append("<tr><td>");
            applicationDetails.Append(GlobalRes.ALBodyHeader1);
            applicationDetails.Append("</td></tr>");
            applicationDetails.Append("<tr><td>");
            applicationDetails.Append(GlobalRes.ALBody);
            applicationDetails.Append("</td></tr>");

            applicationDetails.Append("</table>");
            applicationDetails.Append("<br/>");
            applicationDetails.Append("<table class='tbldata'>");

            applicationDetails.BuildRow("Agreement Ref", _clsal.AgremeentCode);
            applicationDetails.BuildRow("Debit Account No", _clsal.AccountNumber);
            applicationDetails.BuildRow("Total Records", _clsal.TotalRecords);
            applicationDetails.BuildRow("Total Amount", _clsal.TotalAmount);
            applicationDetails.BuildRow("Value Credit Date", _clsal.ValueCreditDate);

            applicationDetails.Append("</table>");

            applicationDetails.Append("<table>");
            applicationDetails.Append("<tr><td>");
            applicationDetails.Append(GlobalRes.ALBody1);
            applicationDetails.Append("</td></tr>");
            applicationDetails.Append("<br/>");
            applicationDetails.Append("<tr><td>");
            applicationDetails.Append(GlobalRes.ALKindRegards);
            applicationDetails.Append("</td></tr>");
            applicationDetails.Append("<tr><td>");
            applicationDetails.Append("");
            applicationDetails.Append("</td></tr>");
            applicationDetails.Append("<tr><td>");
            applicationDetails.Append("");
            applicationDetails.Append("</td></tr>");
            applicationDetails.Append("<tr class='clsregards'><td class='clsregards'>");
            applicationDetails.Append("<p>_____________________________</p></br>" + GlobalRes.ALAuthorizedSignature);
            applicationDetails.Append("</td></tr>");
            //applicationDetails.Append("<tr ><td class='clsregards'>");
            //applicationDetails.Append("<p>Authorized Signature</p>");
            //applicationDetails.Append("</td></tr>");
            applicationDetails.Append("</table>");
            return applicationDetails.ToString();
        }

        #endregion
    }
}