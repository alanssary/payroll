﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace RB.Portal.Web.Helper
{
    public static class ExtensionMethods
    {
        public static void BuildRow(this StringBuilder source, string title, string value)
        {
            source.Append("<tr>");
            source.Append("<td>");
            source.Append(title);
            source.Append("</td>");
            source.Append("<td>");
            source.Append(value);
            source.Append("</td>");
            source.Append("</tr>");

        }
    }
}