﻿
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace RB.Portal.Web.Models
{
    /// <summary>
    /// This class responsible to geneate PDF
    /// </summary>
    public class Generator : IDisposable
    {

        string RBFontRegular = ConfigurationManager.AppSettings["RBFontRegular"];
        string RBFontLight = ConfigurationManager.AppSettings["RBFontLight"];
        string RBFontRegularAR = ConfigurationManager.AppSettings["RBFontRegularAR"];
        string RBFontLightAR = ConfigurationManager.AppSettings["RBFontLightAR"];
        string Logo = ConfigurationManager.AppSettings["Logo"];
        string Line = ConfigurationManager.AppSettings["Line"];
        /// <summary>
        /// Generate PDF as response stream
        /// </summary>
        /// <param name="html"></param>
        /// <param name="language"></param>
        /// <param name="filename"></param>
        /// <param name="tableborder"></param>
        ///  <param name="excludeBraces"></param>
        public void GeneratePDF(string html, string language, string filename, string filepath, bool tableborder, bool excludeBraces)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                WriteHtmlDoc(html, language, ms, tableborder, excludeBraces);
                //HttpContext.Current.Response.Buffer = false;
                //HttpContext.Current.Response.Clear();
                //HttpContext.Current.Response.ClearContent();
                //HttpContext.Current.Response.ClearHeaders();
                //HttpContext.Current.Response.AppendHeader("content-disposition", "attachment;filename=" + string.Format("{0}.pdf", filename));
                //HttpContext.Current.Response.ContentType = "application/pdf";
                //HttpContext.Current.Response.BinaryWrite(ms.ToArray());


                //filename = string.Format("{0}.pdf", filename);
               // string path = Path.Combine("/UserFile/", Path.GetFileName(filename));


                //string destfolderpath = "/UserFile/"+string.Format("{0}.pdf", filename);
                // var fileStream = new FileStream(destfolderpath, FileMode.Create);
                //var pdfWriter2 = PdfWriter.GetInstance(pdfDoc, fileStream);
                // HttpContext.Current.Response.WriteFile(destfolderpath + string.Format("{0}.pdf", filename));
                File.WriteAllBytes(filepath, ms.ToArray());
                //HttpContext.Current.Response.WriteFile(destfolderpath);
                //HttpContext.Current.Response.Flush();
                //HttpContext.Current.Response.End();
            }
        }

        /// <summary>
        /// WriteHtml as PDF to Memory stream
        /// </summary>
        /// <param name="html"></param>
        /// <param name="language"></param>
        /// <param name="outStream"></param>
        /// <param name="tableborder"></param>
        /// <param name="excludeBraces"></param>
        private void WriteHtmlDoc(string html, string language, MemoryStream outStream,  bool tableborder, bool excludeBraces)
        {

            using (Document pdfDoc = new Document(PageSize.A4))
            {
                PdfWriter pdfWritter = PdfWriter.GetInstance(pdfDoc, outStream);
                itextEvents eventHandler = new itextEvents(pdfWritter);
                string regularFont = RBFontRegular;
                string lightFont = RBFontLight;
                if (language.Equals("ar"))
                {
                    regularFont = RBFontRegularAR;
                    lightFont = RBFontLightAR;
                }

                string fontPath = HttpContext.Current.Server.MapPath(regularFont);
                string lightFontPath = HttpContext.Current.Server.MapPath(lightFont);
                int columnWidth = (int)(pdfDoc.PageSize.Width - Settings.MarginLeft - Settings.MarginRight) / 2;
                int columnWidthre = (int)(pdfDoc.PageSize.Width - Settings.MarginLeft - Settings.MarginRight);

                //Get Course
                eventHandler.ColumnWidth = columnWidth;
                eventHandler.Language = language;
                pdfWritter.PageEvent = eventHandler;
                iTextSharp.text.html.simpleparser.HTMLWorker htmlWorker = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
                iTextSharp.text.html.simpleparser.StyleSheet htmlStyle = new iTextSharp.text.html.simpleparser.StyleSheet();

                iTextSharp.text.FontFactory.Register(fontPath, "document-font");
                iTextSharp.text.FontFactory.Register(lightFontPath, "document-font-light");

                //Apllying the styles for the tags

                htmlStyle.LoadTagStyle("tr", "style", "font-family:document-font-light;font-size:11px;line-height:16px;");
                htmlStyle.LoadStyle("div.buttonrow", "display", "none");

              
                

                //
                if (language.Equals("ar"))
                {
                    htmlStyle.LoadTagStyle("td", "style", "font-family:document-font-light;font-size:11px;line-height:16px;text-align: right;padding-bottom:5px;");
                    htmlStyle.LoadTagStyle("h1", "style", "font-family:document-font;font-size:24px;line-height:26px;text-align: right;");
                    htmlStyle.LoadTagStyle("h2", "style", "font-family:document-font;text-align: right;");
                    htmlStyle.LoadTagStyle("h3", "style", "font-family:document-font;text-align: right;");
                    htmlStyle.LoadTagStyle("p", "style", "font-family:document-font-light;text-align: right;font-size:11px;font-weight: normal;");
                    htmlStyle.LoadTagStyle("th", "style", "font-family:document-font-light;font-size:11px;line-height:16px;padding-bottom:5px;text-align: right;");
                    htmlStyle.LoadTagStyle("body", "encoding", "Identity-H");
                }
                else
                {
                    htmlStyle.LoadTagStyle("h1", "style", "font-family:document-font;font-size:24px;line-height:26px;");
                    htmlStyle.LoadTagStyle("h2", "style", "font-family:document-font;");
                    htmlStyle.LoadTagStyle("h3", "style", "font-family:document-font;");
                    htmlStyle.LoadTagStyle("td", "style", "font-family:document-font-light;font-size:11px;line-height:16px;padding-bottom:5px;");
                    //htmlStyle.LoadTagStyle("th", "style", "font-family:document-font-light;font-size:11px;line-height:16px;padding-bottom:5px;");
                    htmlStyle.LoadTagStyle("p", "style", "font-family:document-font-light;font-size:11px;font-weight: normal;");
                }

                htmlStyle.LoadTagStyle("tr.clsregards", "style", "text-align:right;width:" + columnWidthre + "px;");
                htmlStyle.LoadTagStyle("td.clsregards", "style", "text-align:right;width:" + columnWidthre + "px;");
                htmlStyle.LoadTagStyle("table.tbldata tr td", "style", "border: 1px solid black;");

                htmlWorker.SetStyleSheet(htmlStyle);
                pdfDoc.SetMargins(Settings.MarginLeft, Settings.MarginRight, 100, Settings.MarginBottom);
                pdfDoc.Open();
                pdfDoc.NewPage();

                if (language.Equals("ar") && excludeBraces)
                {
                    html = Regex.Replace(html, "\\(", " ");
                    html = Regex.Replace(html, "\\)", " ");
                }
                //  html = Regex.Replace(html, "</h3>", "</h3><br/>");
                //html = Regex.Replace(html, "</h2>", "</h2><br/>");
                html = Regex.Replace(html, "</p>", "</p><br/>");
                html = Regex.Replace(html, "</ul>", "</ul><br/><br/>");
                html = Regex.Replace(html, "</li>", "</li><br/><br/>");

                html = Regex.Replace(html, "</table>", "</table><br/>");
                //Uppercase all h4 and add tebles for horizontal rules
                //html = Regex.Replace(html, @"(<h2>(.*?)<\/h2>)", delegate(Match match)
                //{
                //    return "<table><tr><td>" + match.ToString() + "</td></tr></table>";
                //}, RegexOptions.Multiline);

                //Logger.Info(html);  

                StreamReader reader = new StreamReader(new MemoryStream(Encoding.UTF8.GetBytes(html)));

                foreach (IElement htmlElement in
                    iTextSharp.text.html.simpleparser.HTMLWorker.ParseToList(reader, htmlStyle)
                    )
                {
                    BuildPdfElement(htmlElement, pdfDoc, language, tableborder);
                }

                pdfDoc.Close();
            }
        }

        /// <summary>
        /// To Build PDF Element
        /// </summary>
        /// <param name="htmlElement"></param>
        /// <param name="pdfDoc"></param>
        /// <param name="language"></param>
        /// <param name="tableborder"></param>
        private static void BuildPdfElement(IElement htmlElement, Document pdfDoc, string language, bool tableborder)
        {
            if (htmlElement.Type == Element.PTABLE)
            {
                var table = (PdfPTable)htmlElement;

                PdfPTable tbl = new PdfPTable(table.NumberOfColumns);
                tbl.WidthPercentage = 100.0f;
                if (language.Equals("ar"))
                {
                    tbl.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                    tbl.DefaultCell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                    tbl.HorizontalAlignment = Element.ALIGN_RIGHT;
                }
                foreach (var row in table.Rows)
                {
                    foreach (var cell in row.GetCells())
                    {
                        PdfPCell newCell = new PdfPCell();
                        newCell.MinimumHeight = 10;
                        newCell.SetLeading(0f, 1.3f);
                        newCell.Phrase = new Phrase();
                        if (cell != null)
                        {
                            newCell.BackgroundColor = cell.BackgroundColor;
                            newCell.Colspan = cell.Colspan;
                            newCell.Rowspan = cell.Rowspan;
                            // newCell.Border = tableborder ? cell.Border : 0;
                            newCell.BorderColor = GrayColor.LIGHT_GRAY;
                            newCell.BorderWidth = cell.BorderWidth;
                            newCell.PaddingBottom = 5;
                            if (cell.Column.CompositeElements != null && cell.Column.CompositeElements.FirstOrDefault() != null && cell.Column.CompositeElements.FirstOrDefault().Chunks != null)
                            {
                                foreach (var ce in cell.Column.CompositeElements)
                                {

                                    if (ce.Type == Element.PTABLE)
                                    {
                                        BuildPdfElement(ce, pdfDoc, language, tableborder);
                                    }
                                    else
                                    {
                                        foreach (var ch in ce.Chunks)
                                        {
                                            newCell.Phrase.Add(new Chunk(ch.Content + " ", ch.Font));
                                        }
                                    }
                                }
                            }
                            tbl.AddCell(newCell);
                        }
                    }
                }
                pdfDoc.Add(tbl);
            }
            else
            {

                pdfDoc.Add(htmlElement);
            }
        }

        #region Dispose
        bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                //None
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }

    #region Listener class
    /// <summary>
    /// This event class used to add Header information to PDF 
    /// </summary>
    public class itextEvents : IPdfPageEvent
    {
        string RBFontRegular = ConfigurationManager.AppSettings["RBFontRegular"];
        string RBFontLight = ConfigurationManager.AppSettings["RBFontLight"];
        string RBFontRegularAR = ConfigurationManager.AppSettings["RBFontRegularAR"];
        string RBFontLightAR = ConfigurationManager.AppSettings["RBFontLightAR"];
        string Logo = ConfigurationManager.AppSettings["Logo"];
        string Line= ConfigurationManager.AppSettings["Line"];
        private PdfContentByte pdfContent;
        private PdfWriter pdfWritter;

        public int ColumnWidth
        {
            get;
            set;
        }

        public string Language
        {
            get;
            set;
        }

        public itextEvents(PdfWriter writter)
        {

            this.pdfWritter = writter;
        }
        public void OnEndPage(iTextSharp.text.pdf.PdfWriter writer, iTextSharp.text.Document document)
        {
            int headerImageHeight;
            pdfContent = writer.DirectContent;
            iTextSharp.text.Image imgPDF;

            //Header 
            imgPDF = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(Logo));
            headerImageHeight = (int)imgPDF.Height;
            if (Language.Equals("ar"))
            {
                imgPDF.SetAbsolutePosition(document.PageSize.Width - (Settings.MarginLeft + int.Parse(Math.Round(imgPDF.Width).ToString())), (document.PageSize.Height - (Settings.MarginTop + imgPDF.Height)));
            }
            else
            {
                imgPDF.SetAbsolutePosition(Settings.MarginLeft, (document.PageSize.Height - (Settings.MarginTop + imgPDF.Height)));
            }
            pdfContent.AddImage(imgPDF);

            //Doc title
            PdfContentByte canvas = pdfWritter.DirectContent;
            ColumnText col = new ColumnText(canvas);
            Paragraph p = new Paragraph();

            BaseColor baseColor = new BaseColor(125, 125, 126);

            string regularFont = RBFontRegular;
            string dateFormat = "dd/MM/yyyy";
            if (Language.Equals("ar"))
            {
                regularFont = RBFontRegularAR;
                dateFormat = "dd/MM/yyyy";
            }

            BaseFont baseFont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath(regularFont), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            Font headLineFont = new Font(baseFont, 12, iTextSharp.text.Font.NORMAL, baseColor);

            string dateHeader = DateTime.Now.ToString("dd-MMM-yyyy");// Resources.TextFormatted("PDFSubmittedOn.Text", Indivirtual.Misc.XSLTExtensions.Instance.FormatDate(DateTime.Now.ToString(), dateFormat));
            float dateWidth = baseFont.GetWidthPoint(dateHeader, 12);


            col = new ColumnText(canvas);
            if (Language.Equals("ar"))
            {
                col.SetSimpleColumn(0, document.PageSize.Height - ((headerImageHeight + Settings.MarginTop) - 10), Settings.MarginLeft + dateWidth, 50);
                col.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            }
            else
                col.SetSimpleColumn(document.PageSize.Width - (Settings.MarginLeft + int.Parse(Math.Round(dateWidth).ToString())), document.PageSize.Height - ((headerImageHeight + Settings.MarginTop) - 10), document.PageSize.Width + dateWidth, 50);

            col.Alignment = Element.ALIGN_LEFT;
            p = new Paragraph();
            p.Add(new Chunk(dateHeader, headLineFont));
            col.AddText(p);
            col.Go();



            //string dateHeader1 = "this is test data";// Resources.TextFormatted("PDFSubmittedOn.Text", Indivirtual.Misc.XSLTExtensions.Instance.FormatDate(DateTime.Now.ToString(), dateFormat));
            //float dateWidth1 = baseFont.GetWidthPoint(dateHeader1, 12);


            //col = new ColumnText(canvas);
            //if (Language.Equals("ar"))
            //{
            //    col.SetSimpleColumn(0, document.PageSize.Height - ((headerImageHeight + Settings.MarginTop) - 10), Settings.MarginLeft + dateWidth1, 50);
            //    col.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
            //}
            //else
            //    //col.SetSimpleColumn(document.PageSize.Width - (Settings.MarginLeft + int.Parse(Math.Round(dateWidth1).ToString())), document.PageSize.Height - ((headerImageHeight + Settings.MarginBottom) - 10), document.PageSize.Width + dateWidth1, 50);
            //    col.SetSimpleColumn(document.PageSize.Width - (Settings.MarginLeft + int.Parse(Math.Round(dateWidth1).ToString())), document.PageSize.Height-20, document.PageSize.Width + dateWidth1, 50);
            //col.Alignment = Element.ALIGN_LEFT;
            //p = new Paragraph();
            //p.Add(new Chunk(dateHeader1, headLineFont));
            //col.AddText(p);
            //col.Go();

            imgPDF = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(Line));
            imgPDF.ScaleToFit(document.PageSize.Width - (document.LeftMargin + document.RightMargin), document.PageSize.Height);
            imgPDF.SetAbsolutePosition(document.LeftMargin, document.PageSize.Height - (Settings.HeadDataStartY - 10));
            pdfContent.AddImage(imgPDF);

            imgPDF = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(Line));
            imgPDF.ScaleToFit(document.PageSize.Width - (document.LeftMargin + document.RightMargin), document.PageSize.Height);
            imgPDF.SetAbsolutePosition(document.LeftMargin, 50);
            pdfContent.AddImage(imgPDF);

            string copyRight = HttpContext.Current.Server.HtmlDecode("Riyad Bank");

            col = new ColumnText(canvas);
            if (Language.Equals("ar"))
            {
                //col.SetSimpleColumn(((document.PageSize.Width - 10) - (baseFont.GetWidthPoint(copyRight, 12))), 0, (((document.PageSize.Width - 10) - (baseFont.GetWidthPoint(copyRight, 12)))) + baseFont.GetWidthPoint(copyRight, 12), 50);
                col.SetSimpleColumn((document.PageSize.Width - 10) - (baseFont.GetWidthPoint(copyRight, 12)), 0, (document.PageSize.Width - 10) - (baseFont.GetWidthPoint(copyRight, 12)) + baseFont.GetWidthPoint(copyRight, 12), 50);

                col.RunDirection = PdfWriter.RUN_DIRECTION_RTL;
                col.Alignment = Element.ALIGN_RIGHT;
            }
            else
            {
                col.SetSimpleColumn(Settings.MarginLeft, 1, Settings.MarginBottom + baseFont.GetWidthPoint(copyRight, 12), 50);
                col.Alignment = Element.ALIGN_BOTTOM;
            }
            p = new Paragraph();
            p.Add(new Chunk(copyRight, headLineFont));
            col.AddText(p);
            col.Go();

            pdfContent.Stroke();

        }


        #region Not used methods
        public void OnChapter(PdfWriter writer, Document document, float paragraphPosition, Paragraph title)
        {
            //throw new NotImplementedException();
        }

        public void OnChapterEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            //throw new NotImplementedException();
        }

        public void OnCloseDocument(PdfWriter writer, Document document)
        {
            //throw new NotImplementedException();
        }

        public void OnGenericTag(PdfWriter writer, Document document, Rectangle rect, string text)
        {
            //throw new NotImplementedException();
        }

        public void OnOpenDocument(PdfWriter writer, Document document)
        {
            //throw new NotImplementedException();
        }

        public void OnParagraph(PdfWriter writer, Document document, float paragraphPosition)
        {
            //throw new NotImplementedException();
        }

        public void OnParagraphEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            //throw new NotImplementedException();
        }

        public void OnSection(PdfWriter writer, Document document, float paragraphPosition, int depth, Paragraph title)
        {
            //throw new NotImplementedException();
        }

        public void OnSectionEnd(PdfWriter writer, Document document, float paragraphPosition)
        {
            //throw new NotImplementedException();
        }

        public void OnStartPage(PdfWriter writer, Document document)
        {
            //throw new NotImplementedException();
        }
        #endregion

    }
    #endregion

    #region Settings class
    /// <summary>
    /// PDF Settings Information
    /// </summary>

    public static class Settings
    {
        public static int HeadDataStartY = 100;
        public static int MarginTop = 30;
        public static int MarginLeft = 50;
        public static int MarginRight = 50;
        public static int MarginBottom = 70;

        public static string Language = "ar";

    }
    #endregion
}
