﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace RB.Portal.Web.Models
{
    public class CommonList
    {
        XElement bankList;
        public CommonList()
        {
            bankList = XElement.Load(System.Web.Hosting.HostingEnvironment.MapPath("~/Source/BankList.xml"));
        }

        public List<RSD> getRSDList()
        {
            List<RSD> _rsdlist = new List<RSD>();
            foreach (XElement item in bankList.Elements("RSDList").Elements("rsditem"))
            {
                RSD _rsd = new RSD();
                _rsd.id = item.Attribute("name").Value.ToString();
                _rsd.value = item.Value.ToString();
                _rsdlist.Add(_rsd);
            }
            return _rsdlist;
        }

        public List<WBS> getWBSList()
        {
            List<WBS> _rsdlist = new List<WBS>();
            foreach (XElement item in bankList.Elements("WBSList").Elements("wbsitem"))
            {
                WBS _rsd = new WBS();
                _rsd.id = item.Attribute("name").Value.ToString();
                _rsd.value = item.Value.ToString();
                _rsdlist.Add(_rsd);
            }
            return _rsdlist;
        }

        public List<Currency> getCurrencyList()
        {
            List<Currency> _rsdlist = new List<Currency>();
            foreach (XElement item in bankList.Elements("CurrencyList").Elements("currency"))
            {
                Currency _rsd = new Currency();
                _rsd.id = item.Value.ToString();
                _rsd.value = item.Value.ToString();
                _rsdlist.Add(_rsd);
            }
            return _rsdlist;
        }

        public List<JobType> GetJobTypeList()
        {
            List<JobType> _joblist = new List<JobType>();
            foreach (XElement item in bankList.Elements("JobTypeList").Elements("jobtype"))
            {
                JobType _job = new JobType();
                _job.id = item.Attribute("name").Value.ToString();
                _job.value = item.Value.ToString();
                _joblist.Add(_job);
            }
            return _joblist.ToList();
        }
    }

    public class RSD
    {
        public string id { get; set; }
        public string value { get; set; }
    }

    public class WBS
    {
        public string id { get; set; }
        public string value { get; set; }
    }

    public class JobType
    {
        public string id { get; set; }
        public string value { get; set; }
    }

    public class Currency
    {
        public string id { get; set; }
        public string value { get; set; }
    }
}