﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RB.Portal.Web.Models
{
    public class clsNonWBS
    {
        public string SrNo { get; set; }
        [Required(ErrorMessage = "Beneficiary Reference no is missing")]
        public string BeneficiaryRef{ get; set; }
        [Required(ErrorMessage = "Employee name is missing")]
        public string EmployeeName{ get; set; }

        [Required(ErrorMessage = "Account number is missing")]
        public string AccountNumber { get; set; }

        [Required(ErrorMessage = "Bank code is missing")]
        public string BankCode { get; set; }

        [Required(ErrorMessage = "Amount is missing")]
        public string Amount { get; set; }

        [Required(ErrorMessage = "Currency is missing")]
        public string Currency { get; set; }

        [Required(ErrorMessage = "Status is missing")]
        public string Status { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }       
        public string PaymentDesc { get; set; }
    }
}