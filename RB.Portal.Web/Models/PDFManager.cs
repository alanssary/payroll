﻿
using RB.Portal.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Indivirtual.RB.PDFExport
{
    /// <summary>
    /// This class wrap Generator.cs to generate PDF
    /// </summary>
    public static class PDFManager
    {

        /// <summary>
        /// This method retun the PDF as Stream object from html
        /// </summary>
        /// <param name="html"></param>
        /// <param name="language"></param>
        /// <param name="filename"></param>
        /// <param name="tableborder"></param>
        ///  <param name="excludeBraces"></param>
        public static void GeneratePDF(string html, string language, string filename, string filepath, bool tableborder = false, bool excludeBraces=true)
        {
            using (Generator pdfGenerator = new Generator())
            {
                pdfGenerator.GeneratePDF(html, language, filename, filepath, tableborder, excludeBraces);
            }
        }

        

        /// <summary>
        /// Get Language name for PDF
        /// </summary>
        /// <param name="languageCode"></param>
        /// <returns></returns>
        public static string GetLanguageText(string languageCode)
        {
            if (languageCode.Equals("0"))
                return "English";
            else
                return "Arabic";
        }
       
    }
}
