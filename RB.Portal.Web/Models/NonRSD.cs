﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RB.Portal.Web.Models
{
    public class NonRSD
    {
        public string SrNo { get; set; }
        [Required(ErrorMessage = "Beneficiary Reference no is missing")]
        public string BeneficiaryRef{ get; set; }
        [Required(ErrorMessage = "Employee name is missing")]
        public string EmployeeName{ get; set; }

        [Required(ErrorMessage = "Account number is missing")]
        //[RegularExpression("^[0-9]*$", ErrorMessage = "Account number must be numeric")]
        //[MyStringLength(24, MinimumLength = 24, ErrorMessage = "IBAN account should be 24 characters")]
        //[Range(0, Int64.MaxValue, ErrorMessage = "Account number should not contain characters")]
        //[StringLength(24, ErrorMessage = "Account number cannot be longer than 40 characters.")]

        [RegularExpression(@"^.{24,24}$", ErrorMessage = "Account number should be of 24 characters")]
        //[MinLength(24, ErrorMessage = "Account number cannot be less than 40 characters.")]
        //[MaxLength(24, ErrorMessage = "Account number cannot be longer than 40 characters.")]
        public double AccountNumber{ get; set; }

        [Required(ErrorMessage = "Bank code is missing")]
        public string BankCode { get; set; }
        [Required(ErrorMessage = "Amount is missing")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Amount must be numeric")]
        public double Amount { get; set; }
        [Required(ErrorMessage = "Currency is missing")]
        public string Currency { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        [Required(ErrorMessage = "Status is missing")]
        public string Status { get; set; }
        public string PaymentDesc { get; set; }
    }

    public class MyStringLengthAttribute : StringLengthAttribute
    {
        public MyStringLengthAttribute(int maximumLength)
            : base(maximumLength)
        {
        }

        public override bool IsValid(object value)
        {
            string val = Convert.ToString(value);
            if (val.Length < base.MinimumLength)
                base.ErrorMessage = "Minimum length should be 24";
            if (val.Length > base.MaximumLength)
                base.ErrorMessage = "Maximum length should be 24";
            return base.IsValid(value);
        }
    }

}