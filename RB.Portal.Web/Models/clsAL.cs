﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RB.Portal.Web.Models
{
    public class clsAL
    {
        public string AgremeentCode { get; set; }
        public string AccountNumber { get; set; }
        public string TotalRecords { get; set; }
        public string TotalAmount { get; set; }
        public string ValueCreditDate { get; set; }
    }
}