﻿using RB.Portal.Web.App_LocalResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace RB.Portal.Web.Models
{
    public class Registration
    {

        public IList<SelectListItem> _JobTypeListItems { get; set; }
        public IList<SelectListItem> _RSDListItems { get; set; }
        public IList<SelectListItem> _WBSListItems { get; set; }
        public IList<SelectListItem> _CurrencyListItems { get; set; }

        public Registration()
        {
            _JobTypeListItems = new List<SelectListItem>();
            _RSDListItems = new List<SelectListItem>();
            _WBSListItems = new List<SelectListItem>();
            _CurrencyListItems = new List<SelectListItem>();
        }

        public List<string> _BankCodeList { get; set; }
        public List<string> _BankNameList { get; set; }
        public List<string> _BankClearingCodeList { get; set; }
        //public List<string> _CurrencyList { get; set; }

        [Display(Name = "JobType", ResourceType = typeof(GlobalRes))]
       // [Display(Name = "Job Type")]
       // [dropdownvalidate(ErrorMessage = "Select job type")]
        public string JobType { get; set; }

        [Display(Name = "WBSCompatibity", ResourceType = typeof(GlobalRes))]
        public string WBSCompatibity { get; set; }

        [Display(Name = "RSDCompatibity", ResourceType = typeof(GlobalRes))]
        public string RSDCompatibity { get; set; }

        [Display(Name = "CustomeName", ResourceType = typeof(GlobalRes))]
        [RequiredIfOtherValueEquals("RSDCompatibity", "nonrsdcom", "Customer name.")]
        public string CustomerName { get; set; }

        [Display(Name = "DepartmentName", ResourceType = typeof(GlobalRes))]
        [RequiredIfOtherValueEquals("RSDCompatibity", "rsdcom", "Department name is required.")]
        public string DepartmentName { get; set; }


        [Display(Name = "DivisionNumber", ResourceType = typeof(GlobalRes))]
        [RequiredIfOtherValueEquals("RSDCompatibity","rsdcom", "Division Number is required.")]
        public string DivisionNumber { get; set; }

        [Display(Name = "SubDivisionNumber", ResourceType = typeof(GlobalRes))]
        [RequiredIfOtherValueEquals("RSDCompatibity", "rsdcom", "Sub-Division Number is required.")]
        public string SubDivisionNumber { get; set; }

        [Display(Name = "SectorNumber", ResourceType = typeof(GlobalRes))]
        [RequiredIfOtherValueEquals("RSDCompatibity", "rsdcom", "Sector number is required.")]
        public string SectorNumber { get; set; }

        [Display(Name = "ClassNumber", ResourceType = typeof(GlobalRes))]
        [RequiredIfOtherValueEquals("RSDCompatibity", "rsdcom", "Class number is required.")]
        public string ClassNumber { get; set; }

        [Display(Name = "PaymentOrderNumber", ResourceType = typeof(GlobalRes))]
        [RequiredIfOtherValueEquals("RSDCompatibity", "rsdcom", "Payment order number is required.")]
        public string PaymentOrderNumber { get; set; }

        [Display(Name = "Mins of lab established id")]
        [RequiredIfOtherValueEquals("WBSCompatibity", "wscom", "Required")]
        public string MinsOfLabEstablishedId { get; set; }

        [Display(Name = "Establised/Commerce Room ID")]
        [RequiredIfOtherValueEquals("WBSCompatibity", "wscom", "Required")]
        public string CommerceRoomId { get; set; }

        [Display(Name = "CreditDate", ResourceType = typeof(GlobalRes))]
        public string CreditDate { get; set; }

        [Display(Name = "CompanyName", ResourceType = typeof(GlobalRes))]
        [RequiredIfOtherValueEquals("RSDCompatibity", "rsdcom", "Agency name is required.")]
        public string CompanyName { get; set; }

        [Display(Name = "AgremeentCode", ResourceType = typeof(GlobalRes))]
        [Required(ErrorMessage = "Enter agent code")]
        public string AgremeentCode { get; set; }

        [Display(Name = "FundingAccount", ResourceType = typeof(GlobalRes))]
        [Required(ErrorMessage = "Enter funding account")]
        [FieldLength(13)]
        public string FundingAccount { get; set; }

        [Display(Name = "BranchNo", ResourceType = typeof(GlobalRes))]
        [Required(ErrorMessage = "Enter branch number")]
        [PosNumber(ErrorMessage = "Branch number must be numeric")]
        public string BranchNo { get; set; }

        [Display(Name = "ValueDate", ResourceType = typeof(GlobalRes))]
        [RequiredIfOtherValueEquals("RSDCompatibity", "rsdcom", "Value date is required.")]
        public string ValueDate { get; set; }

        [Display(Name = "Currency", ResourceType = typeof(GlobalRes))]
        [Required(ErrorMessage = "Enter currency")]
        public string Currency {
            get
            {
                return "SAR";
            }
            set
            {
            }
        }


        [Display(Name = "BatchNo", ResourceType = typeof(GlobalRes))]
        public string BatchNo { get; set; }

        [Display(Name = "Label", ResourceType = typeof(GlobalRes))]
        public string LabelMsg { get; set; }

        //  [Display(Name = "File reference number")]
        //  [RequiredIfOtherValueEquals("RSDCompatibity", "nonrsdcom", "File reference number is required.")]
        // public string FileReference { get; set; }

        [Display(Name = "BankCode", ResourceType = typeof(GlobalRes))]
        [Required(ErrorMessage = "Enter bank code")]
        //[RegularExpression("[^0-9]", ErrorMessage = "Bank code must be numeric")]
        public string BankCode
        {
            get
            {
                return "RIBLSARIXXXX";
            }
            set
            {
            }
        }

        public string Language { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if(!string.IsNullOrEmpty(FundingAccount))
            {
                if (CommonHelper.takeNDigits(Convert.ToInt32(FundingAccount), 3) != Convert.ToInt32(BranchNo))
                {
                    yield return new ValidationResult("Invalid brach number, it must be first three digits of Funding Account no.", new List<string> { "Item3" });
                }
            }
           
        }

        public class PosNumberAttribute : ValidationAttribute
        {
            public override bool IsValid(object value)
            {
                if (value == null)
                {
                    return true;
                }
                int getal;
                //if(Regex.IsMatch(value.ToString(), "^[0-9]{13}$"))
                //{
                //    return true;
                //}
                if (int.TryParse(value.ToString(), out getal))
                {
                    if (getal >= 0)
                        return true;
                }
                return false;
            }
        }

        public class dropdownvalidateAttribute : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                if (value == null)
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }
                else if(string.IsNullOrEmpty(value.ToString()))
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }
                if (!string.IsNullOrEmpty(value.ToString()))
                {
                    return ValidationResult.Success;
                }
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }

            public override string FormatErrorMessage(string name)
            {
                return ErrorMessageString;
            }
        }

        public sealed class FieldLengthAttribute : ValidationAttribute
        {
            private int _length { get; set; }
            public FieldLengthAttribute(int length)
            {
                _length = length;
                ErrorMessage = "Invalid funding account (allow only 13 digits numeric number)";
            }

            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {

                if (value != null)
                {
                    int objectLength = Convert.ToString(value).Length;
                    if (Regex.IsMatch(value.ToString(), "^[0-9]{13}$"))
                    {
                        return ValidationResult.Success;
                    }
                   else if (objectLength < _length || objectLength > _length)
                    {
                        return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                    }
                }
                return ValidationResult.Success;
            }

            public override string FormatErrorMessage(string name)
            {
                return ErrorMessageString;
            }

        }       
    }

    public sealed class RequiredIfOtherValueEquals : ValidationAttribute, IClientValidatable
    {

        private const string clientValidationType = "requiredifothervalueequals";
        private const string clientValidationParameterPropertyName = "other";
        private const string clientValidationParameterCompareValueName = "valuetocompare";

        public RequiredIfOtherValueEquals(string otherProperty, string valueToCompare, string errorMessageResourceKey)
            : base()
        {
            if (string.IsNullOrEmpty(otherProperty) || string.IsNullOrEmpty(valueToCompare) || string.IsNullOrEmpty(errorMessageResourceKey))
                throw new ArgumentException("otherProperty or errorMessageResourceKey or valueToCompare is null or empty");

            OtherProperty = otherProperty;
            ValueToCompare = valueToCompare;
            ErrorMessage = errorMessageResourceKey;
        }


        public string OtherProperty { get; private set; }
        public string ValueToCompare { get; private set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo otherPropertyInfo = validationContext.ObjectType.GetProperty(OtherProperty);
            if (otherPropertyInfo == null)
                return new ValidationResult(ErrorMessage);
            object otherPropertyValue = otherPropertyInfo.GetValue(validationContext.ObjectInstance, null);
            if (otherPropertyValue == null)
                return ValidationResult.Success;
            var otherValueString = otherPropertyValue.ToString();
            if (string.Equals(ValueToCompare, otherValueString, StringComparison.OrdinalIgnoreCase) && (value == null || string.IsNullOrEmpty(value.ToString())))
                return new ValidationResult(ErrorMessage);
            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var clientRule = new ModelClientValidationRule { ErrorMessage = this.ErrorMessage, ValidationType = clientValidationType };
            clientRule.ValidationParameters.Add(clientValidationParameterPropertyName, OtherProperty);
            clientRule.ValidationParameters.Add(clientValidationParameterCompareValueName, ValueToCompare);
            yield return clientRule;
        }
    }


    public class EditModel
    {
        [Display(Name = "JobType", ResourceType = typeof(GlobalRes))]
        public string JobType { get; set; }

        [Display(Name = "WBSCompatibity", ResourceType = typeof(GlobalRes))]
        public string WBSCompatibity { get; set; }

        [Display(Name = "RSDCompatibity", ResourceType = typeof(GlobalRes))]
        public string RSDCompatibity { get; set; }
    }
}

