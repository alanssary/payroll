﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RB.Portal.Web.Models
{
    public class clsWBS
    {
        public string SrNo { get; set; }

        [Required(ErrorMessage = "Beneficiary Id is missing")]
        public string BeneficiaryId { get; set; }

        [Required(ErrorMessage = "Employee name is missing")]
        public string EmployeeName { get; set; }

        //[Required(ErrorMessage = "Iqama is missing")]
        //public string Iqama { get; set; }

        [Required(ErrorMessage = "Account number is missing")]
        //[RegularExpression(@"^.{24,24}$", ErrorMessage = "Account number should be of 24 characters")]
        public string AccountNumber { get; set; }       

        [Required(ErrorMessage = "Bank code is missing")]
        public string BankCode { get; set; }

        [Required(ErrorMessage = "Net amount is missing")]
        //[RegularExpression("^[0-9]*$", ErrorMessage = "Amount must be numeric")]
        public string NetAmount { get; set; }

        [Required(ErrorMessage = "Basic salary is missing")]
        public string BasicSalary { get; set; }

        public string HousingAllowance { get; set; }

        public string OtherEarnings { get; set; }

        public string Deductions { get; set; }

        public string Address { get; set; }

        [Required(ErrorMessage = "Status is missing")]
        public string Status { get; set; }

        public string PaymentDesc { get; set; }

        public string PaymentRef { get; set; }

        [Required(ErrorMessage = "Currency is missing")]
        public string Currency { get; set; }

        // public string City { get; set; }
        //public double ZipCode { get; set; }

        // [Required(ErrorMessage = "Nationality is missing")]
        // public string Nationality { get; set; }


        // [Required(ErrorMessage = "Beneficiary name is missing")]
        // public string BeneficiaryName { get; set; }


       

        // [Required(ErrorMessage = "Accrual month is missing")]
        // public string AccrualMonth { get; set; }

        // [Required(ErrorMessage = "Apprentice is missing")]
        // public string Apprentice { get; set; }

        //  public string CareerLadder { get; set; }
        public string Remarks { get; set; }
    }
}