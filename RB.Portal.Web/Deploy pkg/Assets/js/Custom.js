﻿$(function () {
    registrationFormSubmit();
    initAlphaNum();
    registrationEdit();
    myFunction();

    function myFunction() {
        //var x = document.getElementById("myTopnav");
        //if (x.className === "topnav") {
        //    x.className += " responsive";
        //} else {
        //    x.className = "topnav";
        //}
    }

    $('.btndownload').on('click', function () {
        var url = $(this).attr('data-url');
        window.location = "/Registration/TextDownload?filename=" + url;
    });

    $('#btnRegAdd').on('click', function (e) {
        var frmadd = $("#frmRegistration")[0];
        var btndownload = $('.divRegMsg');
        var actionurl = "",
        subtype = "";
        var jobtype = $('#JobType').val();

        var blockContainer = $('.pnlregistration');
        var lang = $('.mainhtmltag').attr('lang');

        if (jobtype == "Corporate") {
            subtype = $('#WBSCompatibity').val();
            if (subtype == "wscom") {
                actionurl = '/Registration/regUpdateWBSExcelSheet';
            }
            else if (subtype == "nonwscom") {
                actionurl = '/Registration/regUpdateNonRSDWBSExcelSheet';
            }
        }
        else if (jobtype == "Government") {
            subtype = $('#RSDCompatibity').val();
            if (subtype == "rsdcom") {
                actionurl = '/Registration/regUpdateRSDExcelSheet';
            }
            else if (subtype == "nonrsdcom") {
                actionurl = '/Registration/regUpdateNonRSDWBSExcelSheet';
            }
        }

        $('#list').html('');
        if (frmadd != typeof ('undefined')) {
            var data = new FormData(frmadd);
            if ($('#frmRegistration').valid()) {
                blockContainer.block({
                    message: '<div id="" class="loader"></div>'
                });

                $.ajax({
                    type: "POST",
                    url: actionurl+'?lang=' + lang,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        if (result.Haserror) {

                            setTimeout(function () {
                                if (!btndownload.hasClass('controlhide')) {
                                    $(btndownload).addClass('controlhide');
                                }
                                $.each(result.msg, function (i, v) {
                                    $('#list').append('<li>' + v + '</li>');
                                });

                                $('.errorList').removeClass('controlhide');

                                if (!$('#frmRegistration').hasClass('controlhide')) {
                                    $('#frmRegistration').removeClass('controlhide');
                                }

                                if ($('.errorList').length > 0) {
                                    $('html, body').animate({
                                        scrollTop: $('.errorList').offset().top - 10
                                    }, 1000);
                                }
                                blockContainer.unblock();
                            }, 2000);
                        }
                        else {


                            setTimeout(function () {
                                $(btndownload).removeClass('controlhide');
                                $(btndownload).find('.btndownload').attr('href', result.filepath);
                                if (!$('.errorList').hasClass('controlhide')) {
                                    $('.errorList').addClass('controlhide');
                                }
                                $('#frmRegistration').addClass('controlhide');
                                blockContainer.unblock();
                            }, 2000);
                        }
                    },
                    submitComplete: function () {
                        blockContainer.unblock();
                    }
                });
            }
            else {
                e.preventDefault();
                blockContainer.unblock();
            }


        }
    });

    $("#file").on("change", function (e) {
        var files = e.target.files;
        var blockContainer = $('#divexcelupload');
        blockContainer.block({
            message: '<div id="" class="loader uploadfile"></div>'
        });
        if (files != null) {
            setTimeout(function () {
                $('#divexceluploadbtn').removeClass('controlhide');
                blockContainer.unblock();

            }, 2000);
        }

    });


    $('#btnUploadfile').on('click', function (e) {

        $('.errorList ul li').remove();
        if (!$('.errorList').hasClass('controlhide')) {
            $('.errorList').addClass('controlhide');
        }
        var blockContainer1 = $('.regedit');
        var _files = $('#file')[0].files[0];
        var jobtype = $('#jobtypeedit').val();
        var btntxtdownload = $('#divtxtdownload');
        var subtype = "";
        if (jobtype == "Corporate") {
            subtype = $('#wbscompatibilityedit').val();
        }
        else {
            subtype = $('#rsdcompatibilityedit').val();
        }

        var frm = $("#frmuploadexcel")[0];
        if (_files.size > 0) {
            if (frm !== undefined) {
                var lang = $('#hdnlanguage').val();

                var data = new FormData(frm);
                data.append("file", _files);
                blockContainer1.block({
                    message: '<div id="" class="loader"></div>'
                });
                $.ajax({
                    type: "POST",
                    url: '/Registration/EditExcel?jobtype=' + jobtype + '&subtype=' + subtype + '&lang=' + lang,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        if (result.Haserror) {
                            $("#frmuploadexcel").removeClass('controlhide');
                            if (!btntxtdownload.hasClass('controlhide')) {
                                $(btntxtdownload).addClass('controlhide');
                            }
                            $.each(result.msg, function (i, v) {
                                $('#list').append('<li>' + v + '</li>');
                            });

                            $('.errorList').removeClass('controlhide');

                            blockContainer1.unblock();
                        }
                        else {
                            $(btntxtdownload).removeClass('controlhide');
                            $(btntxtdownload).find('.btndownload').attr('data-url', result.filepath);
                            $(btntxtdownload).find('.btndownloadAL').prop('href', result.alpath);
                            

                            blockContainer1.unblock();

                            if (!$("#frmuploadexcel").hasClass('controlhide')) {
                                $("#frmuploadexcel").addClass('controlhide');
                            }
                        }
                    },
                    error: function (xhr, status, p3, p4) {
                        blockContainer1.unblock();
                    }
                });
            } else {
                blockContainer1.unblock();
            }
        }
    });

    function nonWorkingDates(date) {
        var day = date.getDay(), Sunday = 0, Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5, Saturday = 6;
        // var closedDates = [[7, 29, 2009], [8, 25, 2010]];
        var closedDays = [[Friday], [Saturday]];
        for (var i = 0; i < closedDays.length; i++) {
            if (day == closedDays[i][0]) {
                return [false];
            }
        }

        //for (i = 0; i < closedDates.length; i++) {
        //    if (date.getMonth() == closedDates[i][0] - 1 &&
        //    date.getDate() == closedDates[i][1] &&
        //    date.getFullYear() == closedDates[i][2]) {
        //        return [false];
        //    }
        //}

        return [true];
    }


    $("#ValueDate").datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: '+3D',
        maxDate: '+30D',
        beforeShowDay: function (date) {
            var show = true;
            if (date.getDay() == 5 || date.getDay() == 6) show = false
            return [show];
        }
        //beforeShow:
        //    function (date) {

        //    //var minDate = $('#checkIn').datepicker('getDate');
        //    //$('#checkOut').datepicker('option', 'minDate', minDate);
        //    //var maxDate = new Date();
        //    //maxDate.setDate(new Date().getDate() + 3);
        //    //$('#ValueDate').datepicker('option', 'maxDate', maxDate);
        //    //if (date.getDay() == 5 || date.getDay() == 6) { show = false; }
        //}
    }).bind("change", function () {
        var minValue = $(this).val();
        $("#ValueDatehdn").val(minValue);
        $('#ValueDate').removeClass('error');
        $('#ValueDate').siblings('label.error').text('');
    });
    $("#ValueDate1").click(function () {
        $("#ValueDate").datepicker("show");
    });

    $("#CreditDate").datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: '+3D',
        maxDate: '+30D',
        //beforeShow: function (input, inst) {
        //    //var minDate = $('#checkIn').datepicker('getDate');
        //    //$('#checkOut').datepicker('option', 'minDate', minDate);
        //    var maxDate = new Date();
        //    maxDate.setDate(new Date().getDate() + 3);
        //    $('#CreditDate').datepicker('option', 'maxDate', maxDate);
        //},
        beforeShowDay: function (date) {
            var show = true;
            if (date.getDay() == 5 || date.getDay() == 6) show = false
            return [show];
        }
    }).bind("change", function () {
        var minValue = $(this).val();
        $("#CreditDatehdn").val(minValue);
        $("#v").datepicker("option", "minDate", minValue);
        $('#CreditDate').removeClass('error');
        $('#CreditDate').siblings('label.error').text('');
    });
    $("#ValueCreditDate").click(function () {
        $("#CreditDate").datepicker("show");
    });

    $('#FundingAccount').on('change keyup paste', function () {
        var no = $(this).val();
        if ($.isNumeric(no)) {
            if ($(this).val().length == 13) {
                $('#BranchNo').val($(this).val().substr(0, 3));
            }
        }
    });
});

function initAlphaNum() {
    $(".fundingaccount-numeric-fields").numeric({
        maxDigits: 13,
        allowMinus: false,
        allowSpace: false,
        allowThouSep: false,
        allowDecSep: false,
        allowPlus: false
    });
    $("#BranchNo").numeric({
        maxDigits: 3,
        allowMinus: false,
        allowSpace: false,
        allowThouSep: false,
        allowDecSep: false,
        allowPlus: false
    });
    $(".numeric-fields-3").numeric({
        maxDigits: 3,
        allowMinus: false,
        allowSpace: false,
        allowThouSep: false,
        allowDecSep: false,
        allowPlus: false
    });
    $(".numeric-fields-2").numeric({
        maxDigits: 2,
        allowMinus: false,
        allowSpace: false,
        allowThouSep: false,
        allowDecSep: false,
        allowPlus: false
    });

    $(".alphanum-fields-8").alphanum({
        maxLength: 8
    });
}

function registrationEdit() {

    var wbsedit = $("#wbscompatibilityedit");
    var rsdedit = $("#rsdcompatibilityedit");
    var divupload = $("#divexcelupload");
    var divuploadbtn = $("#divexceluploadbtn");

    $("#jobtypeedit").on('change', function () {
        var status = this.value;
        var btntxtdownload = $('#divtxtdownload');
        if (!$(btntxtdownload).hasClass('controlhide')) {
            $(btntxtdownload).addClass('controlhide');
        }
        if (!$(divuploadbtn).hasClass('controlhide')) {
            $(divuploadbtn).addClass('controlhide');
        }

        $("#file").val("");


        if (status == "Corporate") {
            $(wbsedit).closest('div.form-group').removeClass('controlhide');
            if (!$(rsdedit).closest('div.form-group').hasClass('controlhide')) {
                $(rsdedit).closest('div.form-group').addClass('controlhide');
            }

            if (!$(divupload).hasClass('controlhide')) {
                $(divupload).addClass('controlhide');
            }


            $(wbsedit).prop('selectedIndex', 0);
        }
        else if (status == "Government") {
            $(rsdedit).closest('div.form-group').removeClass('controlhide');
            if (!$(wbsedit).closest('div.form-group').hasClass('controlhide')) {
                $(wbsedit).closest('div.form-group').addClass('controlhide');
            }
            if (!$(divupload).hasClass('controlhide')) {
                $(divupload).addClass('controlhide');
            }
            $(rsdedit).prop('selectedIndex', 0);
        }
        else {
            if (!$(wbsedit).closest('div.form-group').hasClass('controlhide')) {
                $(wbsedit).closest('div.form-group').addClass('controlhide');
            }
            if (!$(divupload).hasClass('controlhide')) {
                $(divupload).addClass('controlhide');
            }
            if (!$(rsdedit).closest('div.form-group').hasClass('controlhide')) {
                $(rsdedit).closest('div.form-group').addClass('controlhide');
            }

            $(wbsedit).prop('selectedIndex', 0);
            $(rsdedit).prop('selectedIndex', 0);
        }

    });

    $(wbsedit).on('change', function () {
        $("#file").val("");
           if (!$(divuploadbtn).hasClass('controlhide')) {
            $(divuploadbtn).addClass('controlhide');
           }

        if ($(this).val() != "") {
            $(divupload).removeClass('controlhide');
        }
        else {
            if (!$(divupload).hasClass('controlhide')) {
                $(divupload).addClass('controlhide');
            }
        }

        var btntxtdownload = $('#divtxtdownload');
        if (!$(btntxtdownload).hasClass('controlhide')) {
            $(btntxtdownload).addClass('controlhide');
        }
    });
    $(rsdedit).on('change', function () {
        $("#file").val("");
        if (!$(divuploadbtn).hasClass('controlhide')) {
            $(divuploadbtn).addClass('controlhide');
        }

        if ($(this).val() != "") {
            $(divupload).removeClass('controlhide');
        }
        else {
            if (!$(divupload).hasClass('controlhide')) {
                $(divupload).addClass('controlhide');
            }
        }

        var btntxtdownload = $('#divtxtdownload');
        if (!$(btntxtdownload).hasClass('controlhide')) {
            $(btntxtdownload).addClass('controlhide');
        }

    });
}

function registrationFormSubmit() {
    // var item1 = $("#WBSCompatibity").parent();

    var item1 = $("#WBSCompatibity").closest('div.form-group');
    var item2 = $("#RSDCompatibity").closest('div.form-group');
    // var item2 = $("#RSDCompatibity").parent();
    var CreditDateP = $('#CreditDate').closest('div.form-group');
    var ValueDateP = $('#ValueDate').closest('div.form-group');
    var agencyname = $('#company-name').closest('div.form-group');
    var DepartmentName = $('#DepartmentName').closest('div.form-group');
    var DivisionNumber = $('#DivisionNumber').closest('div.form-group');
    var SubDivisionNumber = $('#SubDivisionNumber').closest('div.form-group');
    var CustomerNameP = $('#CustomerName').closest('div.form-group');
    var MinsOfLabEstablishedIdP = $('#MinsOfLabEstablishedId').closest('div.form-group');
    var CommerceRoomIdP = $('#CommerceRoomId').closest('div.form-group');

    var divRsd = $('#divRsd');

    $("#JobType").change(function () {
        $("#WBSCompatibity").val("");
        $("#RSDCompatibity").val("");
        var status = this.value;
        if (!$(agencyname).hasClass('controlhide'))
            $(agencyname).addClass('controlhide');
        if (!$(MinsOfLabEstablishedIdP).hasClass('controlhide'))
            $(MinsOfLabEstablishedIdP).addClass('controlhide');
        if (!$(CommerceRoomIdP).hasClass('controlhide'))
            $(CommerceRoomIdP).addClass('controlhide');

        if (!$(divRsd).hasClass('controlhide'))
            $(divRsd).addClass('controlhide');

        if (!$(CustomerNameP).hasClass('controlhide'))
            $(CustomerNameP).addClass('controlhide');

        if (status == "Corporate") {
            if ($(item1).hasClass('controlhide'))
                $(item1).removeClass('controlhide');

            if (!$(item2).hasClass('controlhide'))
                $(item2).addClass('controlhide');

            if ($(CreditDateP).hasClass('controlhide'))
                $(CreditDateP).removeClass('controlhide');

            if (!$(ValueDateP).hasClass('controlhide'))
                $(ValueDateP).addClass('controlhide');
        }
        else if (status == "Government") {

            if (!$(CustomerNameP).hasClass('controlhide'))
                $(CustomerNameP).addClass('controlhide');

            if (!$(item1).hasClass('controlhide'))
                $(item1).addClass('controlhide');

            if ($(item2).hasClass('controlhide'))
                $(item2).removeClass('controlhide');

            if (!$(CreditDateP).hasClass('controlhide'))
                $(CreditDateP).addClass('controlhide');

            if ($(ValueDateP).hasClass('controlhide'))
                $(ValueDateP).removeClass('controlhide');

        }
        else {
            if (!$(item1).hasClass('controlhide'))
                $(item1).addClass('controlhide');
            if (!$(item2).hasClass('controlhide'))
                $(item2).addClass('controlhide');
        }
    });

    $("#RSDCompatibity").on('change', function () {

        if (!$(MinsOfLabEstablishedIdP).hasClass('controlhide'))
            $(MinsOfLabEstablishedIdP).addClass('controlhide');
        if (!$(CommerceRoomIdP).hasClass('controlhide'))
            $(CommerceRoomIdP).addClass('controlhide');

        if ($(this).val() == "rsdcom") {
            $(divRsd).removeClass('controlhide');
            $(agencyname).removeClass('controlhide');
            if (!$(CustomerNameP).hasClass('controlhide'))
                $(CustomerNameP).addClass('controlhide');
            if (!$(CreditDateP).hasClass('controlhide'))
                $(CreditDateP).addClass('controlhide');
        }
        else {
            if (!$(agencyname).hasClass('controlhide'))
                $(agencyname).addClass('controlhide');
            if (!$(divRsd).hasClass('controlhide'))
                $(divRsd).addClass('controlhide');
            $(CustomerNameP).removeClass('controlhide');
            $(CreditDateP).removeClass('controlhide');
        }
    });


    $("#WBSCompatibity").on('change', function () {
        if ($(this).val() == "wscom") {
            $(CustomerNameP).removeClass('controlhide');
            $(CreditDateP).removeClass('controlhide');
            $(MinsOfLabEstablishedIdP).removeClass('controlhide');
            $(CommerceRoomIdP).removeClass('controlhide');
        }
        else {
            $(CustomerNameP).removeClass('controlhide');
            $(CreditDateP).removeClass('controlhide');

            if (!$(MinsOfLabEstablishedIdP).hasClass('controlhide'))
                $(MinsOfLabEstablishedIdP).addClass('controlhide');
            if (!$(CommerceRoomIdP).hasClass('controlhide'))
                $(CommerceRoomIdP).addClass('controlhide');
        }
    });



    $('#frmRegistration').validate({
        errorElement: "label",
        errorClass: "error",
        ignore: "",
        rules: {
            JobType: {
                customvalid: true
            },
            WBSCompatibity: {
                customvalid: true
            },            RSDCompatibity: {
                customvalid: true
            },            CompanyName: {
                customvalid: true
            },            AgremeentCode:                {
                    customvalid: true
                },            FundingAccount:                    {
                        customvalid: true,
                        customLengthvalid: true
                    },            BranchNo:                {
                    customvalid: true
                },            ValueDate: {
                customvalid: true
            },            CreditDate: {
                customvalid: true
            },            DepartmentName: {
                customvalid: true
            },            DivisionNumber: {
                customvalid: true
            },            SubDivisionNumber: {
                customvalid: true
            },            SectorNumber: {
                customvalid: true
            },            ClassNumber: {
                customvalid: true
            },            PaymentOrderNumber: {
                customvalid: true
            },            Currency: {
                required: true
            },            BankCode: {
                required: true
            },            CustomerName: {
                customvalid: true
            },            MinsOfLabEstablishedId: {
                customvalid: true
            },            CommerceRoomId: {
                customvalid: true
            },
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    success: function () {
                    }
                });
            },

        },
        messages: {
            JobType: "Required",            AgremeentCode: "Required",            FundingAccount: {
                required: "Required",
                customLengthvalid: "Invalid funding account (allow only 13 digits numeric number)"
            },            BranchNo: "Required",            ValueDate: "Required",            DepartmentName: "Required",            DivisionNumber: "Required",            SubDivisionNumber: "Required",            SectorNumber: "Required",            ClassNumber: "Required",            PaymentOrderNumber: "Required",            Currency: "Required",            BankCode: "Required",            CustomerName: "Required",            MinsOfLabEstablishedId: "Required",            CommerceRoomId: "Required"
        },
        errorPlacement: function (error, element) {

            if (element.attr('id') == "CreditDatehdn") {
                error.insertAfter($('#ValueCreditDate'));
                $('#CreditDate').addClass('error');
            }

            if (element.attr('id') == "ValueDatehdn") {
                error.insertAfter($('#ValueDate1'));
                $('#ValueDate').addClass('error');
            }

            if ($(element).hasClass('custom-phone-field')) {
                element.parent().parent().append(error);
            }
            else if (($(element).prop('tagName') == 'SELECT' && $(element).hasClass('chosen-select'))
                                ) {
                error.insertAfter(element.parent());
            }
            else if ($(element).prop('type') == 'text' && $(element).attr('id') == 'salary') {
                element.parent().append(error);
            }

            else {
                error.insertAfter(element);
            }
        }
    });

    $.validator.addMethod("customLengthvalid", function (value, element, param) {
        if (value.length == 13) {
            return true;
        }
        else {
            return false;
        }
    }, 'Invalid funding account (allow only 13 digits numeric number)');


    $.validator.addMethod("customvalid", function (value, element, param) {
        var jobtypeval = $('#JobType').val();
        var id = $(element).attr('id');
        var isenabled = $($(element).closest('div.form-group')).hasClass('controlhide');
        if (!isenabled) {
            if (id == "JobType") {
                if (value == "" || typeof (value) == "undefined" || value == null) {
                    return false;
                }
                else {
                    return true;
                }
            }

            if (id == "AgremeentCode") {
                if (value == "" || typeof (value) == "undefined" || value == null) {
                    return false;
                }
                else {
                    return true;
                }
            }
            if (id == "FundingAccount") {
                if (value == "" || typeof (value) == "undefined" || value == null) {
                    return false;
                }
                else {
                    return true;
                }
            }
            if (id == "BranchNo") {
                if (value == "" || typeof (value) == "undefined" || value == null) {
                    return false;
                }
                else {
                    return true;
                }
            }

            else if ((jobtypeval == "Government" || jobtypeval == "Corporate") && id == "CreditDatehdn") {
                if ($('#RSDCompatibity').val() != "rsdcom") {
                    var crdate = $('#CreditDatehdn').val();
                    if (crdate == "" || typeof (crdate) == "undefined" || crdate == null) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            }
            else if (jobtypeval == "Government" && id == "RSDCompatibity") {
                if (value == "" || typeof (value) == "undefined" || value == null) {
                    return false;
                }
                else {
                    return true;
                }
            }
            else if (jobtypeval == "Corporate" && id == "WBSCompatibity") {
                if (value == "" || typeof (value) == "undefined" || value == null) {
                    return false;
                }
                else {
                    return true;
                }
            }

            if (id == "CustomerName") {
                if (value == "" || typeof (value) == "undefined" || value == null) {
                    return false;
                }
                else {
                    return true;
                }
            }

            if (id == "MinsOfLabEstablishedId") {
                if (value == "" || typeof (value) == "undefined" || value == null) {
                    return false;
                }
                else {
                    return true;
                }
            }

            if (id == "CommerceRoomId") {
                if (value == "" || typeof (value) == "undefined" || value == null) {
                    return false;
                }
                else {
                    return true;
                }
            }

            if (id == "company-name") {
                if (value == "" || typeof (value) == "undefined" || value == null) {
                    return false;
                }
                else {
                    return true;
                }
            }
            var isdivenabled = $('#divRsd').hasClass('controlhide');
            if (!isdivenabled) {

                if ((jobtypeval == "Government") && id == "ValueDatehdn") {
                    if ($('#RSDCompatibity').val() == "rsdcom") {
                        var valdate = $('#ValueDatehdn').val();
                        if (valdate == "" || typeof (valdate) == "undefined" || valdate == null) {
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                }

                if (id == "DepartmentName") {
                    if (value == "" || typeof (value) == "undefined" || value == null) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                if (id == "DivisionNumber") {
                    if (value == "" || typeof (value) == "undefined" || value == null) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                if (id == "SubDivisionNumber") {
                    if (value == "" || typeof (value) == "undefined" || value == null) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                if (id == "SectorNumber") {
                    if (value == "" || typeof (value) == "undefined" || value == null) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                if (id == "ClassNumber") {
                    if (value == "" || typeof (value) == "undefined" || value == null) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                if (id == "PaymentOrderNumber") {
                    if (value == "" || typeof (value) == "undefined" || value == null) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

            }
        }
        return true;

    }, 'Required');
}



