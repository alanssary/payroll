﻿using System.Web.Mvc;
using RB.Portal.Web.Attributes;

namespace RB.Portal.Web.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new LocalizationAttribute("en"), 0);
        }
    }
}